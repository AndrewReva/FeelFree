package com.spduniversity.feelfree.service.impl;

import com.spduniversity.feelfree.config.Application;
import com.spduniversity.feelfree.config.ApplicationProfile;
import com.spduniversity.feelfree.domain.model.Image;
import com.spduniversity.feelfree.domain.repository.ImageRepository;
import com.spduniversity.feelfree.service.common.BlobService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@ActiveProfiles(ApplicationProfile.TEST)
@Transactional
public class SaveImageNoNameTest {
	
	@Resource
	private ImageRepository imageRepository;
	
	@Resource
	private BlobService blobService;
	
	@Test
	public void whenSaveImageWithNotImageNameThenCheckResult() throws Exception {
		Image image = new Image();
		image.setValue(blobService.createBlob(new byte[12]));
		image.setType(MediaType.IMAGE_JPEG_VALUE);
		image.setUploadDate(LocalDateTime.now());
		imageRepository.save(image);
		Assert.assertNotNull(image.getId());
	}
}
