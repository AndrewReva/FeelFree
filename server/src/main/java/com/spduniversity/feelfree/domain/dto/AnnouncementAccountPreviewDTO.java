package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashSet;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementAccountPreviewDTO {
	private String uuid;
	private String title;
	private String shortDescription;
	private AddressDTO address;
	private Favorite favorite;
	private PriceDTO price;
	private Boolean hidden;
	private Boolean rented;
	private LinkedHashSet<ImageUUIDDTO> images;
}
