package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeolocationSearchDTO {
	private String uuid;
	private Double price;
	private Double latitude;
	private Double longitude;
}
