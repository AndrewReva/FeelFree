package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "temporary_user")
@JsonFilter("temporaryUserFilter")
public class TemporaryUser implements Serializable {
    
    private static final long serialVersionUID = 8695250451963323238L;
    
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "token")
    private String token;

    @Column(name = "profileEmail")
    private String profileEmail;

    @Column(name = "firstName")
    @Length(max = 50)
    private String firstName;

    @Column(name = "lastName")
    @Length(max = 50)
    private String lastName;
    
    @Column(name = "password")
    private String password;
    
    @Column(name = "facebook_id")
    private String facebookId;
    
    @Column(name = "google_id")
    private String googleId;
    
    @Column(name = "expiry")
    private LocalDateTime expiryDate;
    
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    @JsonManagedReference
    private Image image;

    @Transient
    public String getFullName() {
        return (StringUtils.defaultIfBlank(firstName, StringUtils.EMPTY) + StringUtils.SPACE +
                StringUtils.defaultIfBlank(lastName, StringUtils.EMPTY)).trim();
    }
    
    @Transient
    public String getProfileEmailIfBlankFullName() {
        return StringUtils.isBlank(getFullName()) ? profileEmail : getFullName();
    }
    
    @PrePersist
    private void executePersist() {
        if (expiryDate == null) {
            expiryDate = LocalDateTime.now().plusDays(1);
        }
    }
}