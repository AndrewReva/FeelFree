package com.spduniversity.feelfree.domain.repository;

public interface SocialConnectionRepository {
	int deleteConnection(String providerId, String providerUserId);
}
