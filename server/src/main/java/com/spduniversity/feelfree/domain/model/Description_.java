package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Description.class)
public abstract class Description_ {

	public static volatile SingularAttribute<Description, Integer> roomCount;
	public static volatile SingularAttribute<Description, Boolean> shampoo;
	public static volatile SingularAttribute<Description, Boolean> wiFi;
	public static volatile SingularAttribute<Description, Boolean> tv;
	public static volatile SingularAttribute<Description, Boolean> kitchenStuff;
	public static volatile SingularAttribute<Description, Boolean> conditioner;
	public static volatile SingularAttribute<Description, Boolean> balcony;
	public static volatile SingularAttribute<Description, Integer> livingPlacesCount;
	public static volatile SingularAttribute<Description, Boolean> smoking;
	public static volatile SingularAttribute<Description, Boolean> iron;
	public static volatile SingularAttribute<Description, Boolean> essentials;
	public static volatile SingularAttribute<Description, Boolean> washingMachine;
	public static volatile SingularAttribute<Description, Long> id;
	public static volatile SingularAttribute<Description, Boolean> animals;
	public static volatile SingularAttribute<Description, Boolean> refrigerator;
	public static volatile SingularAttribute<Description, Boolean> hairDryer;
	public static volatile SingularAttribute<Description, Announcement> announcement;

}

