package com.spduniversity.feelfree.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
public class CommentDTO {
	
	@Setter
	private LocalDateTime updateDate;
	
	@Getter
	@Setter
	private String value;
	
	@Getter
	@Setter
	private UserFirstNameDTO user;
	
	@JsonProperty("date")
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
	public LocalDateTime getDate() {
		return updateDate;
	}
}
