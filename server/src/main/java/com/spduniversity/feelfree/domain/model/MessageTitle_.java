package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(MessageTitle.class)
public abstract class MessageTitle_ {

	public static volatile SingularAttribute<MessageTitle, Long> id;
	public static volatile SingularAttribute<MessageTitle, String> text;
	public static volatile SingularAttribute<MessageTitle, Message> message;

}

