package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashSet;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementAccountDTO {
	private String uuid;
	private String title;
	private String shortDescription;
	private AddressAccountDTO address;
	private DescriptionDTO description;
	private PriceDTO price;
	private GeolocationDTO geolocation;
	private LinkedHashSet<ImageUUIDDTO> images;
}
