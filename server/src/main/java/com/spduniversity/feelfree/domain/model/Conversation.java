
package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "conversation")
@EqualsAndHashCode(exclude = {"conversationUsers", "messages"})
@ToString(exclude = {"conversationUsers", "messages"})
public class Conversation implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "new_msg_time")
    private LocalDateTime messageTime;

    @ManyToMany(mappedBy = "conversations", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JsonBackReference
    private Set<User> conversationUsers = new LinkedHashSet<>();

    @OneToMany(mappedBy =  "conversation",fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @OrderBy("createDate ASC")
    private Set<Message> messages = new LinkedHashSet<>();




}

