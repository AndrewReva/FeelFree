package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(readOnly = true)
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {
	
	@Query("select u from User u where (u.profileEmail = :val or u.facebookId = :val or u.googleId = :val) and u.enabled = true")
	User findByUniqueValue(@Param(value = "val") String uniqueValue);

	@Query("select u from User u where u.profileEmail = :val")
	User findByProfileEmail(@Param(value = "val") String profileEmail);
 
	@Query("select new java.lang.Boolean(count(u.id) > 0) from User u where u.profileEmail = :email")
	boolean isReservedEmail(@Param("email") String email);
	
	@Query("select f.uuid from User u " +
		   "left join u.favorites as f " +
		   "where u.profileEmail = :email and u.enabled = true and f.hidden = false")
	List<String> findFavorites(@Param(value = "email") String profileEmail);
	
	@Query("select new java.lang.Boolean(count(u.id) <> 0) " +
		   "from User u where (u.profileEmail = :val or u.facebookId = :val or u.googleId = :val) and u.enabled = true")
	Boolean isAuthorize(@Param("val") String uniqueValue);
}
