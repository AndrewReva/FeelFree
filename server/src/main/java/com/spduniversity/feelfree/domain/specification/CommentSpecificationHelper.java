package com.spduniversity.feelfree.domain.specification;

import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.model.Announcement_;
import com.spduniversity.feelfree.domain.model.Comment;
import com.spduniversity.feelfree.domain.model.Comment_;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Path;

@Component
public class CommentSpecificationHelper {
	
	public Specification<Comment> equalAnnouncementUuid(String announcementUuid) {
		return (root, query, cb) -> {
			Path<Announcement> announcement = root.get(Comment_.announcement);
			return cb.equal(announcement.get(Announcement_.uuid), announcementUuid);
		};
	}
}
