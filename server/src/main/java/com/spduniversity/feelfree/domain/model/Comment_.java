package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Comment.class)
public abstract class Comment_ {

	public static volatile SingularAttribute<Comment, LocalDateTime> updateDate;
	public static volatile SingularAttribute<Comment, Long> id;
	public static volatile SingularAttribute<Comment, String> value;
	public static volatile SingularAttribute<Comment, User> user;
	public static volatile SingularAttribute<Comment, LocalDateTime> createDate;
	public static volatile SingularAttribute<Comment, Announcement> announcement;

}

