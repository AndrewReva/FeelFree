package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.TemporaryUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Transactional(readOnly = true)
public interface TemporaryUserRepository extends JpaRepository<TemporaryUser, Long> {

    TemporaryUser findByToken(String token);

    TemporaryUser findByProfileEmail(String email);
    
    @Query("select new java.lang.Boolean(count(u.id) <> 0) " +
           "from TemporaryUser u where (u.facebookId = :val or u.googleId = :val) and u.expiryDate > :date")
    Boolean isExist(@Param("val") String uniqueValue, @Param("date") LocalDateTime expiryDate);
    
    @Modifying
    @Transactional
    @Query("delete from TemporaryUser u where u.expiryDate < :date")
    void delete(@Param("date") LocalDateTime date);
    
}
