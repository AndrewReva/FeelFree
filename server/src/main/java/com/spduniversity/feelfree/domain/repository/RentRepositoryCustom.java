package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Rent;
import com.spduniversity.feelfree.domain.repository.core.JpaSpecificationExecutorMapper;

public interface RentRepositoryCustom extends JpaSpecificationExecutorMapper<Rent> {}
