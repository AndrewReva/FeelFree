package com.spduniversity.feelfree.domain.repository.impl;

import com.spduniversity.feelfree.domain.repository.ImageCacheRepositoryCustom;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional
public class ImageCacheRepositoryImpl implements ImageCacheRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public boolean isExistImage(String name, int width, int height) {
		List result = entityManager.createNativeQuery("SELECT 1 FROM image_cache i " +
													  "WHERE i.name = :name and i.height = :height and width = :width")
								   .setParameter("name", name)
								   .setParameter("height", height)
								   .setParameter("width", width)
								   .getResultList();
		return result.size() == 1;
	}
}
