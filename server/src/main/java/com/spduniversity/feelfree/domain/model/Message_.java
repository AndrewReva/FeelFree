package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Message.class)
public abstract class Message_ {

	public static volatile SingularAttribute<Message, Long> id;
	public static volatile SingularAttribute<Message, String> text;
	public static volatile SingularAttribute<Message, MessageTitle> title;
	public static volatile SingularAttribute<Message, User> user;
	public static volatile SingularAttribute<Message, Conversation> conversation;
	public static volatile SingularAttribute<Message, LocalDateTime> createDate;

}

