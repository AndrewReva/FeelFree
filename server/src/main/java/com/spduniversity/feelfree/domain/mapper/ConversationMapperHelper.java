package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.dto.ConversationDTO;
import com.spduniversity.feelfree.domain.dto.MessageDTO;
import com.spduniversity.feelfree.domain.model.Conversation;
import com.spduniversity.feelfree.domain.model.Message;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ConversationMapperHelper {


    public Function<Set<Conversation>, Set<ConversationDTO>> toSetOfConversationDTO() {
        return conversations -> {
            return conversations.stream().map(toConversationDTO())
                    .collect(Collectors.toCollection(LinkedHashSet::new));
        };
    }

    public Function<Conversation, ConversationDTO> toConversationDTO() {
        return conversation -> {
            ConversationDTO result = new ConversationDTO();
            result.setId(conversation.getId());
            result.setLastMessageTime(conversation.getMessageTime());
            result.setMessages(toMessageDTO().apply(conversation.getMessages()));
            result.setConversationTitle(new ArrayList<>(result.getMessages()).get(0).getTitle());

            return result;
        };
    }

    private Function<Collection<Message>, Set<MessageDTO>> toMessageDTO() {
        return messages -> {
            LinkedHashSet<MessageDTO> collect = messages.stream()
                    .map(this::convertToMessageDTO)
                    .collect(Collectors.toCollection(LinkedHashSet::new));
            return changeTitle(collect);
        };
    }

    public MessageDTO convertToMessageDTO(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setCreateMessageTime(message.getCreateDate());
        messageDTO.setFullName(message.getUser().getFullName());
        messageDTO.setText(message.getText());
        messageDTO.setTitle(message.getTitle().getText());
        messageDTO.setConversationId(message.getConversation().getId());
        return messageDTO;
    }
    private Set<MessageDTO> changeTitle (Set<MessageDTO>dto){
        Set<MessageDTO>perfectList= new LinkedHashSet<>();
     List<MessageDTO> messageDTOs = new ArrayList<>(dto);

        for (int i = 0; i < messageDTOs.size();i++){
            MessageDTO oldDto = messageDTOs.get(i);
            MessageDTO newDto = new MessageDTO();
            newDto.setConversationId(oldDto.getConversationId());
            newDto.setText(oldDto.getText());
            newDto.setCreateMessageTime(oldDto.getCreateMessageTime());
            newDto.setFullName(oldDto.getFullName());

            if(i == 0){
                newDto.setTitle(oldDto.getTitle());
                perfectList.add(newDto);
                continue;
            }

            if(messageDTOs.get(i-1).getTitle().equals(messageDTOs.get(i).getTitle())){
                newDto.setTitle("");
                perfectList.add(newDto);
            }
            else { newDto.setTitle(oldDto.getTitle());
                   perfectList.add(newDto);
            }


        }
        return perfectList;
    }


}
