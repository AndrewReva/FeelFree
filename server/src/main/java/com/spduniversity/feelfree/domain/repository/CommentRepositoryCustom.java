package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Comment;
import com.spduniversity.feelfree.domain.repository.core.JpaSpecificationExecutorMapper;

public interface CommentRepositoryCustom extends JpaSpecificationExecutorMapper<Comment> {}
