package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.dto.*;
import com.spduniversity.feelfree.domain.model.*;
import com.spduniversity.feelfree.service.common.ImageNameService;
import com.spduniversity.feelfree.service.common.extra.EntityMapper;
import com.spduniversity.feelfree.web.bean.SaveAnnouncementContainer;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class AnnouncementMapperHelper {
	
	@Resource
	private EntityMapper objectMapper;
	
	@Resource
	private ImageNameService imageNameService;
	
	@Resource
	private RentMapperHelper rentMapper;
	
	public Function<Announcement, AnnouncementSearchSingleDTO> toSearchAnnouncementDTO() {
		return announcement -> {
			AnnouncementSearchSingleDTO result = objectMapper.convertEntityValue(announcement, AnnouncementSearchSingleDTO.class);
			imageNameService.setFullNameIntoImage(result.getImages());
			imageNameService.setFullNameIntoImage(result.getUser().getImage());
			result.setRented(rentMapper.toRentedListDTO().apply(announcement.getRents()));
			return result;
		};
	}
	
	public Function<Announcement, AnnouncementSearchDTO> toSearchAnnouncementsDTO() {
		return announcement -> {
			AnnouncementSearchDTO result = objectMapper.convertEntityValue(announcement, AnnouncementSearchDTO.class);
			imageNameService.setFullNameIntoImage(result.getImages());
			result.setFavorite(Favorite.UNDEFINED);
			return result;
		};
	}

	public Function<Announcement, AnnouncementSearchDTO> toSearchAnnouncementsDTO(List<String> uuidsFavorites) {
		return announcement -> {
			AnnouncementSearchDTO result = objectMapper.convertEntityValue(announcement, AnnouncementSearchDTO.class);
			imageNameService.setFullNameIntoImage(result.getImages());
			result.setFavorite(uuidsFavorites.contains(result.getUuid()) ? Favorite.TRUE : Favorite.FALSE);
			return result;
		};
	}
	
	public Function<Announcement, AnnouncementAccountPreviewDTO> toAccountAnnouncementPreviewDTO(List<String> uuidsFavorites) {
		return announcement -> {
			AnnouncementAccountPreviewDTO result = objectMapper.convertEntityValue(announcement, AnnouncementAccountPreviewDTO.class);
			imageNameService.setNameUuidIntoImage(result.getImages());
			LocalDateTime now = LocalDateTime.now();
			result.setFavorite(uuidsFavorites.contains(result.getUuid()) ? Favorite.TRUE : Favorite.FALSE);
			result.setRented(announcement.getRents().stream().anyMatch(rent -> rent.getCheckOutDate().isAfter(now)));
			return result;
		};
	}

	public Function<Announcement, AnnouncementAccountFavoriteDTO> toAccountAnnouncementFavoriteDTO() {
		return announcement -> {
			AnnouncementAccountFavoriteDTO result = objectMapper.convertEntityValue(announcement, AnnouncementAccountFavoriteDTO.class);
			imageNameService.setFullNameIntoImage(result.getImages());
			return result;
		};
	}
	
	public Function<SaveAnnouncementContainer, Announcement> toAnnouncement() {
		return container -> {
			Announcement announcement = createAnnouncement(container);
			createSaveData(container, announcement);
			return announcement;
		};
	}
	
	public Function<SaveAnnouncementContainer, Announcement> toAnnouncement(Announcement announcement) {
		return container -> {
			setAnnouncementData(container, announcement);
			setSaveData(container, announcement);
			return announcement;
		};
	}
	
	public Function<Announcement, AnnouncementAccountDTO> toAccountAnnouncementDTO() {
		return announcement -> {
			AnnouncementAccountDTO result = objectMapper.convertEntityValue(announcement, AnnouncementAccountDTO.class);
			LinkedHashSet<ImageUUIDDTO> imageUUIDDTOS = announcement.getImages()
														  .stream()
														  .map(e -> new ImageUUIDDTO(e.getName(), imageNameService.getFullImageName(e.getName())))
														  .collect(Collectors.toCollection(LinkedHashSet::new));
			result.setImages(imageUUIDDTOS);
			return result;
		};
	}
	
	public Function<Announcement, UUIDDTO> toUUIDDTO() {
		return announcement -> new UUIDDTO(announcement.getUuid());
	}
	
	private void setSaveData(SaveAnnouncementContainer container, Announcement announcement) {
		setPriceData(container, announcement.getPrice());
		setAddressData(container, announcement.getAddress());
		setDescriptionData(container, announcement.getDescription());
		setGeolocationData(container, announcement.getGeolocation());
	}
	
	private void createSaveData(SaveAnnouncementContainer container, Announcement announcement) {
		Price price = createPrice(container);
		Description description = createDescription(container);
		Address address = createAddress(container);
		Geolocation geolocation = createGeolocation(container);
		
		announcement.setAddress(address);
		announcement.setGeolocation(geolocation);
		announcement.setDescription(description);
		announcement.setPrice(price);
		
		address.setAnnouncements(announcement);
		geolocation.setAnnouncement(announcement);
		description.setAnnouncement(announcement);
		price.setAnnouncement(announcement);
	}
	
	private Geolocation createGeolocation(SaveAnnouncementContainer container) {
		Geolocation geolocation = new Geolocation();
		setGeolocationData(container, geolocation);
		return geolocation;
	}
	
	private void setGeolocationData(SaveAnnouncementContainer container, Geolocation geolocation) {
		BeanUtils.copyProperties(container, geolocation);
	}
	
	private Address createAddress(SaveAnnouncementContainer container) {
		Address address = new Address();
		setAddressData(container, address);
		return address;
	}
	
	private void setAddressData(SaveAnnouncementContainer container, Address address) {
		BeanUtils.copyProperties(container, address);
	}
	
	private Description createDescription(SaveAnnouncementContainer container) {
		Description description = new Description();
		setDescriptionData(container, description);
		return description;
	}
	
	private void setDescriptionData(SaveAnnouncementContainer container, Description description) {
		BeanUtils.copyProperties(container, description);
	}
	
	private Price createPrice(SaveAnnouncementContainer container) {
		Price price = new Price();
		setPriceData(container, price);
		return price;
	}
	
	private void setPriceData(SaveAnnouncementContainer container, Price price) {
		price.setPeriod(container.getPeriod());
		price.setValue(container.getPrice());
	}
	
	private Announcement createAnnouncement(SaveAnnouncementContainer container) {
		Announcement announcement = new Announcement();
		setAnnouncementData(container, announcement);
		return announcement;
	}
	
	private void setAnnouncementData(SaveAnnouncementContainer container, Announcement announcement) {
		announcement.setTitle(container.getTitle());
		announcement.setShortDescription(container.getShortDescription());
	}
}
