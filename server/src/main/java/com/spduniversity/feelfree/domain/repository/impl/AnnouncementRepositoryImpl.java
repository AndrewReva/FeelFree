package com.spduniversity.feelfree.domain.repository.impl;

import com.spduniversity.feelfree.domain.dto.GeolocationSearchDTO;
import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.repository.AnnouncementRepositoryCustom;
import com.spduniversity.feelfree.domain.repository.core.AbstractInnerRepository;
import com.spduniversity.feelfree.service.domain.bean.GeolocationContainer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;

@Repository
@Transactional
public class AnnouncementRepositoryImpl extends AbstractInnerRepository<Announcement, Long> implements AnnouncementRepositoryCustom {
	
	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	@Nullable
	public Page<GeolocationSearchDTO> findGeolocationInfo(@Nonnull GeolocationContainer geolocationContainer,
														  @Nonnull Integer offSet,
														  @Nonnull Integer rows) {
		String geoQuery = "SELECT a.uuid as uuid, " +
						  "p.value as price, " +
						  "g.latitude as latitude, " +
						  "g.longitude as longitude " +
						  "FROM announcement a " +
						  "JOIN price p ON a.price_id = p.id " +
						  "JOIN geolocation g  ON a.geolocation_id = g.id " +
						  "WHERE (:lgthVal * acos(cos(radians(:lat)) * cos(radians(g.latitude)) * cos(radians(g.longitude) - radians(:lng)) + sin(radians(:lat)) * sin(radians(g.latitude)))) <= :rds " +
						  "AND a.hidden = 0 " +
						  "ORDER BY a.create_date";
		
		List<GeolocationSearchDTO> result = (List<GeolocationSearchDTO>) entityManager.createNativeQuery(geoQuery, "SearchGeolocationDTOMapper")
																					  .setFirstResult(offSet * rows)
																					  .setMaxResults(rows)
																					  .setParameter("lat", geolocationContainer.getLatitude())
																					  .setParameter("lng", geolocationContainer.getLongitude())
																					  .setParameter("rds", geolocationContainer.getRadius())
																					  .setParameter("lgthVal", geolocationContainer.getLength().get())
																					  .getResultList();
		
		if (result.isEmpty()) {
			return new PageImpl<>(result, new PageRequest(offSet, rows), 0);
		}
		BigInteger count = findGeolocationCount(geolocationContainer);
		return new PageImpl<>(result, new PageRequest(offSet, rows), count.longValue());
	}
	
	private BigInteger findGeolocationCount(GeolocationContainer geolocationContainer) {
		String geoCountQuery = "SELECT COUNT(a.id) as c " +
 							   "FROM announcement a " +
							   "JOIN geolocation g  ON a.geolocation_id = g.id " +
							   "WHERE (:lgthVal * acos(cos(radians(:lat)) * cos(radians(g.latitude)) * cos(radians(g.longitude) - radians(:lng)) + sin(radians(:lat)) * sin(radians(g.latitude)))) <= :rds " +
							   "AND a.hidden = 0";
		
		
		try {
			return (BigInteger) entityManager.createNativeQuery(geoCountQuery)
											 .setParameter("lat", geolocationContainer.getLatitude())
											 .setParameter("lng", geolocationContainer.getLongitude())
											 .setParameter("rds", geolocationContainer.getRadius())
											 .setParameter("lgthVal", geolocationContainer.getLength().get())
											 .getSingleResult();
		} catch (Exception ex) {
			return BigInteger.ZERO;
		}
	}
}
