package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashSet;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnnouncementSearchSingleDTO {
	private String uuid;
	private String title;
	private String shortDescription;
	private AddressDTO address;
	private DescriptionDTO description;
	private PriceDTO price;
	private List<RentedDTO> rented;
	private GeolocationDTO geolocation;
	private LinkedHashSet<ImageDTO> images;
	private UserDTO user;
}
