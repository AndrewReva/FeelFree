package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@EqualsAndHashCode(exclude = "announcement")
@ToString(exclude = "announcement")
@Entity
@Table(name = "description")
@JsonFilter("descriptionFilter")
public class Description implements Serializable{
	
	private static final long serialVersionUID = -1391486543013883700L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "room_count", columnDefinition = "TINYINT", length = 3)
	private Integer roomCount;
	
	@Column(name = "living_places_count", columnDefinition = "TINYINT", length = 3)
	private Integer livingPlacesCount;
	
	@Column(name = "wi_fi", columnDefinition = "TINYINT", length = 1)
	private Boolean wiFi = Boolean.FALSE;
	
	@Column(name = "washing_machine", columnDefinition = "TINYINT", length = 1)
	private Boolean washingMachine = Boolean.FALSE;
	
	@Column(name = "refrigerator", columnDefinition = "TINYINT", length = 1)
	private Boolean refrigerator = Boolean.FALSE;
	
	@Column(name = "hair_dryer", columnDefinition = "TINYINT", length = 1)
	private Boolean hairDryer = Boolean.FALSE;
	
	@Column(name = "iron", columnDefinition = "TINYINT", length = 1)
	private Boolean iron = Boolean.FALSE;
	
	@Column(name = "smoking", columnDefinition = "TINYINT", length = 1)
	private Boolean smoking = Boolean.FALSE;
	
	@Column(name = "animals", columnDefinition = "TINYINT", length = 1)
	private Boolean animals = Boolean.FALSE;
	
	@Column(name = "kitchen_stuff", columnDefinition = "TINYINT", length = 1)
	private Boolean kitchenStuff = Boolean.FALSE;
	
	@Column(name = "conditioner", columnDefinition = "TINYINT", length = 1)
	private Boolean conditioner = Boolean.FALSE;
	
	@Column(name = "balcony", columnDefinition = "TINYINT", length = 1)
	private Boolean balcony = Boolean.FALSE;
	
	@Column(name = "tv", columnDefinition = "TINYINT", length = 1)
	private Boolean tv = Boolean.FALSE;
	
	@Column(name = "essentials", columnDefinition = "TINYINT", length = 1)
	private Boolean essentials = Boolean.FALSE;
	
	@Column(name = "shampoo", columnDefinition = "TINYINT", length = 1)
	private Boolean shampoo = Boolean.FALSE;
	
	@OneToOne(mappedBy = "description")
	@JsonBackReference
	private Announcement announcement;
}
