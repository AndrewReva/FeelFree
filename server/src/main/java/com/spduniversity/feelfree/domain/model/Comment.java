package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(exclude = {"user", "announcement"})
@ToString(exclude = {"user", "announcement"})
@Entity
@Table(name = "comment")
@JsonFilter("commentFilter")
public class Comment implements Serializable{
	
	private static final long serialVersionUID = 23686095247408808L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "create_date")
	private LocalDateTime createDate;
	
	@Column(name = "update_date")
	private LocalDateTime updateDate;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@JsonManagedReference
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "announcement_comment", joinColumns = @JoinColumn(name = "comment_id"),
			   inverseJoinColumns = @JoinColumn(name = "announcement_id")
	)
	@JsonBackReference
	private Announcement announcement;
	
	@PrePersist
	private void executePersist() {
		if (createDate == null) {
			createDate = LocalDateTime.now();
		}
		
		if (updateDate == null) {
			updateDate = LocalDateTime.now();
		}
	}
	
	@PreUpdate
	private void executeUpdate() {
		updateDate = LocalDateTime.now();
	}
}
