package com.spduniversity.feelfree.domain.model;

import com.spduniversity.feelfree.domain.model.Rent.Status;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Rent.class)
public abstract class Rent_ {

	public static volatile SingularAttribute<Rent, LocalDateTime> checkOutDate;
	public static volatile SingularAttribute<Rent, Long> id;
	public static volatile SingularAttribute<Rent, LocalDateTime> checkInDate;
	public static volatile SingularAttribute<Rent, User> user;
	public static volatile SingularAttribute<Rent, Status> status;
	public static volatile SingularAttribute<Rent, Announcement> announcement;

}

