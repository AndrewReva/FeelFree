package com.spduniversity.feelfree.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@AllArgsConstructor
@NoArgsConstructor
public class UserFirstNameDTO {
	
	@Setter
	private String firstName;
	
	@Setter
	private String profileEmail;
	
	@Getter
	@Setter
	private ImageDTO image;
	
	@JsonProperty("firstName")
	public String getFirstName() {
		return StringUtils.isBlank(firstName) ? StringUtils.substringBefore(profileEmail, "@") : firstName;
	}
}
