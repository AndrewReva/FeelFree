package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository  extends JpaRepository<Role, Long> {
   Role findByValue(String role);

}
