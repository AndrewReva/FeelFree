package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Conversation;
import com.spduniversity.feelfree.domain.model.User;

public interface ConversationRepositoryCustom {
    Conversation findConversationByUsers(User user1, User user2);
}
