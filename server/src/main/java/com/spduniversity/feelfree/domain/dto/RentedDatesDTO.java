package com.spduniversity.feelfree.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.spduniversity.feelfree.domain.model.Rent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RentedDatesDTO {
	private Long id;
	@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "UTC")
	private List<LocalDateTime> rents;
	private Rent.Status status;
	private UserFirstNameDTO user;
}
