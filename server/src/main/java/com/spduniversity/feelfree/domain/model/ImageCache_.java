package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.sql.Blob;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ImageCache.class)
public abstract class ImageCache_ {

	public static volatile SingularAttribute<ImageCache, LocalDateTime> uploadDate;
	public static volatile SingularAttribute<ImageCache, String> name;
	public static volatile SingularAttribute<ImageCache, Integer> width;
	public static volatile SingularAttribute<ImageCache, Long> id;
	public static volatile SingularAttribute<ImageCache, String> type;
	public static volatile SingularAttribute<ImageCache, Blob> value;
	public static volatile SingularAttribute<ImageCache, Integer> height;

}

