package com.spduniversity.feelfree.domain.dto;

public interface MinMaxPriceDTO {
	Double getMinPrice();
	Double getMaxPrice();
}
