package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Data
@EqualsAndHashCode(exclude = "announcement")
@ToString(exclude = "announcement")
@Entity
@Table(name = "price")
@JsonFilter("priceFilter")
public class Price implements Serializable {
	
	private static final long serialVersionUID = -4628192134187539129L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "value")
	private Integer value;
	
	@Column(name = "period")
	@Enumerated(EnumType.STRING)
	private Period period;
	
	@OneToOne(mappedBy = "price")
	@JsonBackReference
	private Announcement announcement;
	
	public enum Period {
		DAY, MONTH
	}
}
