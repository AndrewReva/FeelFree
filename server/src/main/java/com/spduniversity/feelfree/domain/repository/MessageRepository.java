package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Conversation;
import com.spduniversity.feelfree.domain.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface MessageRepository extends JpaRepository<Message, Long> {

    Set<Message> findByConversation(Conversation conversation);

    Message findFirstByConversationOrderByCreateDateDesc(Conversation conversation);
}
