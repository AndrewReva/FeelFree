package com.spduniversity.feelfree.domain.repository.impl;

import com.spduniversity.feelfree.domain.repository.SocialConnectionRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional
@Repository
public class SocialConnectionRepositoryDefault implements SocialConnectionRepository {
	
	@PersistenceContext(unitName = "entityManagerFactory")
	private EntityManager entityManager;
	
	@Override
	public int deleteConnection(String providerId, String providerUserId) {
		String query = "delete from UserConnection where providerId = :providerId and providerUserId = :providerUserId";
		return entityManager.createNativeQuery(query)
							.setParameter("providerId", providerId)
							.setParameter("providerUserId", providerUserId)
							.executeUpdate();
	}
}
