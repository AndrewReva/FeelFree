package com.spduniversity.feelfree.domain.dto;

import com.spduniversity.feelfree.domain.model.Price;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceDTO {
	private Integer value;
	private Price.Period period;
}
