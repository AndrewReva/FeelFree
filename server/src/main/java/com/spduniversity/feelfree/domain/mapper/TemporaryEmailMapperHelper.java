package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.model.TemporaryEmail;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.service.common.util.HashGeneratorUtil;
import com.spduniversity.feelfree.service.web.bean.UserContainer;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class TemporaryEmailMapperHelper {
	
	public Function<String, TemporaryEmail> toTemporaryEmail() {
		return value -> {
			TemporaryEmail temporaryEmail = new TemporaryEmail();
			temporaryEmail.setValue(value);
			temporaryEmail.setToken(HashGeneratorUtil.generateToken());
			return temporaryEmail;
		};
	}
	
	public Function<String, TemporaryEmail> toTemporaryEmail(User user) {
		return value -> {
			TemporaryEmail temporaryEmail = toTemporaryEmail().apply(value);
			temporaryEmail.setUser(user);
			return temporaryEmail;
		};
	}
	
	public Function<TemporaryEmail, UserContainer> toUserContainer() {
		return temporaryEmail -> new UserContainer() {
			@Getter private final String firstName = temporaryEmail.getUser().getFirstName();
			@Getter private final String lastName = temporaryEmail.getUser().getLastName();
			@Getter private final String profileEmail = temporaryEmail.getValue();
			@Getter private final String password = null;
		};
	}
}
