package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.dto.ImageDTO;
import com.spduniversity.feelfree.domain.dto.RentedDTO;
import com.spduniversity.feelfree.domain.dto.RentedDatesDTO;
import com.spduniversity.feelfree.domain.dto.UserFirstNameDTO;
import com.spduniversity.feelfree.domain.model.Rent;
import com.spduniversity.feelfree.service.common.ImageNameService;
import com.spduniversity.feelfree.web.bean.AccountRentContainer;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class RentMapperHelper {
	
	@Resource
	private ImageNameService imageNameService;
	
	public Function<Collection<Rent>, List<RentedDTO>> toRentedListDTO() {
		return rents -> {
			LocalDateTime firstDayOfMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();
			return rents.stream()
						.filter(rent -> rent.getCheckOutDate().isAfter(firstDayOfMonth) && Rent.Status.CONFIRMED.equals(rent.getStatus()))
						.map(this::convertToRentedDTO)
						.collect(Collectors.toList());
		};
	}
	
	public Function<Rent, RentedDatesDTO> toRentedDatesDTO() {
		return rent -> {
			RentedDatesDTO rentedDatesDTO = new RentedDatesDTO();
			rentedDatesDTO.setRents(getRangeStartMonth(rent.getCheckInDate(), rent.getCheckOutDate()));
			rentedDatesDTO.setId(rent.getId());
			rentedDatesDTO.setStatus(rent.getStatus());
			rentedDatesDTO.setUser(covertToUser(rent));
			return rentedDatesDTO;
		};
	}
	
	private RentedDTO convertToRentedDTO(Rent rent) {
		RentedDTO rentedDTO = new RentedDTO();
		rentedDTO.setRents(getRangeStartMonth(rent.getCheckInDate(), rent.getCheckOutDate()));
		rentedDTO.setUser(covertToUser(rent));
		return rentedDTO;
	}
	
	private UserFirstNameDTO covertToUser(Rent rent) {
		UserFirstNameDTO userDTO = new UserFirstNameDTO();
		userDTO.setFirstName(rent.getUser().getFirstName());
		userDTO.setProfileEmail(rent.getUser().getProfileEmail());
		if(rent.getUser().getImage()!=null) {
			userDTO.setImage(new ImageDTO(imageNameService.getFullImageName(rent.getUser().getImage().getName())));
		}
		return userDTO;
	}
	
	private List<LocalDateTime> getRangeStartMonth(LocalDateTime firstDay, LocalDateTime lastDay) {
		if (lastDay.getMonth().getValue() > firstDay.getMonth().getValue()) {
			firstDay = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).atStartOfDay();
		}
		LocalDate nextDay = firstDay.plusDays(1).toLocalDate();
		int maxSize = lastDay.getDayOfMonth() - nextDay.getDayOfMonth();
		LinkedList<LocalDateTime> collect = Stream.iterate(nextDay, localDate -> localDate.plusDays(1))
												  .limit(maxSize)
												  .map(LocalDate::atStartOfDay)
												  .collect(Collectors.toCollection(LinkedList::new));
		collect.addFirst(firstDay);
		collect.addLast(lastDay);
		
		return collect;
	}
	
	public Function<AccountRentContainer, Rent> toRent() {
		return container -> {
			Rent rent = new Rent();
			rent.setCheckInDate(container.getCheckIn());
			rent.setCheckOutDate(container.getCheckOut());
			rent.setStatus(Rent.Status.WAITING);
			return rent;
		};
	}
}
