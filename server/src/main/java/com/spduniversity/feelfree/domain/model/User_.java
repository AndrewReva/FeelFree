package com.spduniversity.feelfree.domain.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.time.LocalDateTime;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(User.class)
public abstract class User_ {

	public static volatile SingularAttribute<User, String> googleId;
	public static volatile SetAttribute<User, Announcement> favorites;
	public static volatile SingularAttribute<User, String> lastName;
	public static volatile SingularAttribute<User, Image> image;
	public static volatile SingularAttribute<User, LocalDateTime> updateDate;
	public static volatile SetAttribute<User, Comment> comments;
	public static volatile SingularAttribute<User, String> facebookId;
	public static volatile SetAttribute<User, Role> roles;
	public static volatile SetAttribute<User, Phone> phones;
	public static volatile SetAttribute<User, Rent> rents;
	public static volatile SetAttribute<User, Conversation> conversations;
	public static volatile SingularAttribute<User, Boolean> enabled;
	public static volatile SetAttribute<User, Email> emails;
	public static volatile SingularAttribute<User, String> firstName;
	public static volatile SingularAttribute<User, String> profileEmail;
	public static volatile SingularAttribute<User, String> password;
	public static volatile SingularAttribute<User, Long> id;
	public static volatile SetAttribute<User, Announcement> announcements;
	public static volatile SingularAttribute<User, LocalDateTime> createDate;

}

