package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Comment;
import com.spduniversity.feelfree.domain.repository.core.JpaInnerRepository;

public interface CommentRepositoryInner extends JpaInnerRepository<Comment, Long> {}
