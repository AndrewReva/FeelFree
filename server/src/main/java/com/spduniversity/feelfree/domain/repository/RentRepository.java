package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.Rent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RentRepository extends JpaRepository<Rent, Long>, RentRepositoryCustom {
	
	@Query("select r from Rent r join r.announcement a where a.uuid = :uuid")
	List<Rent> findAll(@Param("uuid") String announcementUUID);
	
	@Query("select r from Rent r join r.announcement a " +
		   "where r.id = :rentId and " +
		   "a.user.profileEmail = :profileEmail and " +
		   "a.uuid = :uuid")
	Rent findOne(@Param("uuid") String announcementUUID, @Param("rentId") Long id, @Param("profileEmail") String email);
}
