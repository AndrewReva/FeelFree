package com.spduniversity.feelfree.domain.repository.impl;

import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.UserRepositoryCustom;
import com.spduniversity.feelfree.domain.repository.core.AbstractInnerRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class UserRepositoryImpl extends AbstractInnerRepository<User, Long> implements UserRepositoryCustom {
}
