package com.spduniversity.feelfree.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Data
@EqualsAndHashCode(exclude = {"announcement"})
@ToString(exclude = {"announcement"})
@Entity
@Table(name = "geolocation")
@JsonFilter("geolocationFilter")
public class Geolocation implements Serializable {
	
	private static final long serialVersionUID = 3139782126954165742L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name = "latitude", nullable = false, precision = 6)
	private Double latitude;
	
	@Column(name = "longitude", nullable = false, precision = 6)
	private Double longitude;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "geolocation", orphanRemoval = true, cascade = CascadeType.REMOVE)
	@JsonBackReference
	private Announcement announcement;
}
