package com.spduniversity.feelfree.domain.repository.impl;

import com.spduniversity.feelfree.domain.model.Comment;
import com.spduniversity.feelfree.domain.repository.CommentRepositoryCustom;
import com.spduniversity.feelfree.domain.repository.core.AbstractInnerRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CommentRepositoryImpl extends AbstractInnerRepository<Comment, Long> implements CommentRepositoryCustom {}
