package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeolocationDTO {
	private Double latitude;
	private Double longitude;
}
