package com.spduniversity.feelfree.domain.mapper;

import com.spduniversity.feelfree.domain.dto.CommentDTO;
import com.spduniversity.feelfree.domain.model.Comment;
import com.spduniversity.feelfree.service.common.ImageNameService;
import com.spduniversity.feelfree.service.common.extra.EntityMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.function.Function;

@Component
public class CommentMapperHelper {
	
	@Resource
	private EntityMapper objectMapper;
	
	@Resource
	private ImageNameService imageNameService;
	
	public <T> Function<T, Comment> toComment() {
		return container -> {
			Comment comment = new Comment();
			BeanUtils.copyProperties(container, comment);
			return comment;
		};
	}
	
	public Function<Comment, CommentDTO> toCommentDTO() {
		return container -> {
			CommentDTO result = objectMapper.convertEntityValue(container, CommentDTO.class);
			imageNameService.setFullNameIntoImage(result.getUser().getImage());
			return result;
		};
	}
}
