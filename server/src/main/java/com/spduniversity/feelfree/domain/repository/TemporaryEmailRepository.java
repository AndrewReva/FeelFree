package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.model.TemporaryEmail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TemporaryEmailRepository extends JpaRepository<TemporaryEmail, Long> {
	
	@Query("select te from TemporaryEmail te left join fetch te.user where te.token = :token")
	TemporaryEmail findByToken(@Param("token") String token);
}
