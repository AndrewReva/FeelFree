package com.spduniversity.feelfree.domain.repository;

import com.spduniversity.feelfree.domain.dto.MinMaxPriceDTO;
import com.spduniversity.feelfree.domain.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PriceRepository extends JpaRepository<Price, Long> {
	
	@Query("select min(p.value) as minPrice, max(p.value) as maxPrice from Price p")
	MinMaxPriceDTO findMinMaxPrice();
}
