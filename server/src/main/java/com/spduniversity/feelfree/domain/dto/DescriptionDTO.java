package com.spduniversity.feelfree.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DescriptionDTO {
	private Integer roomCount;
	private Integer livingPlacesCount;
	private Boolean wiFi;
	private Boolean washingMachine;
	private Boolean refrigerator;
	private Boolean hairDryer;
	private Boolean iron;
	private Boolean smoking;
	private Boolean animals;
	private Boolean kitchenStuff;
	private Boolean conditioner;
	private Boolean balcony;
	private Boolean tv;
	private Boolean essentials;
	private Boolean shampoo;
}
