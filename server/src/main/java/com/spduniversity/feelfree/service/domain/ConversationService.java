package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.dto.ConversationDTO;
import com.spduniversity.feelfree.domain.model.Conversation;
import com.spduniversity.feelfree.domain.model.Message;
import com.spduniversity.feelfree.domain.model.MessageTitle;
import com.spduniversity.feelfree.domain.model.User;

import java.util.Set;

public interface ConversationService {

    Conversation getConversation(User user1, User user2);

    void saveMessage(Message message);

    void saveConversation(Conversation conversation);

    MessageTitle getTitleFromLastMessage(Conversation conversation);

    Conversation getConversationById(Long id);

   Set<ConversationDTO> getUserConversations(User user);

}
