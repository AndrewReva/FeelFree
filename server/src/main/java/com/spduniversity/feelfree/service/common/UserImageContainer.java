package com.spduniversity.feelfree.service.common;

import com.spduniversity.feelfree.domain.model.Image;

public interface UserImageContainer {
	Image getImage();
	void setImage(Image image);
}
