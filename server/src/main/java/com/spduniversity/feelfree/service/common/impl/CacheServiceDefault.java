package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.domain.model.ImageCache;
import com.spduniversity.feelfree.service.common.CacheService;
import com.spduniversity.feelfree.service.common.bean.CacheContainer;
import com.spduniversity.feelfree.service.domain.ImageCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.concurrent.Future;

@Slf4j
@Service
public class CacheServiceDefault implements CacheService {
	
	@Resource
	private ImageCacheService imageCacheService;
	
	@Override
	@Async
	public Future<Boolean> createCache(@Nullable CacheContainer container) {
		if (container == null) {
			return new AsyncResult<>(Boolean.FALSE);
		}
		return new AsyncResult<>(saveImage(container));
	}
	
	private boolean saveImage(CacheContainer container) {
		ImageCache imageCache = new ImageCache();
		BeanUtils.copyProperties(container, imageCache);
		if (!trySaveImage(imageCache)) {
			trySaveImage(imageCache);
		}
		return imageCache.getId() != null;
	}

	private boolean trySaveImage(ImageCache imageCache) {
		try {
			ImageCache result = imageCacheService.saveImage(imageCache);
			return (result != null ? result.getId() : null) != null;
		} catch (DataIntegrityViolationException ex) {
			log.error(ex.getMessage());
			deleteCache(imageCache.getName(), imageCache.getWidth(), imageCache.getHeight());
			return false;
		} catch (Exception ex) {
			log.error(ex.getMessage());
			return false;
		}
	}
	
	@Override
	public void deleteCache(String name, int with, int height) {
		imageCacheService.delete(name, with, height);
	}
}