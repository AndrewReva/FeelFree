package com.spduniversity.feelfree.service.common.extra;

import org.springframework.context.MessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;

public class MessageSourceSingleton {
	
	private MessageSourceSingleton() {}
	
	public static MessageSource get() {
		return Inner.getInstance();
	}
	
	private static class Inner {
		private static MessageSource instance = null;
		
		private static MessageSource getInstance() {
			if (instance == null) {
				ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
				messageSource.setBasename("i18n/messages");
				instance = messageSource;
			}
			return instance;
		}
	}
}
