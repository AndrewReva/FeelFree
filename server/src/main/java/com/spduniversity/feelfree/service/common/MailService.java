package com.spduniversity.feelfree.service.common;

import com.spduniversity.feelfree.domain.dto.EmailNotificationDTO;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.service.web.bean.UserContainer;
import com.spduniversity.feelfree.web.bean.NeedHelpContainer;

import javax.annotation.Nonnull;

public interface MailService {
	<T extends UserContainer> void sendActivationEmail(@Nonnull T user, @Nonnull String activationKey);
	
	<T extends UserContainer> void sendSocialMargeEmail(@Nonnull T user, @Nonnull String activationKey);
	
	<T extends UserContainer> void sendActivationEmail(@Nonnull T user, @Nonnull String activationKey, @Nonnull String password);
	
	<T extends UserContainer> void sendEmailResetEmail(@Nonnull T user, @Nonnull String activationKey);
	
	void sendPasswordResetEmail(@Nonnull User user, @Nonnull String password);

	void sendNeedHelpEmail(@Nonnull NeedHelpContainer container);
	
	<T extends UserContainer> void sendSocialPasswordEmail(@Nonnull T user, @Nonnull String password);

	void sendMessageNotification(@Nonnull EmailNotificationDTO dto);

	void sendEmailRequestBook(@Nonnull EmailNotificationDTO dto);

	void sendStatusOfRequestBook(@Nonnull EmailNotificationDTO dto);
}
