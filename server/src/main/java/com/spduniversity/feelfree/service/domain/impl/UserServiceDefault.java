package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.dto.AnnouncementAccountFavoriteDTO;
import com.spduniversity.feelfree.domain.dto.ImageDTO;
import com.spduniversity.feelfree.domain.dto.UserDTO;
import com.spduniversity.feelfree.domain.dto.UserDetailsDTO;
import com.spduniversity.feelfree.domain.mapper.AnnouncementMapperHelper;
import com.spduniversity.feelfree.domain.mapper.ImageMapperHelper;
import com.spduniversity.feelfree.domain.mapper.UserMapperHelper;
import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.model.Image;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.UserRepository;
import com.spduniversity.feelfree.service.common.ImageNameService;
import com.spduniversity.feelfree.service.common.MailService;
import com.spduniversity.feelfree.service.common.extra.EntityMapper;
import com.spduniversity.feelfree.service.common.util.HashGeneratorUtil;
import com.spduniversity.feelfree.service.domain.AnnouncementService;
import com.spduniversity.feelfree.service.domain.ImageService;
import com.spduniversity.feelfree.service.domain.UserService;
import com.spduniversity.feelfree.web.bean.UserInfoContainer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserServiceDefault implements UserService {
	
	@Resource
	private UserRepository userRepository;

	@Resource
	private AnnouncementService announcementService;
	
	@Resource(name = "userMapperHelper")
	private UserMapperHelper mapperHelper;

	@Resource
	private EntityMapper objectMapper;

	@Resource
	private ImageService imageService;

	@Resource
	ImageMapperHelper imageMapper;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private MailService mailService;

	@Resource
	private ImageNameService imageNameService;

	@Resource(name = "announcementMapperHelper")
	private AnnouncementMapperHelper mapper;
	
	@Override
	@Transactional
	@Nullable
	public UserDetailsDTO getUserDetailsData(@Nullable String email) {
		return Optional.ofNullable(email)
					   .map(this::getUser)
					   .map(mapperHelper.toUserDetailsDTO())
					   .orElse(null);
	}

	@Override
	@Transactional
	@Nullable
	public UserDTO getUserData(String email){
		return Optional.ofNullable(email)
				.map(this::getUser)
				.map(mapperHelper.toUserDTO())
				.orElse(null);
	}

	@Override
	@Nullable
	public User getUser(@Nullable String profileEmail) {
		if (profileEmail != null) {
			return userRepository.findByUniqueValue(profileEmail);
		}
		return null;
	}
	
	@Override
	@Nullable
	public User getUserByEmail(@Nullable String profileEmail) {
		if (profileEmail != null) {
			return userRepository.findByProfileEmail(profileEmail);
		}
		return null;
	}
	
	@Override
	@Transactional
	public void changePassword(String password,String email){
		User user = userRepository.findByUniqueValue(email);
		user.setPassword(passwordEncoder.encode(password));
	}
	@Override
	@Transactional
	public Boolean resetPassword(String email){
		User user = userRepository.findByUniqueValue(email);
		if (user==null){
			return false;
		}
		String newPassword = HashGeneratorUtil.generatePassword();
		changePassword(newPassword,email);
		mailService.sendPasswordResetEmail(user,newPassword);
		return true;
	}
	
	@Override
	@Transactional
	public void changeInfo(UserInfoContainer container, String email){
		User user = userRepository.findByUniqueValue(email);
		BeanUtils.copyProperties(container,user);
	}
	
	@Override
	@Transactional
	public boolean save(User user){
		if (user != null) {
			try {
				userRepository.save(user);
				return true;
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		return false;
	}
	
	@Override
	@Transactional
	public void enableUser(User user){
		if (user != null) {
			user.setEnabled(true);
		}
	}

	@Override
	public Set<AnnouncementAccountFavoriteDTO> getFavorites(String email){
		User user = userRepository.findByUniqueValue(email);
		Set <Announcement> fav = user.getFavorites();
		return fav.stream()
				  .filter(e -> Boolean.FALSE.equals(e.getHidden()))
				  .map(e -> mapper.toAccountAnnouncementFavoriteDTO().apply(e))
				  .collect(Collectors.toSet());
	}
	
	@Nonnull
	@Override
	public List<String> getFavoritesData(@Nullable String profileEmail) {
		if (StringUtils.isBlank(profileEmail)) {
			return Collections.emptyList();
		}
		return userRepository.findFavorites(profileEmail);
	}

	@Override
	@Transactional
	public boolean addFavorite(String uniqueValue, String uuid){
		User user = userRepository.findByUniqueValue(uniqueValue);
		Announcement announcement = announcementService.getAnnouncement(uuid);
		if(user == null|| announcement == null){
			return false;
		}
		user.getFavorites().add(announcement);
		announcement.getFavourites().add(user);
		return true;
	}

	@Override
	@Transactional
	public boolean removeFavorite(String uniqueValue, String uuid){
		User user = userRepository.findByUniqueValue(uniqueValue);
		Announcement announcement = announcementService.getAnnouncement(uuid);
		if(user == null|| announcement == null){
			return false;
		}
        user.getFavorites().remove(announcement);
		return true;
	}


	
	@Override
	public boolean isAuthorize(@Nullable String uniqueValue) {
		if (StringUtils.isNotBlank(uniqueValue)) {
			return userRepository.isAuthorize(uniqueValue);
		}
		return false;
	}
	
	@Override
	public boolean isReservedEmail(@Nullable String email) {
		if (StringUtils.isNotBlank(email)) {
			return userRepository.isReservedEmail(email);
		}
		return false;
	}
	
	@Nullable
	@Override
	public User findByUniqueValue(String email) {
		if (StringUtils.isBlank(email)) {
			return null;
		}
		return userRepository.findByUniqueValue(email);
	}
	
	@Override
	public void updateProfileEmail(Long userId, String profileEmail) {
		if (userId == null || StringUtils.isBlank(profileEmail)) {
			return;
		}
		User user = userRepository.findOne(userId);
		user.setProfileEmail(profileEmail);
		userRepository.save(user);
	}

	@Nullable
	@Override
	public ImageDTO saveUserImage(@Nullable MultipartFile file, String profileEmail) {
		User user = findByUniqueValue(profileEmail);
		if (file == null || user == null) {
			return null;
		}
		Image image = imageService.save(convertSingleImage(file));
		user.setImage(image);
		userRepository.save(user);
		ImageDTO dto = objectMapper.convertEntityValue(user.getImage(), ImageDTO.class);
		dto.setName(imageNameService.getFullImageName(dto.getValue()));
		return dto;

	}




	@Nonnull
	private Image convertSingleImage(MultipartFile file) {
		return imageMapper.createImageData(file);
	}
}
