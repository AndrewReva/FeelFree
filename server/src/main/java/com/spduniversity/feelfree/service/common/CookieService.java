package com.spduniversity.feelfree.service.common;

import com.spduniversity.feelfree.service.common.impl.CookieServiceDefault;
import org.springframework.web.context.request.NativeWebRequest;

import javax.annotation.Nullable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public interface CookieService {
	
	@Nullable
	Cookie create(String name, String value, Integer maxAge, CookieServiceDefault.Encoder encoder);
	void add(NativeWebRequest request, String name, String value, Integer maxAge, CookieServiceDefault.Encoder encoder);
	void add(HttpServletResponse response, String name, String value, Integer maxAge, CookieServiceDefault.Encoder encoder);
	void add(HttpServletResponse response, Cookie... cookies);
	void add(NativeWebRequest response, Cookie... cookies);
}
