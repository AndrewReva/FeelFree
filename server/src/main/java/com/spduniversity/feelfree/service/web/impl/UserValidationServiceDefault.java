package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.UserRepository;
import com.spduniversity.feelfree.service.common.ValidatorService;
import com.spduniversity.feelfree.service.web.UserValidationService;
import com.spduniversity.feelfree.web.bean.UserPasswordUpdateContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
@Service
public class UserValidationServiceDefault implements UserValidationService {

    @Resource
    private UserRepository userRepository;

    @Resource
    private ValidatorService validator;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Boolean isReservedEmail(String email) {
        return userRepository.findByUniqueValue(email) != null;
    }

    @Override
    public Map<String, String> getPasswordErrors(UserPasswordUpdateContainer container, String email){
        User user = userRepository.findByUniqueValue(email);
        Map<String, String> result = new HashMap<>();
        if (!validator.isValid(container)) {
            result.putAll(validator.getErrors(container));
        }
        if(!passwordEncoder.matches(container.getOldPassword(),user.getPassword())){
            result.put("oldPassword","incorrect old password");
        }
        return result;
    }
}
