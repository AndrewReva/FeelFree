package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.model.TemporaryUser;
import com.spduniversity.feelfree.domain.repository.TemporaryUserRepository;
import com.spduniversity.feelfree.service.domain.TemporaryUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Slf4j
@Service
@Transactional
public class TemporaryUserServiceDefault implements TemporaryUserService {
    
    @Autowired
    private TemporaryUserRepository temporaryUserRepository;

    @Override
    public String saveTemporaryUser(TemporaryUser temporaryUser){
        if (temporaryUser == null) {
            return null;
        }
        deleteTemporaryUserIfExist(temporaryUser.getProfileEmail());
        return save(temporaryUser) ? temporaryUser.getToken() : null;
    }
    
    @Override
    public boolean save(TemporaryUser temporaryUser) {
        if (temporaryUser != null) {
            try {
                temporaryUserRepository.save(temporaryUser);
                return true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return false;
    }
    
    @Override
    public TemporaryUser findByToken(String token) {
        if (StringUtils.isNotBlank(token)) {
            return temporaryUserRepository.findByToken(token);
        }
        return null;
    }
    
    @Override
    public void deleteTemporaryUserIfExist(String email){
        if (StringUtils.isNotBlank(email)) {
            TemporaryUser tempUser = temporaryUserRepository.findByProfileEmail(email);
            if (tempUser != null){
                delete(tempUser);
            }
        }
    }
    
    @Override
    public boolean delete(TemporaryUser temporaryUser) {
        if (temporaryUser != null) {
            try {
                temporaryUserRepository.delete(temporaryUser);
                return true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return false;
    }
    
    @Override
    public boolean deleteExpireUser(LocalDateTime date) {
        if (date != null) {
            try {
                temporaryUserRepository.delete(date);
                return true;
            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return false;
    }
	
	@Override
	public boolean isNonExpire(String uniqueValue) {
        if (StringUtils.isNotBlank(uniqueValue)) {
            return temporaryUserRepository.isExist(uniqueValue, LocalDateTime.now());
        }
        return false;
	}
}
