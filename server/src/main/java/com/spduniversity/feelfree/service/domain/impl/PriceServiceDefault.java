package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.dto.MinMaxPriceDTO;
import com.spduniversity.feelfree.domain.repository.PriceRepository;
import com.spduniversity.feelfree.service.domain.PriceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PriceServiceDefault implements PriceService {
	
	@Resource
	private PriceRepository priceRepository;
	
	@Override
	public MinMaxPriceDTO getPriceRange() {
		return priceRepository.findMinMaxPrice();
	}
}
