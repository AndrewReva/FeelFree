package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.model.TemporaryEmail;
import com.spduniversity.feelfree.domain.repository.TemporaryEmailRepository;
import com.spduniversity.feelfree.service.domain.TemporaryEmailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.annotation.Resource;

@Slf4j
@Service
public class TemporaryEmailServiceDefault implements TemporaryEmailService {
	
	@Resource
	private TemporaryEmailRepository repository;
	
	@Override
	public boolean save(TemporaryEmail temporaryEmail) {
		if (temporaryEmail != null) {
			try {
				repository.save(temporaryEmail);
				return true;
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		return false;
	}
	
	@Nullable
	@Override
	public TemporaryEmail findByToken(@Nullable String token) {
		if (StringUtils.isNotBlank(token)) {
			return repository.findByToken(token);
		}
		return null;
	}
	
	@Override
	public void delete(TemporaryEmail temporaryEmail) {
		if (temporaryEmail != null) {
			repository.delete(temporaryEmail);
		}
	}
}
