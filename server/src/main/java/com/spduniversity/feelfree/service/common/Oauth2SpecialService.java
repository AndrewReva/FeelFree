package com.spduniversity.feelfree.service.common;

import org.springframework.security.core.Authentication;

public interface Oauth2SpecialService {

    void removeTokenFromSession(Authentication auth);

}
