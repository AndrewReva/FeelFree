package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.domain.dto.*;
import com.spduniversity.feelfree.domain.mapper.AnnouncementMapperHelper;
import com.spduniversity.feelfree.domain.mapper.CommentMapperHelper;
import com.spduniversity.feelfree.domain.mapper.ImageMapperHelper;
import com.spduniversity.feelfree.domain.mapper.RentMapperHelper;
import com.spduniversity.feelfree.domain.model.*;
import com.spduniversity.feelfree.domain.specification.AnnouncementSpecificationHelper;
import com.spduniversity.feelfree.domain.specification.RentSpecificationHelper;
import com.spduniversity.feelfree.service.common.PageableService;
import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import com.spduniversity.feelfree.service.common.util.BeanDefaultUtils;
import com.spduniversity.feelfree.service.domain.*;
import com.spduniversity.feelfree.service.web.AccountService;
import com.spduniversity.feelfree.service.web.MessageService;
import com.spduniversity.feelfree.web.bean.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AccountServiceDefault implements AccountService {
	
	@Resource
	private AnnouncementService announcementService;
	
	@Resource
	private ImageService imageService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private CommentService commentService;
	
	@Resource
	private RentService rentService;
	
	@Resource
	private PageableService pageableService;
	
	@Resource
	private VariablesContainer variablesContainer;
	
	@Resource
	private ImageMapperHelper imageMapper;
	
	@Resource
	private AnnouncementMapperHelper announcementMapper;
	
	@Resource
	private CommentMapperHelper commentMapper;

	@Resource
	private RentMapperHelper rentMapper;
	
	@Resource
	private AnnouncementSpecificationHelper announcementSpecification;
	
	@Resource
	private RentSpecificationHelper rentSpecification;

	@Resource
	private MessageService messageService;
	
	@Override
	@Nullable
	public UUIDDTO saveAnnouncement(@Nullable SaveAnnouncementContainer container, @Nullable String profileEmail) {
		if (container != null && StringUtils.isNotBlank(profileEmail)) {
			if (StringUtils.isBlank(container.getUuid())) {
				return saveNewAnnouncement(container, profileEmail);
			}
			return updateAnnouncement(container, profileEmail);
		}
		return null;
	}
	
	@Override
	public <T extends UUIDContainer> boolean updateAnnouncement(@Nullable T container, @Nullable String profileEmail) {
		if (container != null && StringUtils.isNotBlank(profileEmail)) {
			String uuid = container.getUuid();
			if (StringUtils.isNotBlank(uuid)) {
				Specification<Announcement> specification = announcementSpecification.equalsUuidAndUserActive(uuid.trim(), profileEmail.trim());
				Announcement announcement = announcementService.getAnnouncement(specification);
				if (announcement != null) {
					BeanDefaultUtils.copyNonNullProperties(container, announcement);
					announcementService.save(announcement);
					return true;
				}
			}
		}
		return false;
	}
	
	@Nullable
	@Override
	public <T extends UUIDContainer> CommentDTO saveComment(@Nullable T container, @Nullable String profileEmail) {
		return saveComment(container, profileEmail, commentMapper.toCommentDTO());
	}
	
	@Nullable
	private <T extends UUIDContainer, V> V saveComment(@Nullable T container, @Nullable String profileEmail, Function<Comment, V> mapperFunction) {
		if (container != null && StringUtils.isNotBlank(profileEmail)) {
			String uuid = container.getUuid();
			if (StringUtils.isNotBlank(uuid)) {
				Specification<Announcement> specification = Specifications.where(announcementSpecification.equalUuid(uuid.trim()))
																		  .and(announcementSpecification.nonHidden());
				Announcement announcement = announcementService.getAnnouncement(specification);
				if (announcement != null) {
					Comment comment = convertToComment(container);
					setUserToComment(profileEmail, comment);
					setCommentToAnnouncement(announcement, comment);
					setAnnouncementToComment(announcement, comment);
					return mapperFunction.apply(commentService.save(comment));
				}
			}
		}
		return null;
	}
	
	private void setCommentToAnnouncement(Announcement announcement, Comment comment) {
		announcement.getComments().add(comment);
	}
	private void setAnnouncementToComment(Announcement announcement, Comment comment) {
		comment.setAnnouncement(announcement);
	}
	
	private <T extends UUIDContainer> Comment convertToComment(@Nullable T container) {
		return commentMapper.toComment().apply(container);
	}
	
	private void setUserToComment(String profileEmail, Comment comment) {
		User user = userService.getUser(profileEmail.trim());
		if (user != null) {
			user.getComments().add(comment);
			comment.setUser(user);
		}
	}
	
	@Override
	public Set<ImageUuidPrimeDTO> updateImage(@Nullable AccountPrimeImageContainer container, String profileEmail) {
		if (container == null || StringUtils.isBlank(profileEmail)) {
			return Collections.emptySet();
		}
		if (StringUtils.isNotBlank(container.getAnnouncementUuid()) && StringUtils.isNotBlank(container.getImageUuid())) {
				Announcement announcement = announcementService
						.getAnnouncement(announcementSpecification.equalsUuidAndUserActive(container.getAnnouncementUuid(), profileEmail));
			if (announcement != null) {
				if (Boolean.TRUE.equals(container.getPrime())) {
					clearPrimes(announcement);
					setPrime(announcement, container.getImageUuid());
					announcementService.save(announcement);
					return imageMapper.toImageUuidDTO().apply(announcement.getImages());
				}
				return imageMapper.toImageUuidDTO().apply(announcement.getImages());			}
		}
		return Collections.emptySet();
	}
	
	private void setPrime(Announcement announcement, String imageUuid) {
		if (announcement != null) {
			announcement.getImages().forEach(e -> {
				if (e.getName().equals(imageUuid.trim())) {
					e.setPrime(Boolean.TRUE);
				}
			});
		}
	}
	
	private void clearPrimes(Announcement announcement) {
		if (announcement != null) {
			announcement.getImages().forEach(e -> e.setPrime(Boolean.FALSE));
		}
	}
	
	@Nonnull
	@Override
	public List<ImageDTO> saveImage(@Nullable UploadImageContainer container, String profileEmail) {
		if (container == null || CollectionUtils.isEmpty(container.getFiles()) || StringUtils.isBlank(profileEmail)) {
			return Collections.emptyList();
		}
		if (StringUtils.isNotBlank(container.getUuid())) {
			return saveNewImages(container.getUuid(), profileEmail, container.getFiles());
		}
		return Collections.emptyList();
	}
	
	@SuppressWarnings("unchecked")
	@Nullable
	@Override
	public AnnouncementAccountDTO getAnnouncement(@Nullable String uuid, String profileEmail) {
		if (StringUtils.isBlank(uuid) || StringUtils.isBlank(profileEmail)) {
			return null;
		}
		return announcementService.getAnnouncementData(announcementMapper.toAccountAnnouncementDTO(),
													   announcementSpecification.equalsUuidAndUserActive(uuid, profileEmail));
	}
	
	@Override
	public boolean deleteAnnouncement(@Nullable String uuid, @Nullable String profileEmail) {
		if (StringUtils.isBlank(uuid) || StringUtils.isBlank(profileEmail)) {
			return false;
		}
		return announcementService.delete(announcementSpecification.equalsUuidAndUserActive(uuid, profileEmail));
	}
	
	@Override
	public boolean deleteImage(@Nullable String announcementUuid, @Nullable String imageUuid, @Nullable String profileEmail) {
		if (StringUtils.isBlank(announcementUuid) || StringUtils.isBlank(imageUuid) || StringUtils.isBlank(profileEmail)) {
			return false;
		}
		return imageService.delete(announcementUuid, imageUuid, profileEmail);
	}
	
	@Nonnull
	@SuppressWarnings("unchecked")
	@Override
	public Page<AnnouncementAccountPreviewDTO> getAnnouncements(@Nullable AccountAnnouncementContainer container, @Nullable String profileEmail) {
		if (StringUtils.isBlank(profileEmail)) {
			return new PageImpl<>(Collections.emptyList());
		}
		PageRequest pageable = pageableService.createPageable(container,
															  variablesContainer.getAccountAnnouncementPage(),
															  variablesContainer.getAccountAnnouncementSize(),
															  variablesContainer.getAccountAnnouncementSort());
		List<String> favorites = userService.getFavoritesData(profileEmail);
		return announcementService.getAnnouncementData(announcementMapper.toAccountAnnouncementPreviewDTO(favorites),
													   pageable,
													   announcementSpecification.equalsUserActive(profileEmail));
	}
	
	@Nullable
	@Override
	public List<RentedDTO> getRents(@Nullable String uuid, @Nullable String profileEmail) {
		if (StringUtils.isBlank(uuid) || StringUtils.isBlank(profileEmail)) {
			return null;
		}
		return rentService.getRentsData(rentMapper.toRentedListDTO(),
										rentSpecification.equalsAnnouncementAndUserActive(uuid, profileEmail));
	}
	
	@Override
	public boolean saveRent(AccountRentContainer container, String profileEmail) {
		if (container != null && StringUtils.isNotBlank(profileEmail)) {
			Rent rent = rentMapper.toRent().apply(container);
			Announcement announcement = announcementService.getAnnouncement(container.getUuid().trim());
			if (announcement != null) {
				rent.setAnnouncement(announcement);
				announcement.getRents().add(rent);
				User user = userService.getUser(profileEmail);
				if (user != null) {
					rent.setUser(user);
					user.getRents().add(rent);
					messageService.saveMessageAtRent(user,announcement,rent);
					return rentService.save(rent).getId() != null;
				}
			}
		}
		return false;
	}
	
	@Override
	public boolean updateRent(@Nullable AccountAnnouncementRentedSwitchContainer container, @Nullable String profileEmail) {
		if (container != null && StringUtils.isNotBlank(profileEmail)) {
			Rent rent = rentService.getRent(container.getUuid().trim(), container.getId(), profileEmail.trim());
			if (rent != null) {
				rent.setStatus(container.getStatus());
				rentService.save(rent);
				messageService.updateMessageAtRent(rent,profileEmail);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public List<RentedDatesDTO> getRentedDates(@Nullable AccountAnnouncementRentedDateContainer container, String uuid, String profileEmail) {
		if (container == null || StringUtils.isBlank(uuid) || StringUtils.isBlank(profileEmail)) {
			return Collections.emptyList();
		}
		ArrayList<Specification<Rent>> specifications = new ArrayList<>();
		specifications.add(rentSpecification.equalsAnnouncementAndUserActive(uuid, profileEmail));
		if (container.getFrom() != null) {
			specifications.add(rentSpecification.greaterThanOrEqualToDate(container.getFrom()));
		}
		
		if (container.getTo() != null) {
			specifications.add(rentSpecification.lessThanOrEqualToDate(container.getTo()));
		}
		
		if (container.getStatus() != null) {
			specifications.add(rentSpecification.equalStatus(container.getStatus()));
		}
		return rentService.getRentsData(rentMapper.toRentedDatesDTO(), specifications);
	}
	
	@Nonnull
	private List<ImageDTO> saveNewImages(String uuid, String profileEmail, List<MultipartFile> files) {
		Announcement announcement = announcementService.getAnnouncement(announcementSpecification.equalsUuidAndUserActive(uuid, profileEmail));
		if (announcement != null && announcement.getImages().size() + files.size() <= variablesContainer.getAccountImagesCount()) {
			List<Image> images = convertToImage(files);
			images.forEach(image -> image.setAnnouncement(announcement));
			return imageService.save(images, imageMapper.toImageDTO());
		}
		return Collections.emptyList();
	}
	
	@Nonnull
	private List<Image> convertToImage(List<MultipartFile> files) {
		return files.stream()
					.map(imageMapper.toImage())
					.filter(Objects::nonNull)
					.collect(Collectors.toList());
	}
	
	@Nullable
	private UUIDDTO updateAnnouncement(SaveAnnouncementContainer container, String profileEmail) {
		return Optional.of(container)
					   .map(c -> announcementService.getAnnouncement(
					   		announcementSpecification.equalsUuidAndUserActive(c.getUuid().trim(), profileEmail.trim())))
					   .map(announcement -> announcementMapper.toAnnouncement(announcement).apply(container))
					   .map(announcement -> announcementService.save(announcement))
					   .map(announcementMapper.toUUIDDTO())
					   .orElse(null);
	}
	
	@Nullable
	private UUIDDTO saveNewAnnouncement(SaveAnnouncementContainer container, String profileEmail) {
		return Optional.of(container)
					   .map(announcementMapper.toAnnouncement())
					   .map(announcement -> addUser(profileEmail, announcement))
					   .map(announcement -> announcementService.save(announcement))
					   .map(announcementMapper.toUUIDDTO())
					   .orElse(null);
	}
	
	@Nullable
	private Announcement addUser(String profileEmail, Announcement announcement) {
		User user = userService.getUser(profileEmail.trim());
		if (user != null) {
			user.getAnnouncements().add(announcement);
			announcement.setUser(user);
			return announcement;
		}
		return null;
	}
}
