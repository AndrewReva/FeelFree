package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.service.common.ResizeImageService;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

@Slf4j
@Service
public class ResizeImageServiceDefault implements ResizeImageService {
	
	@Override
	public byte[] scaleImage(InputStream image, int width, int height) {
		if (image != null) {
			try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()){
				Thumbnails.of(image)
						  .useOriginalFormat()
						  .size(width, height)
						  .toOutputStream(byteArrayOutputStream);
				return byteArrayOutputStream.toByteArray();
			} catch (IOException ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		return null;
	}
	
	@Override
	public byte[] getImage(String url, int width, int height, String format) {
		if (StringUtils.isNotBlank(url)) {
			try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()){
				Thumbnails.of(new URL(url))
						  .outputFormat(format)
						  .size(width, height)
						  .toOutputStream(byteArrayOutputStream);
				return byteArrayOutputStream.toByteArray();
			} catch (IOException ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		return null;
	}
}
