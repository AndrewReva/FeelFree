package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.domain.dto.*;
import com.spduniversity.feelfree.domain.mapper.AnnouncementMapperHelper;
import com.spduniversity.feelfree.domain.mapper.CommentMapperHelper;
import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.specification.CommentSpecificationHelper;
import com.spduniversity.feelfree.service.common.PageableService;
import com.spduniversity.feelfree.service.common.extra.EntityMapper;
import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import com.spduniversity.feelfree.service.common.util.UserSecurityUtil;
import com.spduniversity.feelfree.service.domain.AnnouncementService;
import com.spduniversity.feelfree.service.domain.CommentService;
import com.spduniversity.feelfree.service.domain.PriceService;
import com.spduniversity.feelfree.service.domain.UserService;
import com.spduniversity.feelfree.service.domain.bean.AnnouncementContainer;
import com.spduniversity.feelfree.service.domain.bean.GeolocationContainer;
import com.spduniversity.feelfree.service.web.SearchService;
import com.spduniversity.feelfree.web.bean.AccountPageableCommentContainer;
import com.spduniversity.feelfree.web.bean.SearchAnnouncementsContainer;
import com.spduniversity.feelfree.web.bean.SearchGeolocationContainer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

@Slf4j
@Service
public class SearchServiceDefault implements SearchService {
	
	@Resource
	private AnnouncementService announcementService;
	
	@Resource
	private PageableService pageableService;
	
	@Resource
	private PriceService priceService;
	
	@Resource
	private CommentService commentService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private VariablesContainer variablesContainer;
	
	@Resource
	private EntityMapper objectMapper;
	
	@Resource
	private AnnouncementMapperHelper announcementMapper;
	
	@Resource
	private CommentMapperHelper commentMapper;
	
	@Resource
	private CommentSpecificationHelper commentSpecification;
	
	@Nonnull
	@Override
	public Page<GeolocationSearchDTO> getGeolocations(@Nullable SearchGeolocationContainer searchGeolocationContainer) {
		if (searchGeolocationContainer == null) {
			return new PageImpl<>(Collections.emptyList());
		}
		GeolocationContainer geolocationContainer = objectMapper.convertValue(searchGeolocationContainer, GeolocationContainer.class);
		Integer page = ObjectUtils.defaultIfNull(searchGeolocationContainer.getPage(), variablesContainer.getGeolocationPage());
		Integer size = ObjectUtils.defaultIfNull(searchGeolocationContainer.getSize(), variablesContainer.getGeolocationSize());
		return announcementService.getGeolocationData(geolocationContainer, page, size);
	}
	
	@Nullable
	@Override
	public AnnouncementSearchSingleDTO getAnnouncement(@Nullable String uuid) {
		if (StringUtils.isBlank(uuid)) {
			return null;
		}
		return announcementService.getAnnouncementData(uuid, announcementMapper.toSearchAnnouncementDTO());
	}
	
	@Nonnull
	@Override
	public Page<AnnouncementSearchDTO> getAnnouncements(@Nullable SearchAnnouncementsContainer searchAnnouncementsContainer) {
		if (searchAnnouncementsContainer == null) {
			return new PageImpl<>(Collections.emptyList());
		}
		PageRequest pageable = pageableService.createPageable(searchAnnouncementsContainer,
															  variablesContainer.getSearchAnnouncementsPage(),
															  variablesContainer.getSearchAnnouncementsSize(),
															  variablesContainer.getSearchAnnouncementsSort());
		
		AnnouncementContainer announcementContainer = objectMapper.convertValue(searchAnnouncementsContainer, AnnouncementContainer.class);
		Function<Announcement, AnnouncementSearchDTO> mapperFunction = getMapperFunctionByUserAuthenticate();
		return announcementService.getAnnouncementData(announcementContainer, pageable, mapperFunction);
	}
	
	@Override
	public MinMaxPriceDTO getPriceRange() {
		return priceService.getPriceRange();
	}
	
	private Function<Announcement, AnnouncementSearchDTO> getMapperFunctionByUserAuthenticate() {
		if (UserSecurityUtil.isAnonymous()) {
			return announcementMapper.toSearchAnnouncementsDTO();
		}
		List<String> favorites = userService.getFavoritesData(UserSecurityUtil.getName());
		return announcementMapper.toSearchAnnouncementsDTO(favorites);
	}
	
	@SuppressWarnings("unchecked")
	@Nonnull
	@Override
	public Page<CommentDTO> getComments(AccountPageableCommentContainer container, String uuid) {
		PageRequest pageable = pageableService.createPageable(container,
															  variablesContainer.getSearchCommentPage(),
															  variablesContainer.getSearchCommentSize(),
															  variablesContainer.getSearchCommentSort());
		return commentService.getCommentData(commentMapper.toCommentDTO(),
											 pageable,
											 commentSpecification.equalAnnouncementUuid(uuid));
	}
}
