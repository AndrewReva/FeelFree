package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.model.Rent;
import com.spduniversity.feelfree.domain.repository.RentRepository;
import com.spduniversity.feelfree.service.domain.RentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class RentServiceDefault implements RentService {
	
	@Resource
	private RentRepository repository;
	
	@Override
	@Nonnull
	public <T> List<T> getRentsData(@Nullable String uuid, @Nonnull Function<Rent, T> mapperFunction) {
		if (StringUtils.isBlank(uuid)) {
			return Collections.emptyList();
		}
		return repository.findAll(uuid)
						 .stream()
						 .map(mapperFunction)
						 .collect(Collectors.toList());
	}
	
	@Override
	@Nullable
	public Rent getRent(String uuid, Long id, String profileEmail) {
		if (StringUtils.isBlank(uuid) || id == null || StringUtils.isBlank(profileEmail)) {
			return null;
		}
		return repository.findOne(uuid, id, profileEmail);
	}
	
	
	@SafeVarargs
	@Override
	@Nonnull
	public final <T> List<T> getRentsData(@Nonnull Function<Rent, T> mapperFunction, @Nonnull Specification<Rent>... specifications) {
		return repository.resultBuilder(mapperFunction)
						 .whereAnd(specifications)
						 .resultList();
	}
	
	@Override
	public <T> List<T> getRentsData(@Nonnull Function<Rent, T> mapperFunction, @Nonnull List<Specification<Rent>> specifications) {
		return repository.resultBuilder(mapperFunction)
						 .whereAnd(specifications)
						 .resultList();
	}
	
	@Override
	@Nullable
	public <T> T getRentsData(@Nonnull Function<Collection<Rent>, T> mapperFunction, @Nonnull Specification<Rent> specifications) {
		return Optional.ofNullable(repository.findAll(specifications))
					   .map(mapperFunction)
					   .orElse(null);
	}
	
	@Nonnull
	@Override
	public Rent save(@Nonnull Rent rent) {
		return repository.save(rent);
	}
}
