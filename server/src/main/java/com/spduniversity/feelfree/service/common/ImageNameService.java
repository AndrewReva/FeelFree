package com.spduniversity.feelfree.service.common;

import com.spduniversity.feelfree.domain.dto.CommentDTO;
import com.spduniversity.feelfree.domain.dto.ImageDTO;
import com.spduniversity.feelfree.domain.dto.ImageUUIDDTO;

import java.util.Collection;

public interface ImageNameService {
	void setFullNameIntoImage(Collection<ImageDTO> images);
	void setFullNameIntoImage(ImageDTO image);
	
	void setNameUuidIntoImage(ImageUUIDDTO image);
	
	void setNameUuidIntoImage(Collection<ImageUUIDDTO> images);
	
	String getFullImageName(String image);
	void setFullNameIntoMessage(CommentDTO image);
	void setFullNameIntoMessage(Collection<CommentDTO> image);
}
