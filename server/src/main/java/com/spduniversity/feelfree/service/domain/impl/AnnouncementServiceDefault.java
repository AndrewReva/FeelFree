package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.dto.GeolocationSearchDTO;
import com.spduniversity.feelfree.domain.model.Announcement;
import com.spduniversity.feelfree.domain.repository.AnnouncementRepository;
import com.spduniversity.feelfree.domain.repository.core.ResultBuilder;
import com.spduniversity.feelfree.domain.specification.AnnouncementSpecificationHelper;
import com.spduniversity.feelfree.service.common.extra.AnnouncementSearchSpecificationCreator;
import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import com.spduniversity.feelfree.service.domain.AnnouncementService;
import com.spduniversity.feelfree.service.domain.bean.AnnouncementContainer;
import com.spduniversity.feelfree.service.domain.bean.GeolocationContainer;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.function.Function;

@Slf4j
@Service
public class AnnouncementServiceDefault implements AnnouncementService {
	
	@Resource
	private AnnouncementRepository repository;
	
	@Resource
	private AnnouncementSpecificationHelper specification;
	
	@Resource
	private VariablesContainer variablesContainer;
	
	@SuppressWarnings("unchecked")
	@Nullable
	@Override
	public <T> T getAnnouncementData(@Nullable String uuid,
									 @NotNull Function<Announcement, T> mapperFunction) {
		if (StringUtils.isBlank(uuid)) {
			return null;
		}
		return repository.resultBuilder(mapperFunction)
						 .whereAnd(specification.equalUuid(uuid.trim()), specification.nonHidden())
						 .singleResult();
	}
	
	@Nullable
	@SafeVarargs
	@Override
	public final <T> T getAnnouncementData(@NonNull Function<Announcement, T> mapperFunction,
										   @Nullable Specification<Announcement>... specifications) {
		return repository.resultBuilder(mapperFunction)
						 .whereAnd(specifications)
						 .singleResult();
	}
	
	@Nonnull
	@Override
	public <T> Page<T> getAnnouncementData(@Nonnull AnnouncementContainer announcementContainer,
										   @Nonnull Pageable pageable,
										   @Nonnull Function<Announcement, T> mapperFunction) {
		
		ResultBuilder<Announcement, T> dtoResultBuilder = repository.resultBuilder(mapperFunction)
																	.pageable(pageable);
		AnnouncementSearchSpecificationCreator.execute(dtoResultBuilder, announcementContainer);
		return dtoResultBuilder.pageResult();
	}
	
	@SafeVarargs
	@Nonnull
	@Override
	public final <T> Page<T> getAnnouncementData(@Nonnull Function<Announcement, T> mapperFunction,
												 @Nonnull Pageable pageable,
												 @Nullable Specification<Announcement>... specifications) {
		
		return repository.resultBuilder(mapperFunction)
						 .pageable(pageable)
						 .whereAnd(specifications)
						 .pageResult();
	}
	
	@Nonnull
	@Override
	public Page<GeolocationSearchDTO> getGeolocationData(@Nonnull GeolocationContainer geolocationContainer,
														 @Nullable Integer offSet,
														 @Nullable Integer rows) {
		geolocationContainer.setRadius(ObjectUtils.defaultIfNull(geolocationContainer.getRadius(), variablesContainer.getGeolocationRadius()));
		geolocationContainer.setLength(ObjectUtils.defaultIfNull(geolocationContainer.getLength(), variablesContainer.getGeolocationLength()));
		
		return repository.findGeolocationInfo(geolocationContainer,
											  ObjectUtils.defaultIfNull(offSet, variablesContainer.getGeolocationOffset()),
											  ObjectUtils.defaultIfNull(rows, variablesContainer.getGeolocationRows()));
	}
	
	@Override
	@Nullable
	public Announcement save(@Nonnull Announcement announcement) {
		return repository.save(announcement);
	}
	
	@Override
	@Nullable
	public Announcement getAnnouncement(@Nonnull Specification<Announcement> specification) {
		return repository.findOne(specification);
	}
	
	@Override
	@Nullable
	public Announcement getAnnouncement(@Nullable String uuid) {
		if (StringUtils.isBlank(uuid)) {
			return null;
		}
		return repository.findOne(specification.equalUuid(uuid.trim()));
	}
	
	@Override
	public boolean delete(@Nonnull Specification<Announcement> specification) {
 		Announcement announcement = getAnnouncement(specification);
		if (announcement == null) {
			return false;
		}
		try {
			repository.delete(announcement);
			return repository.isDeleted(announcement.getUuid());
		} catch (Exception ex) {
			log.error(ex.getMessage());
			return false;
		}
	}
}
