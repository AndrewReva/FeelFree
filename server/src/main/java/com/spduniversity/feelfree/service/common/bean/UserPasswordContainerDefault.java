package com.spduniversity.feelfree.service.common.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserPasswordContainerDefault<T> implements UserPasswordContainer<T> {
	public T user;
	private String password;
}
