package com.spduniversity.feelfree.service.common.extra;

import com.google.common.base.Splitter;
import com.spduniversity.feelfree.config.ApplicationConfig.YmlSettings;
import com.spduniversity.feelfree.web.common.Length;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Getter
public class VariablesContainer {
	
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\..[_A-Za-z0-9-]+)*@[A-Za-zА-Яа-я0-9-_]+(\\.[A-Za-zА-Яа-я0-9-_]+)*(\\.[A-Za-zА-Яа-я0-9-_]{2,})*([^0-9!@#$%+^&*\\(\\);:\\'\\\"\\\\?.,/\\[\\]{}<>№ ]+)$";
	public static final String CLIENT_ID_FEEL_FREE = "my-client-with-secret";
	public static final String CLIENT_ID_SOCIAL = "social";
	public static final Integer PASSWORD_LENGTH = Integer.valueOf(StringUtils.strip(YmlSettings.get().getProperty("base.encoder.length.password")));
	public static final Integer TOKEN_LENGTH = Integer.valueOf(StringUtils.strip(YmlSettings.get().getProperty("base.encoder.length.token")));
	public static final String TOKEN_COOKIE_NAME = StringUtils.strip(YmlSettings.get().getProperty("base.cors.cookie.token.name"));
	public static final String ERROR_COOKIE_NAME = StringUtils.strip(YmlSettings.get().getProperty("base.cors.cookie.error.name"));
	public static final String SUCCESS_COOKIE_NAME = StringUtils.strip(YmlSettings.get().getProperty("base.cors.cookie.success.name"));
	public static final Integer TOKEN_COOKIE_AGE = Integer.valueOf(StringUtils.strip(YmlSettings.get().getProperty("base.cors.cookie.token.age")));
	public static final Integer ERROR_COOKIE_AGE = Integer.valueOf(StringUtils.strip(YmlSettings.get().getProperty("base.cors.cookie.error.age")));
	public static final Integer SUCCESS_COOKIE_AGE = Integer.valueOf(StringUtils.strip(YmlSettings.get().getProperty("base.cors.cookie.success.age")));
	public static final List<String> TRUST_SOCIAL = Splitter.on(",")
													  .trimResults()
													  .omitEmptyStrings()
													  .splitToList(StringUtils.strip(YmlSettings.get().getProperty("base.social.trust")));
	
	@Value("${domain.service.announcement.geolocation.radius}")
	private Integer geolocationRadius;
	
	@Value("${domain.service.announcement.geolocation.length}")
	private Length geolocationLength;
	
	@Value("${domain.service.announcement.geolocation.offset}")
	private Integer geolocationOffset;
	
	@Value("${domain.service.announcement.geolocation.rows}")
	private Integer geolocationRows;
	
	@Value("${web.service.search.geolocation.page}")
	private Integer geolocationPage;
	
	@Value("${web.service.search.geolocation.size}")
	private Integer geolocationSize;
	
	@Value("${web.service.search.comment.size}")
	private Integer searchCommentSize;
	
	@Value("${web.service.search.comment.page}")
	private Integer searchCommentPage;
	
	@Value("${web.service.search.comment.sort}")
	private List<String> searchCommentSort;
	
	@Value("${web.service.search.announcements.size}")
	private Integer searchAnnouncementsSize;
	
	@Value("${web.service.search.announcements.page}")
	private Integer searchAnnouncementsPage;
	
	@Value("${web.service.search.announcements.sort}")
	private List<String> searchAnnouncementsSort;
	
	@Value("${web.service.account.announcements.size}")
	private Integer accountAnnouncementSize;
	
	@Value("${web.service.account.announcements.page}")
	private Integer accountAnnouncementPage;
	
	@Value("${web.service.account.announcements.sort}")
	private List<String> accountAnnouncementSort;
	
	@Value("${web.service.account.image.count}")
	private Integer accountImagesCount;

	@Value("${spring.mail.username}")
	private String from;

	@Value("${base.url.client}")
	private String clientUrl;

	@Value("${base.url.email.activate}")
	private String emailActivateUrl;

	@Value("${base.url.email.social-activate}")
	private String socialEmailActivateUrl;
	
	@Value("${base.url.email.reset}")
	private String emailResetUrl;

	@Value("${base.url.server}")
	private String serverUrl;

	@Value("${base.url.image.load}")
	private String imageLoadUrl;
	
	@Value("${base.mail.support.username}")
	private String supportEmail;
	
	@Value("${base.cors.cookie.token.name}")
	private String tokenCookieName;
	
	@Value("${base.cors.cookie.token.age}")
	private Integer tokenCookieAge;
	
	@Value("${base.cors.cookie.error.name}")
	private String errorCookieName;
	
	@Value("${base.cors.cookie.error.age}")
	private Integer errorCookieAge;
	
	@Value("${base.cors.cookie.success.name}")
	private String successCookieName;
	
	@Value("${base.cors.cookie.success.age}")
	private Integer successCookieAge;
}
