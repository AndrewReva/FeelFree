package com.spduniversity.feelfree.service.web.impl;

import com.spduniversity.feelfree.service.web.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LoginServiceDefault implements LoginService {}
