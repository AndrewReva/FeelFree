package com.spduniversity.feelfree.service.web;

import com.spduniversity.feelfree.web.bean.UserRegisterContainer;

public interface WebTemporaryUserService {

    String saveTemporaryUser(UserRegisterContainer container);

    String reviewToken(String token);

}
