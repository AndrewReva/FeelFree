package com.spduniversity.feelfree.service.common.impl;

import com.spduniversity.feelfree.domain.model.*;
import com.spduniversity.feelfree.domain.repository.RoleRepository;
import com.spduniversity.feelfree.service.common.BlobService;
import com.spduniversity.feelfree.service.common.ResizeImageService;
import com.spduniversity.feelfree.service.common.SocialUserCreator;
import com.spduniversity.feelfree.service.common.SocialUserCreatorService;
import com.spduniversity.feelfree.service.common.bean.UserPasswordContainer;
import com.spduniversity.feelfree.service.common.bean.UserPasswordContainerDefault;
import com.spduniversity.feelfree.service.common.util.HashGeneratorUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.social.connect.Connection;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.plus.Person;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SocialUserCreatorServiceDefault implements SocialUserCreatorService {
	
	private final Map<String, SocialUserCreator> providerCreator = new HashMap<>();
	
	@Resource(name = "facebookUserCreator")
	private SocialUserCreator facebookUserCreator;
	
	@Resource(name = "googleUserCreator")
	private SocialUserCreator googleUserCreator;
	
	@PostConstruct
	private void init() {
		providerCreator.put("facebook", facebookUserCreator);
		providerCreator.put("google", googleUserCreator);
	}
	
	@Override
	public boolean isExistEmail(Connection<?> connection) {
		return connection != null && StringUtils.isNotBlank(connection.fetchUserProfile().getEmail());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public UserPasswordContainer<User> createUser(Connection<?> connection) {
		if (connection == null) {
			return null;
		}
		UserPasswordContainer<User> result = null;
		for (Map.Entry<String, SocialUserCreator> userCreator : providerCreator.entrySet()) {
			if (userCreator.getKey().equals(connection.getKey().getProviderId())) {
				result = userCreator.getValue().createUser(connection);
				break;
			}
		}
		return result;
	}
	
	@Override
	public UserPasswordContainer<TemporaryUser> createTemporaryUser(Connection<?> connection) {
		if (connection == null) {
			return null;
		}
		UserPasswordContainer<TemporaryUser> result = null;
		for (Map.Entry<String, SocialUserCreator> userCreator : providerCreator.entrySet()) {
			if (userCreator.getKey().equals(connection.getKey().getProviderId())) {
				result = userCreator.getValue().createTemporaryUser(connection);
				break;
			}
		}
		return result;
	}
	
	@Override
	public void editUser(Connection<?> connection, User user) {
		if (connection == null) {
			return;
		}
		for (Map.Entry<String, SocialUserCreator> userCreator : providerCreator.entrySet()) {
			if (userCreator.getKey().equals(connection.getKey().getProviderId())) {
				userCreator.getValue().editUser(connection, user);
				break;
			}
		}
	}
	
	@Component("facebookUserCreator")
	public static class FacebookUserCreator implements SocialUserCreator {
		
		private static final int TOKEN_LENGTH = 20;
		private static final String ROLE_USER = "ROLE_USER";
		private static final String DEFAULT_IMAGE_EXTENSION = "jpeg";
		private static final String DEFAULT_IMAGE_TYPE = MediaType.IMAGE_JPEG_VALUE;
		private static final int DEFAULT_IMAGE_WIDTH = 200;
		private static final int DEFAULT_IMAGE_HEIGHT = 200;
		private static final String IMAGE_URL_FORMAT = "%s?width=%s&height=%s";
		
		@Resource
		private RoleRepository roleRepository;
		
		@Resource
		private ResizeImageService imageService;
		
		@Resource
		private BlobService blobService;
		
		@SuppressWarnings("unchecked")
		@Override
		public UserPasswordContainer<User> createUser(Connection<?> connection) {
			Connection<Facebook> facebookConnection = (Connection<Facebook>) connection;
			org.springframework.social.facebook.api.User userProfile = facebookConnection.getApi().userOperations().getUserProfile();
			User user = new User();
			user.setFirstName(userProfile.getFirstName());
			user.setLastName(userProfile.getLastName());
			user.setEnabled(Boolean.TRUE);
			user.setProfileEmail(userProfile.getEmail());
			user.setFacebookId(userProfile.getId());
			String password = HashGeneratorUtil.generatePassword();
			user.setPassword(HashGeneratorUtil.generateBCrypt(password));
			Role role = roleRepository.findByValue(ROLE_USER);
			user.getRoles().add(role);
			role.getUsers().add(user);
			user.setImage(createImage(connection));
			return new UserPasswordContainerDefault<>(user, password);
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public UserPasswordContainer<TemporaryUser> createTemporaryUser(Connection<?> connection) {
			Connection<Facebook> facebookConnection = (Connection<Facebook>) connection;
			org.springframework.social.facebook.api.User userProfile = facebookConnection.getApi().userOperations().getUserProfile();
			TemporaryUser user = new TemporaryUser();
			user.setFirstName(userProfile.getFirstName());
			user.setLastName(userProfile.getLastName());
			user.setProfileEmail(userProfile.getEmail());
			String password = HashGeneratorUtil.generatePassword();
			user.setPassword(HashGeneratorUtil.generateBCrypt(password));
			user.setFacebookId(userProfile.getId());
			user.setToken(HashGeneratorUtil.generate(TOKEN_LENGTH));
			user.setImage(createImage(connection));
			return new UserPasswordContainerDefault<>(user, password);
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public void editUser(Connection<?> connection, User user) {}
		
		private Image createImage(Connection<?> connection) {
			String imageUrl = String.format(IMAGE_URL_FORMAT, connection.getImageUrl(), DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT);
			if (imageUrl != null) {
				byte[] image = imageService.getImage(imageUrl, DEFAULT_IMAGE_WIDTH, DEFAULT_IMAGE_HEIGHT, DEFAULT_IMAGE_EXTENSION);
				Image imageEntity = new Image();
				imageEntity.setValue(blobService.createBlob(image));
				imageEntity.setType(DEFAULT_IMAGE_TYPE);
				return imageEntity;
			}
			return null;
		}
	}
	
	@Component("googleUserCreator")
	public static class GoogleUserCreator implements SocialUserCreator {
		
		private static final String ROLE_USER = "ROLE_USER";
		private static final String DEFAULT_IMAGE_EXTENSION = "jpeg";
		private static final String DEFAULT_IMAGE_TYPE = MediaType.IMAGE_JPEG_VALUE;
		private static final int DEFAULT_IMAGE_SIZE = 200;
		
		@Resource
		private RoleRepository roleRepository;
		
		@Resource
		private ResizeImageService imageService;
		
		@Resource
		private BlobService blobService;
		
		@SuppressWarnings("unchecked")
		@Override
		public UserPasswordContainer<User> createUser(Connection<?> connection) {
			Connection<Google> googleConnection = (Connection<Google>) connection;
			Person userProfile = googleConnection.getApi().plusOperations().getGoogleProfile();
			Set<Email> emails = createEmails(userProfile.getEmailAddresses());
			User user = new User();
			user.setFirstName(userProfile.getGivenName());
			user.setLastName(userProfile.getFamilyName());
			user.setEnabled(Boolean.TRUE);
			user.setProfileEmail(userProfile.getAccountEmail());
			user.getEmails().addAll(emails);
			user.setGoogleId(userProfile.getId());
			user.setImage(createImage(connection));
			Role role = roleRepository.findByValue(ROLE_USER);
			user.getRoles().add(role);
			role.getUsers().add(user);
			String password = HashGeneratorUtil.generatePassword();
			user.setPassword(HashGeneratorUtil.generateBCrypt(password));
			return new UserPasswordContainerDefault(user, password);
		}
		
		private Set<Email> createEmails(Set<String> emails) {
			return getEmailExcludeProfileEmail(emails)
					.stream()
					.map(email -> {
						Email emailEntity = new Email();
						emailEntity.setValue(email);
						return emailEntity;
					})
					.collect(Collectors.toSet());
		}
		
		private Set<String> getEmailExcludeProfileEmail(Set<String> emails) {
			return emails.stream()
						 .skip(1)
						 .collect(Collectors.toSet());
		}
		
		private Image createImage(Connection<?> connection) {
			String imageUrl = StringUtils.substringBeforeLast(connection.getImageUrl(), "sz");
			if (imageUrl != null) {
				byte[] image = imageService.getImage(imageUrl + "sz=" + DEFAULT_IMAGE_SIZE,
													 DEFAULT_IMAGE_SIZE,
													 DEFAULT_IMAGE_SIZE,
													 DEFAULT_IMAGE_EXTENSION);
				Image imageEntity = new Image();
				imageEntity.setValue(blobService.createBlob(image));
				imageEntity.setType(DEFAULT_IMAGE_TYPE);
				return imageEntity;
			}
			return null;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public UserPasswordContainer<TemporaryUser> createTemporaryUser(Connection<?> connection) {
			return null;
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public void editUser(@Nonnull Connection<?> connection, @Nonnull User user) {
			Connection<Google> googleConnection = (Connection<Google>) connection;
			Person userProfile = googleConnection.getApi().plusOperations().getGoogleProfile();
			if (StringUtils.isBlank(user.getFirstName())) {
				user.setFirstName(userProfile.getGivenName());
			}
			if (StringUtils.isBlank(user.getLastName())) {
				user.setFirstName(userProfile.getFamilyName());
			}
			Set<String> difference = getDifferenceEmails(user.getEmails(), userProfile.getEmailAddresses());
			if (!CollectionUtils.isEmpty(difference)) {
				user.getEmails().addAll(createEmails(difference));
			}
			if (StringUtils.isBlank(user.getGoogleId())) {
				user.setGoogleId(userProfile.getId());
			}
			if (user.getImage() == null) {
				user.setImage(createImage(connection));
			}
		}
		
		private Set<String> getDifferenceEmails(Set<Email> userEmails, Set<String> socialEmails) {
			Set<String> userEmailsAsStrings = userEmails.stream().map(Email::getValue).collect(Collectors.toSet());
			if (!CollectionUtils.isEmpty(userEmailsAsStrings)) {
				return getEmailExcludeProfileEmail(socialEmails).stream()
																.filter(e -> !userEmailsAsStrings.contains(e))
																.collect(Collectors.toSet());
			}
			return Collections.emptySet();
		}
	}
}
