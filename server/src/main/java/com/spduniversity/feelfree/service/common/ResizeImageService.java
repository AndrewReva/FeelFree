package com.spduniversity.feelfree.service.common;

import javax.annotation.Nullable;
import java.io.InputStream;

public interface ResizeImageService {
	
	@Nullable
	byte[] scaleImage(InputStream image, int weight, int height);
	
	byte[] getImage(String url, int width, int height, String format);
}
