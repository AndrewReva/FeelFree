package com.spduniversity.feelfree.service.common.extra;

import com.spduniversity.feelfree.service.common.ValidatorService;
import com.spduniversity.feelfree.service.common.impl.ValidatorServiceDefault;

import javax.validation.Configuration;
import javax.validation.Validation;
import javax.validation.Validator;

public class ValidatorSingleton {
	
	private ValidatorSingleton() {}
	
	public static ValidatorService get() {
		return Inner.getInstance();
	}
	
	private static class Inner {
		private static ValidatorService instance = null;
		
		private static ValidatorService getInstance() {
			if (instance == null) {
				Configuration<?> configure = Validation.byDefaultProvider().configure();
				Validator validator = configure.messageInterpolator(new ServerLocaleMessageInterpolator(configure.getDefaultMessageInterpolator()))
											   .buildValidatorFactory().getValidator();
				instance = new ValidatorServiceDefault(validator);
			}
			return instance;
		}
	}
}
