package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.model.TemporaryUser;

import java.time.LocalDateTime;

public interface TemporaryUserService {

    String saveTemporaryUser(TemporaryUser container);
    
    boolean save(TemporaryUser temporaryUser);
    
    TemporaryUser findByToken(String token);
    
    void deleteTemporaryUserIfExist(String email);
    
    boolean delete(TemporaryUser temporaryUser);
    
    boolean deleteExpireUser(LocalDateTime date);
    
    boolean isNonExpire(String uniqueValue);
}
