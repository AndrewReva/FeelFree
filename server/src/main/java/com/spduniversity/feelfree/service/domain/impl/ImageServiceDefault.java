package com.spduniversity.feelfree.service.domain.impl;

import com.spduniversity.feelfree.domain.dto.LoadImageDTO;
import com.spduniversity.feelfree.domain.model.Image;
import com.spduniversity.feelfree.domain.repository.ImageRepository;
import com.spduniversity.feelfree.service.domain.ImageService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ImageServiceDefault implements ImageService {
	
	@Resource
	private ImageRepository repository;

	@Override
	@Nullable
	public LoadImageDTO getImageData(@Nullable String name) {
		if (StringUtils.isBlank(name)) {
			return null;
		}
		return repository.findTopByName(name.trim(), LoadImageDTO.class);
	}
	
	@Override
	public boolean isSave(Image image) {
		if (image != null) {
			try {
				repository.save(image);
				return true;
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
			}
		}
		return false;
	}
	
	@Override
	@Nonnull
	public <T> List<T> save(@Nullable List<Image> images, @Nonnull Function<Image, T> mapperFunction) {
		if (CollectionUtils.isEmpty(images)) {
			return Collections.emptyList();
		}
		return images.stream()
					 .map(image -> repository.save(image))
					 .map(mapperFunction)
					 .collect(Collectors.toList());
	}
	@Override
	@Nonnull
	public  Image save(@NotNull Image image) {
		return repository.save(image);
	}
	
	@Override
	@Nonnull
	public List<Image> save(@Nullable List<Image> images) {
		if (CollectionUtils.isEmpty(images)) {
			return Collections.emptyList();
		}
		return repository.save(images);
	}
	
	@Override
	public boolean delete(@Nonnull String announcementUuid, @Nonnull String imageUuid, @Nonnull String profileEmail) {
		try {
			repository.delete(announcementUuid.trim(), imageUuid.trim(), profileEmail.trim());
			return repository.isDeleted(imageUuid.trim());
		} catch (Exception ex) {
			log.error(ex.getMessage());
			return false;
		}
	}
	
	@Nullable
	@Override
	public Image getImage(String announcementUuid, String uuid, String profileEmail) {
		return repository.findOne(announcementUuid, uuid, profileEmail);
	}
}
