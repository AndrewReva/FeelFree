package com.spduniversity.feelfree.service.domain;

import com.spduniversity.feelfree.domain.model.Rent;
import org.springframework.data.jpa.domain.Specification;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public interface RentService {
	
	@Nonnull
	<T> List<T> getRentsData(@Nullable String uuid, @Nonnull Function<Rent, T> mapperFunction);
	
	@Nullable
	Rent getRent(String uuid, Long id, String profileEmail);
	
	@SuppressWarnings("unchecked")
	@Nonnull
	<T> List<T> getRentsData(@Nonnull Function<Rent, T> mapperFunction, @Nonnull Specification<Rent>... specifications);
	
	
	<T> List<T> getRentsData(@Nonnull Function<Rent, T> mapperFunction, @Nonnull List<Specification<Rent>> specifications);
	
	@Nullable
	<T> T getRentsData(@Nonnull Function<Collection<Rent>, T> mapperFunction, @Nonnull Specification<Rent> specifications);
	
	@Nonnull
	Rent save(@Nonnull Rent rent);
}
