package com.spduniversity.feelfree.service.common.util;

import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.authentication.encoding.BasePasswordEncoder;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.UUID;
import java.util.regex.Pattern;

@Slf4j
public class HashGeneratorUtil {
	
	private static final int RANDOM_COUNT = 32;
	private static final Pattern SPACE = Pattern.compile("-");
	private static final Pattern LETTER_NUMBER = Pattern.compile("^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])[a-zA-Z0-9]+$");
	private static final String EMPTY = "";
	private static final PasswordEncoder bCryptEncoder = new BCryptPasswordEncoder();
	private static final BasePasswordEncoder md5Encoder = new Md5PasswordEncoder();
	
	
	private static String generate(String value) {
		return md5Encoder.encodePassword(bCryptEncoder.encode(value), null);
	}
	
	public static String generateMd5(String value, String salt) {
		return md5Encoder.encodePassword(value, salt);
	}
	
	public static String generateBCrypt(String value) {
		return bCryptEncoder.encode(value);
	}
	
	public static String generate(int length) {
		return RandomStringUtils.randomAlphanumeric(length);
	}
	
	public static String generateString() {
		return generate(RandomStringUtils.randomAlphanumeric(RANDOM_COUNT));
	}
	
	public static byte[] generateBytes() {
		return generate(RandomStringUtils.randomAlphanumeric(RANDOM_COUNT)).getBytes();
	}
	
	public static String generateUUID() {
		return SPACE.matcher(UUID.nameUUIDFromBytes(generateBytes()).toString()).replaceAll(EMPTY);
	}
	
	public static String generatePassword() {
		String password = RandomStringUtils.randomAlphanumeric(VariablesContainer.PASSWORD_LENGTH);
		if (LETTER_NUMBER.matcher(password).matches()) {
			return password;
		}
		return generatePassword();
	}
	
	public static String generateToken() {
		return RandomStringUtils.randomAlphanumeric(VariablesContainer.TOKEN_LENGTH);
	}
}
