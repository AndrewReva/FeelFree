package com.spduniversity.feelfree.service.domain.impl;

import com.google.common.collect.Sets;
import com.spduniversity.feelfree.domain.dto.ConversationDTO;
import com.spduniversity.feelfree.domain.mapper.ConversationMapperHelper;
import com.spduniversity.feelfree.domain.model.Conversation;
import com.spduniversity.feelfree.domain.model.Message;
import com.spduniversity.feelfree.domain.model.MessageTitle;
import com.spduniversity.feelfree.domain.model.User;
import com.spduniversity.feelfree.domain.repository.ConversationRepository;
import com.spduniversity.feelfree.domain.repository.MessageRepository;
import com.spduniversity.feelfree.service.domain.ConversationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Set;

@Service
public class ConversationServiceDefault implements ConversationService{

    @Resource
    ConversationRepository conversationRepository;

    @Resource
    MessageRepository messageRepository;

    @Resource
    ConversationMapperHelper conversationMapperHelper;

    @Override
    public Conversation getConversation(User user1, User user2){
        Conversation currentConversation = conversationRepository.findConversationByUsers(user1,user2);
        if (currentConversation==null){
            currentConversation = new Conversation();
            Set<Conversation> user1Conv = user1.getConversations();
            Set<Conversation> user2Conv = user2.getConversations();
            currentConversation.setConversationUsers(Sets.newHashSet(user1,user2));
            currentConversation.setMessageTime(LocalDateTime.now());
            user1Conv.add(currentConversation);
            user1.setConversations(user1Conv);
            user2Conv.add(currentConversation);
            user2.setConversations(user2Conv);
            conversationRepository.save(currentConversation);

        }
        return currentConversation;
    }
    @Transactional
    @Override
    public void saveMessage(Message message){
        messageRepository.save(message);
    }

    @Transactional
    @Override
    public void saveConversation(Conversation conversation){
        conversation.setMessageTime(LocalDateTime.now());
        conversationRepository.save(conversation);
    }

    @Override
    public MessageTitle getTitleFromLastMessage(Conversation conversation){
        MessageTitle messageTitle = messageRepository.findFirstByConversationOrderByCreateDateDesc(conversation).getTitle();
        return messageTitle;

    }
    @Override
    public Conversation getConversationById(Long id){
      return conversationRepository.findById(id);
    }

    @Override
    public Set<ConversationDTO> getUserConversations(User user){
        Set<Conversation> conversations = user.getConversations();
        Set<ConversationDTO>conversationDTOS =conversationMapperHelper.toSetOfConversationDTO().apply(conversations);
       return conversationDTOS;

    }
}
