package com.spduniversity.feelfree.service.common.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class PageableDefaultContainer extends PageableContainer {
	private Integer defaultPage;
	private Integer defaultSize;
	private List<String> defaultSort;
	
	public PageableDefaultContainer(Integer page, Integer size, List<String> sort, Integer defaultPage, Integer defaultSize,
									List<String> defaultSort) {
		super(page, size, sort);
		this.defaultPage = defaultPage;
		this.defaultSize = defaultSize;
		this.defaultSort = defaultSort;
	}
}
