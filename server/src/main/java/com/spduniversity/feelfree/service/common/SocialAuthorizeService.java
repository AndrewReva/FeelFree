package com.spduniversity.feelfree.service.common;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.social.connect.Connection;

import javax.annotation.Nullable;

public interface SocialAuthorizeService {
	
	boolean isAuthorize(String uniqueValue);
	boolean isTemporary(String uniqueValue);
	@Nullable
	UserDetails getDetailsData(String uniqueValue);
	@Nullable
	OAuth2AccessToken authorizeUser(String uniqueValue, String clientId);
	@Nullable
	OAuth2AccessToken reauthorizeUser(String uniqueValue, String clientId);
	boolean removeAccessToken();
	
	boolean deleteConnection(Connection<?> connection);
}
