package com.spduniversity.feelfree.service.common.util;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.Field;
import java.util.Arrays;

public class BeanDefaultUtils extends BeanUtils {
	
	public static void copyNonNullProperties(Object source, Object target) {
		String[] result = Arrays.stream(FieldUtils.getAllFields(source.getClass()))
								.filter(field -> {
									try {
										return FieldUtils.readField(source, field.getName(), true) == null;
									} catch (Exception ex) {
										return false;
									}
								})
								.map(Field::getName)
								.toArray(String[]::new);
		copyProperties(source, target, result);
	}
}
