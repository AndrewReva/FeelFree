package com.spduniversity.feelfree.service.common;

import java.io.InputStream;
import java.sql.Blob;

public interface BlobService {
	Blob createBlob(final byte[] bytes);
	Blob createBlob(final InputStream stream, final long length);
}
