package com.spduniversity.feelfree.config.social;

import com.spduniversity.feelfree.service.common.CookieService;
import com.spduniversity.feelfree.service.common.SocialAuthorizeService;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionFactory;
import org.springframework.social.connect.web.ProviderSignInInterceptor;
import org.springframework.social.google.api.Google;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

class GoogleProviderSignInInterceptor extends SocialProviderSignInInterceptor implements ProviderSignInInterceptor<Google> {
	
	GoogleProviderSignInInterceptor(SocialAuthorizeService service, CookieService cookieService) {
		super(service, cookieService);
	}
	
	@Override
	public void preSignIn(ConnectionFactory<Google> connectionFactory, MultiValueMap<String, String> parameters, WebRequest request) {
	}
	
	@Override
	public void postSignIn(Connection<Google> connection, WebRequest request) {
		String id = connection.getApi().plusOperations().getGoogleProfile().getId();
		authorize((ServletWebRequest) request, connection, id);
	}
}
