package com.spduniversity.feelfree.config;

public interface ApplicationProfile {
	String DEV = "dev";
	String TEST = "test";
	String PROD = "prod";
}
