package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.annotation.UUIDLength;
import com.spduniversity.feelfree.web.annotation.ValidMultipartFile;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class UploadImageContainer {
	
	@NotEmpty
	@UUIDLength(equal = 32)
	private String uuid;
	
	@ValidMultipartFile(formats = {"jpeg", "jpg", "png", "gif", "bmp", "tiff"}, maxSize = 4194304L, minSize = 1L, commonMaxSize = 42991616L)
	private List<MultipartFile> files;
}
