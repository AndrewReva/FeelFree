package com.spduniversity.feelfree.web.bean;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class AccountMessageAnnouncementContainer {
    @NotEmpty
    private String title;

    @NotEmpty
    private String text;

    @NotEmpty
    private String announcementUuid;
}
