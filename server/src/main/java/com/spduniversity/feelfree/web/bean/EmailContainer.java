package com.spduniversity.feelfree.web.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Pattern;

import static com.spduniversity.feelfree.service.common.extra.VariablesContainer.EMAIL_PATTERN;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class EmailContainer {
    
    @Length(max = 320)
    @Pattern(regexp = EMAIL_PATTERN, message = "{annotation.parameters.email.invalid}")
    private String email;
}
