package com.spduniversity.feelfree.web.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.NotBlank;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccountCommentContainer extends UUIDContainer {
	
	@NotBlank
	private String value;
}
