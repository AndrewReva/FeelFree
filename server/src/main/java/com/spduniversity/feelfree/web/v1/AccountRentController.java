package com.spduniversity.feelfree.web.v1;


import com.spduniversity.feelfree.service.common.util.UserSecurityUtil;
import com.spduniversity.feelfree.service.web.AccountService;
import com.spduniversity.feelfree.web.bean.AccountRentContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/account")
public class AccountRentController {
	
	@Resource
	private AccountService accountService;
	
	@PostMapping(value = "/rent", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> rentSave(@RequestBody(required = false) AccountRentContainer container) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			target.badRequest().build();
		}
		if (accountService.saveRent(container, UserSecurityUtil.getName())) {
			return ResponseCreator.ok("rent.save.ok").build();
		}
		return ResponseCreator.error("rent.save.error").build();
	}
}
