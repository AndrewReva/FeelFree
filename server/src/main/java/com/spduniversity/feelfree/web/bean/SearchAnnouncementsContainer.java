package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.domain.model.Price;
import com.spduniversity.feelfree.service.common.bean.PageableContainer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SearchAnnouncementsContainer extends PageableContainer {
	private String country;
	private String city;
	private String region;
	private String placeId;
	private Integer minPrice;
	private Integer maxPrice;
	private Integer rooms;
	private Integer livingPlaces;
	private Price.Period period;
	private Boolean nonNullImage;
	private Boolean wiFi;
	private Boolean washingMachine;
	private Boolean refrigerator;
	private Boolean hairDryer;
	private Boolean iron;
	private Boolean smoking;
	private Boolean animals;
	private Boolean kitchenStuff;
	private Boolean conditioner;
	private Boolean balcony;
	private Boolean tv;
	private Boolean essentials;
	private Boolean shampoo;
}
