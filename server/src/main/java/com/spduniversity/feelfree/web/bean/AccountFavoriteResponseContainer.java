package com.spduniversity.feelfree.web.bean;

import lombok.Data;

@Data
public class AccountFavoriteResponseContainer {
    private String uuid;

    private String favorite;


}
