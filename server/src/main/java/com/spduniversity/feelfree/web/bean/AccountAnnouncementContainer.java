package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.service.common.bean.PageableContainer;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccountAnnouncementContainer extends PageableContainer {}
