package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.annotation.UUIDLength;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class UUIDContainer {
	
	@UUIDLength(equal = 32)
	protected String uuid;
}
