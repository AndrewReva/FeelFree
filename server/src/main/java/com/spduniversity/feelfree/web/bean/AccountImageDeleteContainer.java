package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.annotation.UUIDLength;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountImageDeleteContainer {
	
	@NotEmpty
	@UUIDLength(equal = 32)
	private String announcementUuid;
	
	@NotEmpty
	@UUIDLength(equal = 32)
	private String imageUuid;
}
