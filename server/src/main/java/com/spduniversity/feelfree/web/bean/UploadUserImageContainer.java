package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.annotation.ValidMultipartFile;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class UploadUserImageContainer {
	
	@ValidMultipartFile(formats = {"jpeg", "jpg", "png", "gif", "bmp", "tiff"}, maxSize = 4194304L, minSize = 1L, commonMaxSize = 4194304L)
	private List<MultipartFile> file;
}
