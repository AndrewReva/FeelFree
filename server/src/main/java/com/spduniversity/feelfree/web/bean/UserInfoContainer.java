package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.domain.model.Email;
import com.spduniversity.feelfree.domain.model.Phone;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoContainer {
    private String firstName;
    private String lastName;
    private Set<Email> emails;
    private Set<Phone> phones;
}