package com.spduniversity.feelfree.web.bean;

import com.spduniversity.feelfree.web.annotation.UUIDLength;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

@Data
public class AccountPrimeImageContainer {
	
	@NotBlank
	@UUIDLength(equal = 32)
	private String imageUuid;
	
	@NotBlank
	@UUIDLength(equal = 32)
	private String announcementUuid;
	private Boolean prime;
}
