package com.spduniversity.feelfree.web.bean;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class AccountMessageContainer {

    @NotEmpty
    private String text;

    private Long conversationId;

}
