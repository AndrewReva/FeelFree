package com.spduniversity.feelfree.web.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
public class AccountRentContainer extends UUIDContainer {
	private LocalDateTime checkIn;
	private LocalDateTime checkOut;
}
