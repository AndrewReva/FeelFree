package com.spduniversity.feelfree.web.v1;


import com.spduniversity.feelfree.service.common.CookieService;
import com.spduniversity.feelfree.service.common.ValidatorService;
import com.spduniversity.feelfree.service.common.extra.VariablesContainer;
import com.spduniversity.feelfree.service.common.impl.CookieServiceDefault.Encoder;
import com.spduniversity.feelfree.service.web.RegistrationService;
import com.spduniversity.feelfree.web.bean.EmailContainer;
import com.spduniversity.feelfree.web.bean.UserRegisterContainer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.spduniversity.feelfree.config.social.SocialProviderSignInInterceptor.*;
import static com.spduniversity.feelfree.service.common.extra.VariablesContainer.*;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/registration")
public class RegistrationController {

    @Resource
    private RegistrationService registrationService;

    @Resource
    private VariablesContainer variables;
    
    @Resource
    private ProviderSignInUtils signInUtils;
    
    @Resource
    private ValidatorService validatorService;
    
    @Resource
    private CookieService cookieService;
    
    @Resource
	private MessageSource messageSource;

    @PreAuthorize("permitAll()")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> registration(@RequestBody UserRegisterContainer userRegisterContainer){
        Map<String, String> errors = registrationService.getErrors(userRegisterContainer);
        if (errors.isEmpty()){
            registrationService.registerTemporaryUser(userRegisterContainer);
            return ResponseEntity.ok(new HashMap<>());
        }
        return ResponseEntity.badRequest().body(errors);
    }
    
    @GetMapping(value = "/activate", produces = MediaType.APPLICATION_JSON_VALUE)
    public void activateUser(@RequestParam (value = "token") String token,
                             @Value("${web.controller.registration.redirect.success}") String redirectSuccess,
                             @Value("${web.controller.registration.redirect.error}") String redirectError,
                             HttpServletResponse response) {
        String userEmail = registrationService.reviewToken(token);
        if (userEmail != null) {
            if (registrationService.makeAuthorizeRedirectResponse(response, userEmail, variables.getClientUrl() + redirectSuccess)) {
                return;
            }
        }
		String error = messageSource.getMessage("registration.activate.error", new Object[]{}, LocaleContextHolder.getLocale());
		cookieService.add(response, ERROR_COOKIE_NAME, error, 5, Encoder.BASE64);
        registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectError);
    }
    
    @GetMapping(value = "/activate/social", produces = MediaType.APPLICATION_JSON_VALUE)
    public void activateSocialUser(@RequestParam (value = "token") String token,
                             @Value("${web.controller.registration.redirect.success}") String redirectSuccess,
                             @Value("${web.controller.registration.redirect.error}") String redirectError,
                             HttpServletResponse response) {
        String userEmail = registrationService.reviewSocialToken(token);
        if (userEmail != null) {
            if (registrationService.makeAuthorizeRedirectResponse(response, userEmail, variables.getClientUrl() + redirectSuccess)) {
                return;
            }
        }
		String error = messageSource.getMessage("registration.activate.error", new Object[]{}, LocaleContextHolder.getLocale());
		cookieService.add(response, ERROR_COOKIE_NAME, error, ERROR_COOKIE_AGE, Encoder.BASE64);
        registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectError);
    }
    
    @GetMapping(value = "/email/reset", produces = MediaType.APPLICATION_JSON_VALUE)
    public void resetEmail(@RequestParam (value = "token") String token,
                           @Value("${web.controller.registration.redirect.success}") String redirectSuccess,
                           @Value("${web.controller.registration.redirect.error}") String redirectError,
                           HttpServletResponse response) throws IOException {
        String userEmail = registrationService.registerEmail(token);
        if (userEmail != null) {
            if (registrationService.makeAuthorizeRedirectResponse(response, userEmail, variables.getClientUrl() + "/home")) {
                return;
            }
        }
		String error = messageSource.getMessage("registration.email.error", new Object[]{}, LocaleContextHolder.getLocale());
		cookieService.add(response, ERROR_COOKIE_NAME, error, ERROR_COOKIE_AGE, Encoder.BASE64);
        registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectError);
    }
    
    @GetMapping("/social/sign-in")
    public void socialSignIn(@Value("${web.controller.registration.redirect.success}") String redirectSuccess,
                             @Value("${web.controller.registration.redirect.error}") String redirectError,
                             WebRequest request,
                             HttpServletResponse response,
                             HttpSession session) {
        String token = (String) session.getAttribute(DEFAULT_TOKEN_VALUE_SESSION_NAME);
        if (StringUtils.isNotBlank(token)) {
            session.removeAttribute(DEFAULT_TOKEN_VALUE_SESSION_NAME);
            cookieService.add(response, variables.getTokenCookieName(), token, variables.getTokenCookieAge(), Encoder.NON);
            registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectSuccess);
            return;
        }
        String socialError = (String) session.getAttribute(DEFAULT_SOCIAL_ERROR_SESSION_NAME);
        if (StringUtils.isNotBlank(socialError)) {
			session.removeAttribute(DEFAULT_SOCIAL_ERROR_SESSION_NAME);
			sendErrorMessage(redirectError, response, socialError);
			return;
        }
        String activationError = (String) session.getAttribute(DEFAULT_ACTIVATION_ERROR_SESSION_NAME);
        if (StringUtils.isNotBlank(activationError)) {
            session.removeAttribute(DEFAULT_ACTIVATION_ERROR_SESSION_NAME);
			sendErrorMessage(redirectError, response, activationError);
            return;
        }
		String error = messageSource.getMessage("registration.social.error", new Object[]{}, LocaleContextHolder.getLocale());
		cookieService.add(response, ERROR_COOKIE_NAME, error, ERROR_COOKIE_AGE, Encoder.BASE64);
        registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectError);
    }
	
	private void sendErrorMessage(String redirectError, HttpServletResponse response, String socialError) {
		cookieService.add(response, ERROR_COOKIE_NAME, socialError, ERROR_COOKIE_AGE, Encoder.BASE64);
		registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectError);
	}
	
	@GetMapping("/social/sign-up")
    public void socialSignUp(@Value("${web.controller.registration.redirect.success}") String redirectSuccess,
                             @Value("${web.controller.registration.redirect.error}") String redirectError,
                             WebRequest request,
                             HttpServletResponse response) {
        Connection<?> connection = signInUtils.getConnectionFromSession(request);
        if (connection != null) {
            String email = connection.fetchUserProfile().getEmail();
            if (StringUtils.isBlank(email)) {
                registrationService.makeRedirectResponse(response, "/api/v1/registration/social/sign-up/email");
                return;
            }
            if (TRUST_SOCIAL.contains(connection.getKey().getProviderId())) {
                if (registrationService.registerSocialUser(request)) {
                    String success = messageSource.getMessage("registration.social.success", new Object[]{}, LocaleContextHolder.getLocale());
                    cookieService.add(response, SUCCESS_COOKIE_NAME, success, SUCCESS_COOKIE_AGE, Encoder.BASE64);
                    registrationService.makeAuthorizeRedirectResponse(response, email, CLIENT_ID_SOCIAL,
                                                                      variables.getClientUrl() + redirectSuccess);
                    signInUtils.doPostSignUp(email, request);
                    return;
                }
				String error = messageSource.getMessage("registration.social.error", new Object[]{}, LocaleContextHolder.getLocale());
				cookieService.add(response, ERROR_COOKIE_NAME, error, ERROR_COOKIE_AGE, Encoder.BASE64);
                registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectError);
                return;
            } else {
                if (registrationService.registerSocialTemporaryUser(request, email)) {
					String success = messageSource.getMessage("registration.social.confirmEmail", new Object[]{}, LocaleContextHolder.getLocale());
                    cookieService.add(response, SUCCESS_COOKIE_NAME, success, SUCCESS_COOKIE_AGE, Encoder.BASE64);
                    registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectSuccess);
                    signInUtils.doPostSignUp(email, request);
                    return;
                }
            }
        }
		String error = messageSource.getMessage("registration.social.error", new Object[]{}, LocaleContextHolder.getLocale());
		cookieService.add(response, ERROR_COOKIE_NAME, error, ERROR_COOKIE_AGE, Encoder.BASE64);
        registrationService.makeRedirectResponse(response, variables.getClientUrl() + redirectError);
    }

    @GetMapping("/social/sign-up/email")
    public ModelAndView socialSignUpEmail(@ModelAttribute("email") String email, Model model) {
        ModelAndView mav = new ModelAndView("socialEmailForm");
        mav.addAllObjects(model.asMap());
        return mav;
    }

    @PostMapping("/social/sign-up/email")
    public RedirectView socialSignUpEmail(@ModelAttribute("email") String email,
                                          @Value("${web.controller.registration.redirect.success}") String redirectSuccess,
                                          @Value("${web.controller.registration.redirect.error}") String redirectError,
                                          WebRequest request,
                                          RedirectAttributes redirectAttributes,
                                          HttpServletResponse response) {
        EmailContainer emailContainer = EmailContainer.of(email);
        if (!validatorService.isValid(emailContainer)) {
            redirectAttributes.addFlashAttribute("email", email);
            redirectAttributes.addFlashAttribute("error", validatorService.getErrors(emailContainer).get("email"));
            return new RedirectView("/api/v1/registration/social/sign-up/email");
        }
        if (registrationService.registerSocialTemporaryUser(request, email)) {
			String success = messageSource.getMessage("registration.social.confirmEmail", new Object[]{}, LocaleContextHolder.getLocale());
            cookieService.add(response, SUCCESS_COOKIE_NAME, success, SUCCESS_COOKIE_AGE, Encoder.BASE64);
            signInUtils.doPostSignUp(email, request);
            return new RedirectView(variables.getClientUrl() + redirectSuccess);
        }
		String error = messageSource.getMessage("registration.social.error", new Object[]{}, LocaleContextHolder.getLocale());
		cookieService.add(response, ERROR_COOKIE_NAME, error, ERROR_COOKIE_AGE, Encoder.BASE64);
        return new RedirectView(variables.getClientUrl() + redirectError);
    }
}
