package com.spduniversity.feelfree.web.v1;

import com.spduniversity.feelfree.domain.dto.CommentDTO;
import com.spduniversity.feelfree.service.web.AccountService;
import com.spduniversity.feelfree.web.bean.AccountCommentContainer;
import com.spduniversity.feelfree.web.common.ResponseCreator;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/account")
public class AccountCommentController {
	
	@Resource
	private AccountService accountService;
	
	@PostMapping(value = "/comment", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> commentSave(@RequestBody AccountCommentContainer container, Principal principal) {
		ResponseCreator.ObjectValidator target = ResponseCreator.validate(container);
		if (target.isNotValid()) {
			return target.badRequest().build();
		}
		CommentDTO result = accountService.saveComment(container, principal.getName());
		if (result == null) {
			return ResponseCreator.error("comment.error").build();
		}
		return ResponseCreator.ok(result).build();
	}
}
