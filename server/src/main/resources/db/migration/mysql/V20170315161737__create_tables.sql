CREATE TABLE IF NOT EXISTS `address` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `region` varchar(200) NOT NULL,
  `street` varchar(200) NOT NULL,
  `app_number` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `description` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `room_count` tinyint(3) unsigned DEFAULT NULL,
  `living_places_count` tinyint(3) unsigned DEFAULT NULL,
  `wi_fi` tinyint(1) unsigned NOT NULL,
  `washing_machine` tinyint(1) unsigned NOT NULL,
  `refrigerator` tinyint(1) unsigned NOT NULL,
  `hair_dryer` tinyint(1) unsigned NOT NULL,
  `iron` tinyint(1) unsigned NOT NULL,
  `smoking` tinyint(1) unsigned NOT NULL,
  `animals` tinyint(1) unsigned NOT NULL,
  `kitchen_stuff` tinyint(1) unsigned NOT NULL,
  `conditioner` tinyint(1) unsigned NOT NULL,
  `balcony` tinyint(1) unsigned NOT NULL,
  `dishes` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `geolocation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `latitude` float(10,6) NOT NULL,
  `longitude` float(10,6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `latitude` (`latitude`),
  KEY `longitude` (`longitude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `price` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` decimal(7,2) NOT NULL,
  `period` enum('DAY','MONTH') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `period` (`period`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `announcement` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) NOT NULL,
  `title` varchar(100) NOT NULL,
  `short_description` varchar(2000) DEFAULT NULL,
  `geolocation_id` bigint(20) unsigned NOT NULL,
  `description_id` bigint(20) unsigned DEFAULT NULL,
  `address_id` bigint(20) unsigned DEFAULT NULL,
  `price_id` bigint(20) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  KEY `FK_announcement_announcement_descriptione` (`description_id`),
  KEY `FK_announcement_address` (`address_id`),
  KEY `FK_announcement_geolocation` (`geolocation_id`),
  KEY `FK_announcement_price` (`price_id`),
  KEY `hidden` (`hidden`),
  CONSTRAINT `FK_announcement_address` FOREIGN KEY (`address_id`) REFERENCES `address` (`id`),
  CONSTRAINT `FK_announcement_announcement_descriptione` FOREIGN KEY (`description_id`) REFERENCES `description` (`id`),
  CONSTRAINT `FK_announcement_geolocation` FOREIGN KEY (`geolocation_id`) REFERENCES `geolocation` (`id`),
  CONSTRAINT `FK_announcement_price` FOREIGN KEY (`price_id`) REFERENCES `price` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `image` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` mediumblob NOT NULL,
  `type` varchar(50) NOT NULL,
  `name` char(36) NOT NULL,
  `upload_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `image_cache` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` mediumblob NOT NULL,
  `name` varchar(32) NOT NULL,
  `width` smallint(4) NOT NULL,
  `height` smallint(4) NOT NULL,
  `type` varchar(50) NOT NULL,
  `upload_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`width`,`height`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `announcement_image` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) unsigned NOT NULL,
  `image_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `announcement_id` (`announcement_id`,`image_id`),
  KEY `FK_announcement_image_image` (`image_id`),
  CONSTRAINT `FK_announcement_image_announcement_image` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`),
  CONSTRAINT `FK_announcement_image_image` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `profile_email` varchar(254) NOT NULL,
  `password` varchar(60) NOT NULL,
  `image_id` bigint(20) unsigned DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_image` (`image_id`),
  CONSTRAINT `FK_user_image` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `message` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` text NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_message_user` (`user_id`),
  CONSTRAINT `FK_message_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `rent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `check_in_date` datetime NOT NULL,
  `check_out_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `check_in_date` (`check_in_date`),
  KEY `check_out_date` (`check_out_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `announcement_message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) unsigned NOT NULL,
  `message_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `announcement_id` (`announcement_id`,`message_id`),
  KEY `FK_announcement_message_message` (`message_id`),
  CONSTRAINT `FK_announcement_message_announcement` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`),
  CONSTRAINT `FK_announcement_message_message` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `announcement_rent` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `announcement_id` bigint(20) unsigned NOT NULL,
  `rent_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `announcement_id` (`announcement_id`,`rent_id`),
  KEY `FK_announcement_rent_rent` (`rent_id`),
  CONSTRAINT `FK_announcement_rent_announcement_rent` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`),
  CONSTRAINT `FK_announcement_rent_rent` FOREIGN KEY (`rent_id`) REFERENCES `rent` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `email` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `phone` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_announcement` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `announcement_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`announcement_id`),
  KEY `FK_user_announcement_announcement` (`announcement_id`),
  CONSTRAINT `FK_user_announcement_announcement` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`),
  CONSTRAINT `FK_user_announcement_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_email` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `email_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_settings_id` (`user_id`,`email_id`),
  KEY `FK_user_email_email` (`email_id`),
  CONSTRAINT `FK_user_email_email` FOREIGN KEY (`email_id`) REFERENCES `email` (`id`),
  CONSTRAINT `FK_user_email_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_phone` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `phone_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_settins_id` (`user_id`,`phone_id`),
  KEY `FK_user_phone_phone` (`phone_id`),
  CONSTRAINT `FK_user_phone_phone` FOREIGN KEY (`phone_id`) REFERENCES `phone` (`id`),
  CONSTRAINT `FK_user_phone_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_roles_user` (`user_id`),
  KEY `FK_user_roles_roles` (`role_id`),
  CONSTRAINT `FK_user_role_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  CONSTRAINT `FK_user_roles_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
