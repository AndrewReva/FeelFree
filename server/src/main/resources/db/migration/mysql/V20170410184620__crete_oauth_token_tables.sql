CREATE TABLE `oauth_access_token` (
	`token_id` VARCHAR(256) NULL DEFAULT NULL,
	`token` BLOB NULL,
	`authentication_id` VARCHAR(256) NULL DEFAULT NULL,
	`user_name` VARCHAR(256) NULL DEFAULT NULL,
	`client_id` VARCHAR(256) NULL DEFAULT NULL,
	`authentication` BLOB NULL,
	`refresh_token` VARCHAR(256) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `oauth_client_details` (
	`client_id` VARCHAR(256) NOT NULL,
	`resource_ids` VARCHAR(256) NULL DEFAULT NULL,
	`client_secret` VARCHAR(256) NULL DEFAULT NULL,
	`scope` VARCHAR(256) NULL DEFAULT NULL,
	`authorized_grant_types` VARCHAR(256) NULL DEFAULT NULL,
	`web_server_redirect_uri` VARCHAR(256) NULL DEFAULT NULL,
	`authorities` VARCHAR(256) NULL DEFAULT NULL,
	`access_token_validity` INT(11) NULL DEFAULT NULL,
	`refresh_token_validity` INT(11) NULL DEFAULT NULL,
	`additional_information` VARCHAR(4096) NULL DEFAULT NULL,
	`autoapprove` VARCHAR(256) NULL DEFAULT NULL,
	PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `oauth_code` (
	`code` VARCHAR(256) NULL DEFAULT NULL,
	`authentication` BLOB NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `oauth_refresh_token` (
	`token_id` VARCHAR(256) NULL DEFAULT NULL,
	`token` BLOB NULL,
	`authentication` BLOB NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;