CREATE TABLE `authorize` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`unique_name` CHAR(32) NOT NULL,
	`target` ENUM('FEEL_FREE','FACEBOOK','TWITTER') NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `unique_name` (`unique_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_authorize` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` BIGINT(20) UNSIGNED NOT NULL,
	`authorize_id` BIGINT(20) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `FK_unique_user_authorize` (`user_id`, `authorize_id`),
	INDEX `FK_user_authorize_authorize` (`authorize_id`),
	CONSTRAINT `FK_user_authorize_authorize` FOREIGN KEY (`authorize_id`) REFERENCES `authorize` (`id`),
	CONSTRAINT `FK_user_authorize_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;