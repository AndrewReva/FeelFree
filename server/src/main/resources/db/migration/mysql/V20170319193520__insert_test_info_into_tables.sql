INSERT INTO `email` (`id`, `value`) VALUES
	(1, 'additional@email.com'),
	(2, 'main@email.com');

INSERT INTO `user` (`id`, `first_name`, `last_name`, `profile_email`, `password`, `image_id`, `enabled`, `create_date`, `update_date`) VALUES
	(1, 'Igor', 'Yurko', 'igor@email.com', '$2a$06$XBzzPWeP7CHIg4jn5fcHie5ni7.jlXFxxb0FRSv91nHKv./xE2j3u', 6, 1, '2017-03-19 18:20:02', '2017-03-19 18:20:04'),
	(2, 'Vasia', 'Rogov', 'vasia@email.com', '$2a$06$XBzzPWeP7CHIg4jerfcHie5ni7.jlXFxxb0FRSv91nHKv./xE2j3u', 1, 1, '2017-03-19 18:20:02', '2017-03-19 18:20:04'),
	(3, 'Al', 'Pachino', 'clint@gmail.com', '$2a$06$XBzzPWeP7CHIg4jn5ffdds5ni7.jlXFxxb0FRSv91nHKv./xE2j3u', 2, 1, '2017-03-24 10:02:10', '2017-03-24 10:02:13'),
	(4, 'Dima', 'Koshevoy', 'dima@gmail.com', '$2a$06$XBzzPWeP7CHIg4jn5ffdds5ni7.jlXFxxb0FRSv91nHKv./xE2j3u', 3, 1, '2017-03-24 10:02:10', '2017-03-24 10:02:13'),
	(5, 'Natasha', 'Dreval', 'natasha@gmail.com', '$2a$06$XBzzPWeP7CHIg4jn5ffdds5ni7.jlXFxxb0FRSv91nHKv./xE2j3u', 4, 1, '2017-03-24 10:02:10', '2017-03-24 10:02:13'),
	(6, 'Demian', 'Kurilenko', 'demian@gmail.com', '$2a$06$XBzzPWeP7CHIg4jn5ffdds5ni7.jlXFxxb0FRSv91nHKv./xE2j3u', 7, 1, '2017-03-24 10:02:10', '2017-03-24 10:02:13'),
	(7, 'Bogdan', 'Fot', 'bogdan@gmail.com', '$2a$06$XBzzPWeP7CHIg4jn5ffdds5ni7.jlXFxxb0FRSv91nHKv./xE2j3u', 5, 1, '2017-03-24 10:02:10', '2017-03-24 10:02:13'),
  (8, 'fn', 'ln', 'admin@email.com', '$2a$06$gkfGA3YHwI3uFPGQ3VSiSuZjQFxkXWtK17E.lUnbbLr9bwr8G78yC', 22, 1, '2017-03-19 18:20:02', '2017-03-19 18:20:04');

INSERT INTO `message` (`id`, `value`, `user_id`, `create_date`, `update_date`) VALUES
	(1, 'Message 1', 1, '2017-03-19 18:17:42', '2017-03-20 18:17:45'),
	(2, 'Message 2', 2, '2017-03-19 18:17:42', '2017-03-20 18:17:45'),
	(3, 'Message 3', 2, '2017-03-19 18:17:42', '2017-03-20 18:17:45'),
	(4, 'Message 4', 3, '2017-03-24 10:11:17', '2017-03-24 10:11:19'),
	(5, 'Message 5', 4, '2017-03-24 10:11:17', '2017-03-24 10:11:19'),
	(6, 'Message 6', 5, '2017-03-24 10:11:17', '2017-03-24 10:11:19'),
	(7, 'Message 7', 6, '2017-03-24 10:11:17', '2017-03-24 10:11:19'),
	(8, 'Message 8', 3, '2017-03-24 10:11:17', '2017-03-24 10:11:19'),
	(9, 'Message 9', 2, '2017-03-24 10:11:17', '2017-03-24 10:11:19'),
	(10, 'Message 10', 5, '2017-03-24 10:11:17', '2017-03-24 10:11:19'),
	(11, 'Message 10', 4, '2017-03-24 10:11:17', '2017-03-24 10:11:19');

INSERT INTO `phone` (`id`, `value`) VALUES
	(1, '804723456788'),
	(2, '09623123125678');

INSERT INTO `rent` (`id`, `check_in_date`, `check_out_date`) VALUES
	(1, '2017-03-18 18:18:25', '2017-03-25 18:18:33'),
	(2, '2017-03-20 10:14:33', '2017-03-23 10:14:49'),
	(3, '2017-03-21 10:53:53', '2017-03-23 10:54:04');

INSERT INTO `announcement_message` (`id`, `announcement_id`, `message_id`) VALUES
	(1, 1, 1),
	(10, 1, 10),
	(11, 1, 11),
	(2, 3, 2),
	(3, 3, 3),
	(4, 4, 4),
	(6, 5, 5),
	(5, 5, 6),
	(7, 6, 7),
	(8, 7, 8),
	(9, 8, 9);

INSERT INTO `announcement_rent` (`id`, `announcement_id`, `rent_id`) VALUES
	(1, 1, 1),
	(2, 3, 2),
	(3, 4, 3);

INSERT INTO `user_announcement` (`id`, `user_id`, `announcement_id`) VALUES
	(1, 1, 1),
	(2, 2, 3),
	(3, 3, 4),
	(4, 4, 5),
	(5, 5, 6),
	(7, 6, 7),
	(6, 7, 8);


INSERT INTO `user_email` (`id`, `user_id`, `email_id`) VALUES
	(1, 1, 1);

INSERT INTO `user_phone` (`id`, `user_id`, `phone_id`) VALUES
	(1, 1, 1),
	(2, 1, 2);

INSERT INTO `user_role` (`id`, `user_id`, `role_id`) VALUES
	(1, 1, 2),
	(2, 1, 1),
	(3, 2, 2),
	(4, 3, 2),
	(5, 4, 2),
	(6, 5, 2),
	(7, 6, 2),
	(8, 7, 2),
	(9, 8, 2),
	(10, 8, 1);