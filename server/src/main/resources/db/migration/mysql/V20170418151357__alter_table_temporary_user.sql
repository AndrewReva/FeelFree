ALTER TABLE `temporary_user` ADD COLUMN `image_id` BIGINT(20) NULL DEFAULT NULL AFTER `google_id`;
ALTER TABLE `temporary_user` ADD INDEX `token` (`token`);