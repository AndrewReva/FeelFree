DELETE FROM `feel_free`.`message` WHERE  `id`=1;
DELETE FROM `feel_free`.`message` WHERE  `id`=2;
ALTER TABLE `message`
	ALTER `title` DROP DEFAULT;
ALTER TABLE `message`
	CHANGE COLUMN `title` `title_id` BIGINT(20) UNSIGNED NOT NULL AFTER `user_id`;
CREATE TABLE `Message_title` (
	`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
	`text` VARCHAR(170) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
ALTER TABLE `message`
	ADD CONSTRAINT `FK_message_message_title` FOREIGN KEY (`title_id`) REFERENCES `message_title` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;
INSERT INTO `message_title` (`id`, `text`) VALUES (1, 'Hello, I want to rent your appartament');
INSERT INTO `message` (`id`, `conversation_id`, `user_id`, `title_id`, `line_text`, `created_at`) VALUES (1, 1, 6, 1, 'Hello its Demian', '2017-04-10 18:24:29');
INSERT INTO `message` (`id`, `conversation_id`, `user_id`, `title_id`, `line_text`, `created_at`) VALUES (2, 1, 8, 1, 'Hello Demian, its Admin answering to you!', '2017-04-10 18:25:03');