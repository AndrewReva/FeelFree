RENAME TABLE `announcement_message` TO `announcement_comment`;
ALTER TABLE `announcement_comment` ALTER `message_id` DROP DEFAULT;
ALTER TABLE `announcement_comment`
	CHANGE COLUMN `message_id` `comment_id` BIGINT(20) UNSIGNED NOT NULL AFTER `announcement_id`;

ALTER TABLE `announcement_comment`
	DROP FOREIGN KEY `FK_announcement_message_message`;

ALTER TABLE `announcement_comment`
	ADD CONSTRAINT `FK_announcement_comment_comment` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`);

ALTER TABLE `announcement_comment`
	DROP FOREIGN KEY `FK_announcement_message_announcement`;

ALTER TABLE `announcement_comment`
	ADD CONSTRAINT `FK_announcement_comment_announcement` FOREIGN KEY (`announcement_id`) REFERENCES `announcement` (`id`);