INSERT INTO `address` (`id`, `country`, `city`, `region`, `street`, `app_number`) VALUES
  (1, 'Ukraine', 'Cherkassy', 'Chercassy', 'Pilipenco', 5),
  (2, 'Ukraine', 'Kiev', 'Kiev', 'Sobornosty', 2),
  (3, 'Ukraine', 'Smila', 'Cherkassy', 'Marks', 1),
  (4, 'Ukraine', 'Lviv', 'Lviv', 'Horododska', 2),
  (5, 'Ukraine', 'Uman', 'Cherkassy', 'Korolova', 3),
  (6, 'Ukraine', 'Kharkov', 'Kharkov', 'Rymarskaya', 2),
  (7, 'Ukraine', 'Nikolaev', 'Nikolaev', 'Chkalova', 1);

INSERT INTO `price` (`id`, `value`, `period`) VALUES
  (1, 2200.00, 'MONTH'),
  (2, 1200.00, 'MONTH'),
  (3, 2700.00, 'MONTH'),
  (4, 4200.00, 'MONTH'),
  (5, 900.00, 'MONTH'),
  (6, 700.00, 'MONTH'),
  (7, 200.00, 'DAY');

INSERT INTO `geolocation` (`id`, `latitude`, `longitude`) VALUES
  (1, 49.445484, 32.071400),
  (2, 50.447254, 30.523462),
  (3, 49.226990, 31.855101),
  (4, 49.838413, 24.025301),
  (5, 48.764423, 30.237001),
  (6, 49.993500, 36.230385),
  (7, 46.975033, 31.994583);

INSERT INTO `description` (`id`, `room_count`, `living_places_count`, `wi_fi`, `washing_machine`, `refrigerator`, `hair_dryer`, `iron`, `smoking`, `animals`, `kitchen_stuff`, `conditioner`, `balcony`, `dishes`) VALUES
  (1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
  (2, 3, 2, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1),
  (3, 5, 4, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0),
  (4, 4, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1),
  (5, 3, 2, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0),
  (6, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1),
  (7, 3, 2, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0);

INSERT INTO `announcement` (`id`, `uuid`, `title`, `short_description`, `geolocation_id`, `description_id`, `address_id`, `price_id`, `hidden`, `create_date`, `update_date`) VALUES
  (1, 'bb2794a110b43b16a034069ad6ac5696', 'Summer Stone Duplexes', 'Nestled within 4 acres of professionally maintained landscaping, you are sure to find everything you need and more at Summer Stone Duplexes. Enjoy single level living with attached 2-car carports, washer and dryer connections, fully equipped kitchens, spacious closets, exterior storage closets* and large windows. Refreshing swimming pool within community, 24-hour fitness center, billiard\'s room, on-site management and 24-hour emergency maintenance all at your fingertips.', 1, 1, 1, 1, 0, '2017-03-06 10:04:57', '2017-03-18 10:04:57'),
  (3, '0eb97cfb1d5d31c599eb23a423403a14', 'Grandview Heights', 'Grandview Heights is revolutionizing what luxury Glenpool and South Tulsa apartments can be! Choose from a variety of designer floor plans that feature fantastic upscale amenities like private garage parking and refreshing spa-like bathrooms. Enjoy the luxury of modern gourmet kitchens with stainless steel appliances, and apartment interiors with high 9-foot ceilings, or vaulted ceilings on top floors! Relax in your very own enclosed sunroom or take in the refreshing South Tulsa air on your private outdoor patio. Grandview Heights provides the finest luxury apartments in Glenpool and Tulsa, OK.', 2, 2, 2, 2, 0, '2017-03-09 10:04:57', '2017-03-18 10:04:57'),
  (4, 'd0f19c96eea136398803cfee5df78071', 'The Mansions At Riverside', 'The Mansions at Riverside Apartments grace a historic landscape and offer a premiere location in Tulsa\'s southern enclave at the Creek Turnpike...with easy access to Riverside Drive...yet it is really quite removed from the bustle of the city. Access to downtown employment is easy and convenient for those who work and play in the City Center. Bistros, galleries and shopping areas are a short distance away, perfect for casual weekend relaxation and fun.', 3, 3, 3, 3, 0, '2017-03-10 10:04:57', '2017-03-18 10:04:57'),
  (5, '17e21d9cd0223b468c8cac8736d11764', 'Crown Win River', 'Coming Soon!!! With floor plans featuring 9 and 11 ceilings, One and Two bedroom Villas with attached garages and Elevator access to our Penthouse Floor with treetop views, Crown Win River marries luxury with quality of life. Located in the heart of South Tulsa, we are only minutes from major expressways, shopping and entertainment. Our accommodating staff takes care of every detail so you can relax and enjoy your new home.Coming Soon!!! With floor plans featuring 9 and 11 ceilings, One and Two bedroom Villas with attached garages and Elevator access to our Penthouse Floor with treetop views, Crown Win River marries luxury with quality of life. Located in the heart of South Tulsa, we are only minutes from major expressways, shopping and entertainment. Our accommodating staff takes care of every detail so you can relax and enjoy your new home.Coming Soon!!! With floor plans featuring 9 and 11 ceilings, One and Two bedroom Villas with attached garages and Elevator access to our Penthouse Floor with treetop views, Crown Win River marries luxury with quality of life.Located in the heart of South Tulsa, we are only minutes from major expressways, shopping and entertainment. Our accommodating staff takes care of every detail so you can relax and enjoy your new home.Coming Soon!!! Coming Soon!!! With floor plans featuring 9 and 11 ceilings, One and Two bedroom Villas with attached garages and Elevator access to our Penthouse Floor with treetop views, Crown Win River marries luxury with quality of life. Located in the heart of South Tulsa, we are only minutes from major expressways, shopping and entertainment. Our accommodating staff takes care of every detail so you can relax and enjoy your new home.', 4, 4, 4, 4, 0, '2017-03-12 10:04:57', '2017-03-18 10:04:57'),
  (6, '0ecc9db63c533dc7b84ea00a8fb41217', 'Ashford Overlook', 'Welcome home to Ashford Overlook. Offering 1, 2 and 3 bedroom apartment homes, ranging in size from 630-1,000sf. Conveniently located in the Jenks school district, you are close to what you want and what you need. Come visit and pick your new apartment home today!', 5, 5, 5, 5, 0, '2017-03-15 10:04:57', '2017-03-18 10:04:57'),
  (7, 'c1b8d00bb6c43ba2b8e594e323c364aa', 'Red River', 'A HOP, SKIP AND A JUMP from just about everything. In the heart of south Tulsa, you\'ll appreciate being close to Highway 75, I-44 and the Creek Turnpike. Our wonderful location is only minutes from O.R.U., The Plaza Center, Walmart Super Center and the new Tulsa Hills shopping center. Pet Rent is 10 monthly for pets 40 lbs and under, with breed restrictions. If jogging, bike riding or volleyball appeal to you, the RiverParks area is 1/2 mile away. Stop by and make Red River your new home!', 6, 6, 6, 6, 0, '2017-03-17 10:04:57', '2017-03-18 10:04:57'),
  (8, 'eb89e1f40d8a3f45aa92b0488ee25d95', 'The Park at Forest Oaks', 'Just minutes from the Arkansas River and Helmerich Park, The Park at Forest Oaks Apartments is located near the best dining, shopping, and entertainment in the beautiful, natural setting of the Kensington neighborhood. We also feature close proximity to Oral Roberts University, making our community a great location for university students and staff. If you have an insatiable thirst for knowledge, you will love nearby Jenks City Library and Barnes & Noble. Prefer to dine out? Hebert\'s Specialty Meats, India Palace Restaurant, Sushi House, and Eritrean & Ethiopian Cafe are in the immediate area and will satisfy your every culinary craving. When you live at The Park at Forest Oaks Apartments, you can start your day with a walk to Starbucks on East 71 Street and enjoy a searing hot latte. With convenient access to everywhere you want to be, you will enjoy life at The Park at Forest Oaks Apartments.', 7, 7, 7, 7, 0, '2017-03-18 10:04:57', '2017-03-18 10:04:57');