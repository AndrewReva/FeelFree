ALTER TABLE `geolocation`
	DROP INDEX `longitude`,
	DROP INDEX `latitude`,
	ADD INDEX `latitude_longitude` (`latitude`, `longitude`);