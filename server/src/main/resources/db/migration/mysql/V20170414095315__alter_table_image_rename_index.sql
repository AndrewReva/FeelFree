ALTER TABLE `image_cache`
	DROP INDEX `name`,
	ADD UNIQUE INDEX `name_width_height` (`name`, `width`, `height`);