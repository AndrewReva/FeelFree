You need to have mysql database version 5.7
Run project:
1. Create db "feel_free"
2. Enable(Load) Lombok Plugin
3. Change database login and password in /server/resources/application.yml
3. Go to the /server folder -> gradlew BootRun
4. Go to the /client folder -> npm install -> npm start:local
If you have problem with database, you can clean it, choose gradle task flyWayClean
(check database configuration in build.gradle)

Swagger:
1. http://localhost:8080/swagger-ui.html
2. http://localhost:8080/v2/api-docs