import React from 'react';
import {render} from 'react-dom';
import {App} from './app';

import Promise from 'promise-polyfill';

// To add to window
if (!window.Promise) {
    window.Promise = Promise;
}

render(
    <App/>,
    document.getElementById('root')
);


