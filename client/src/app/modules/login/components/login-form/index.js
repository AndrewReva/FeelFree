import './login-form.styl';
import React from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';

import {login} from 'app/services/user/requests';
import {loginSuccess} from 'app/services/user/actions';

import {validate} from './validate';

import {InputGroup} from './components';
import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';
import {Link} from 'kit/components/link';

export const LoginForm = connect(null, {
    loginSuccess
})(reduxForm({
    form: 'login',
    validate
})(props => {
    const submit = values => {
        return login(values).then((json) => {
            props.loginSuccess(json).then(() => {
                props.redirect();
            });
        });
    };

    return (
        <div>
            <form className="login-form" onSubmit={props.handleSubmit(submit)}>
                <h4 className="login-form__title">Login</h4>

                {props.submitSucceeded && (
                    <FormMessage
                        message="Success"
                        type={FormMessageTypes.success}
                        className="login-form__form-message"
                    />
                )}

                {props.error && (
                    <FormMessage message={props.error} type={FormMessageTypes.error} className="login-form__form-message"/>
                )}

                {!props.submitSucceeded && (
                    <div>
                        <Field name="username" component={InputGroup} label="Email"/>
                        <Field name="password" component={InputGroup} label="Password" type="password"/>

                        <Link className="login-form__input-caption" to="/reset-password">Forgot password?</Link>

                        <Button
                            type="submit"
                            className="login-form__button-primary"
                            caption="Login"
                            buttonType={ButtonTypes.default}
                        />

                        <div className="login-form__register">
                            <p className="login-form__register-caption" href="#">Still don't have account?</p>
                            <Button caption="Register" buttonType={ButtonTypes.default} onClick={props.toRegistration}/>
                        </div>
                    </div>
                )}
            </form>
            {!props.submitSucceeded && (
                <div>
                    <form action="http://feelfree.ddns.net:8060/signin/google" method="post"
                    className="login-form__google">
                        <Button
                            caption="Sign in with google"
                            buttonType={ButtonTypes.primary}
                            secondary={true}
                            type="submit"
                        />
                    </form>
                    <form action="http://feelfree.ddns.net:8060/signin/facebook" method="post"
                    className="login-form__facebook">
                        <Button
                            caption="Sign in with facebook"
                            buttonType={ButtonTypes.primary}
                            ternary={true}
                            type="submit"
                        />
                    </form>
                </div>)
            }
        </div>
    );
}));

