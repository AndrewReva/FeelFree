import './login.styl';
import React, {Component} from 'react';
import autobind from 'autobind-decorator';

import {LoginForm} from './components/login-form';

export class Login extends Component {
    @autobind
    redirect() {
        const {location, history} = this.props;
        history.push(location.state ? location.state.from.pathname : '');
    }

    @autobind
    toRegistration() {
        const {history} = this.props;
        history.push('/registration');
    }

    render() {
        return (
            <div className="login">
                <LoginForm redirect={this.redirect} toRegistration={this.toRegistration}/>
            </div>
        );
    }
}
