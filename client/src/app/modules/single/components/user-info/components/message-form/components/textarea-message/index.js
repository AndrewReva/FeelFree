import React from 'react';
import {TextArea} from 'kit/components/textarea/index';
import {ValidationError} from 'kit/components/validation-error';

export const TextAreaMessage = ({ input, label, type, meta: { touched, error, warning} }) => (
    <div className="create-announcement-form__textarea-group">
        <TextArea
            type={type}
            {...input}
            className="message__textarea"
            placeholder={label}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
