import './message-form.styl';
import React from 'react';
import {Button, ButtonTypes} from 'kit/components/button';
import {InputTitle} from './components/input-title';
import {TextAreaMessage} from './components/textarea-message';
import {validate} from './validate';
import { Field, reduxForm } from 'redux-form';
import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import classNames from 'classnames';

export const MessageForm = reduxForm({
    form: 'sendMessage',
    validate
})(props => {
    const submit = values => {
        let newValues = {
            title: values.title,
            text: values.message,
            announcementUuid: props.id
        };

        props.sendMessage(newValues);

        if (props.submitSucceeded) {
            props.reset();
        }
    };

    return (
        <form onSubmit={props.handleSubmit(submit)} className="message-form">
            {props.submitSucceeded && (
                <FormMessage
                    message="Thank you for your letter, owner will contact you soon"
                    type={FormMessageTypes.success}
                />
            )}
            <div className={classNames({'message-form_hide': props.submitSucceeded})}>
                <h4>Title</h4>
                <Field name="title" component={InputTitle} label="Title"/>
                <h4>Message</h4>
                <Field name="message" component={TextAreaMessage} label="Message"/>
                <Button
                    buttonType={ButtonTypes.accent}
                    caption="Send" type="submit"
                    className="message-form__button"
                />
            </div>
        </form>
    );
});
