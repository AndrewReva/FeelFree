import './leave-review.styl';
import React from 'react';
import {CommentTextArea} from './components/comment-textarea';
import {Button, ButtonTypes} from 'kit/components/button';
import {reduxForm, Field} from 'redux-form';

export const LeaveReview = (reduxForm({
    form: 'addComment'
})(props => {

    const submit = values => {
        let newValues = {
            uuid: props.uuid,
            value: values.comment
        };
        if (values.comment) {
            props.addAnnouncementComment(newValues);
        }
        props.reset();
    };

    return (
        <form className="review-form" onSubmit={props.handleSubmit(submit)}>
            <div className="review-form_flex">
                <h4 className="review-form__title">Leave review:</h4>
                <Field name="comment" component={CommentTextArea}/>
            </div>
            <Button
                className="review-form__btn"
                buttonType={ButtonTypes.accent}
                caption="Submit"
                type="submit"
                busy={props.announcements.pendingComments}
                disabled={props.announcements.pendingComments}
            />
        </form>
    );
}));
