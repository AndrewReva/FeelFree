import './single-map.styl';
import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import autobind from 'autobind-decorator';

const Marker = () => <div className="single-map__marker"/>;

@autobind
export class SingleMap extends Component {
    constructor(props) {
        super(props);

        this.state = {
            center: props.center,
            zoom: 12
        };
    }

    onChange({center, zoom}) {
        this.setState({
            center,
            zoom
        });
    }

    render() {
        const {announcement} = this.props;

        return (
            <div className="single-map">
                <div className="single-map__map">
                    <GoogleMapReact
                        onChange={this.onChange}
                        defaultCenter={this.props.defaultCenter}
                        defaultZoom={this.props.defaultZoom}
                        onClick={this.addMarker}
                        center={this.state.center}
                        zoom={this.state.zoom}
                    >
                        <Marker
                            lat={announcement.geolocation.latitude}
                            lng={announcement.geolocation.longitude}
                        />
                    </GoogleMapReact>
                </div>
            </div>
        );
    }
}

SingleMap.defaultProps = {
    defaultCenter: {lat: 59.95, lng: 30.33},
    center: {lat: 59.95, lng: 30.33},
    defaultZoom: 11
};
