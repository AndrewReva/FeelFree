import React, {PropTypes} from 'react';

export const Location = ({address: {city, region, country}}) => {
    return (
        <p>{city}, {region}, {country}</p>
    );
};

Location.propTypes = {
    address: PropTypes.object
};

Location.defaultProps = {
    address: {}
};
