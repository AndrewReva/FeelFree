import './search.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';

import {
    getAnnouncements,
    getNextAnnouncements,
    setSearchParameters,
    getPriceRange
} from 'app/services/announcements/actions';
import {addFavourite, removeFavourite} from 'app/services/account/actions';

import {AnnouncementList} from 'app/components/announcement-list';
import {AnnouncementPreview} from 'app/components/announcement-preview';
import {SearchFormDetail} from 'app/modules/search/components/search-form-detail';
import {SearchMap} from 'app/modules/search/components/search-map';
import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';

import {Container} from 'kit/components/container';
import {Spinner} from 'kit/components/spinner';

import autobind from 'autobind-decorator';
import {isEmpty} from 'lodash';

@autobind
export const Search = connect(state => ({announcements: state.announcements}), {
    getAnnouncements, getNextAnnouncements, setSearchParameters, getPriceRange,
    addFavourite, removeFavourite
})(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hideButton: false,
            clickSearch: false
        };
    }

    componentDidMount() {
        this.props.getAnnouncements();
        this.props.getPriceRange();
    }

    componentWillUnmount() {
        this.props.setSearchParameters({});
    }

    getNextPage() {
        const {pageNumberAnnouncements} = this.props.announcements;
        this.props.getNextAnnouncements({...this.state.values, page: pageNumberAnnouncements + 1});
    }

    onClickSearch() {
        this.setState({clickSearch: true});
    }

    goToSingle(uuid) {
        this.props.history.push(`/announcement/${uuid}`)
    }

    render() {
        const {
            totalPagesAnnouncements,
            pageNumberAnnouncements,
            data,
            error,
            parameters,
            priceRange
        } = this.props.announcements;

        let searchMassage = null;

        let initialValues = {price: {min: priceRange.minPrice, max: priceRange.maxPrice}, details: [], period: 'MONTH'};

        if (!isEmpty(parameters)) {
            initialValues = {
                ...parameters.values,
                price: {min: priceRange.minPrice, max: priceRange.maxPrice},
                details: []
            };
        }

        if (!isEmpty(parameters) || (this.state.clickSearch && !error)) {
            searchMassage = <FormMessage
                                message="Search results"
                                type={FormMessageTypes.success}
                                className="search__search-massage"
                            />;
        }

        if (error) {
            searchMassage = <FormMessage
                                message="No results found. Please, change search filters!"
                                type={FormMessageTypes.error}
                                className="search__search-massage"
                            />;
        }

        return (
            <div className="search">
                {isEmpty(priceRange) || isEmpty(this.props.announcements) ?
                    (<div className="search__spinner"><Spinner/></div>)
                    :
                    (
                        <div>
                            <Container>
                                <SearchFormDetail
                                    getAnnouncements={this.props.getAnnouncements}
                                    announcements={this.props.announcements}
                                    initialValues={initialValues}
                                    onClickSearch={this.onClickSearch}
                                    setSearchParameters={this.props.setSearchParameters}
                                />
                                {this.props.announcements.parameters.values && (
                                    <SearchMap
                                        announcements={this.props.announcements.data}
                                        placeId={this.props.announcements.parameters.values.location ?
                                            this.props.announcements.parameters.values.location.placeId || null
                                            :
                                            null
                                        }
                                        goToSingle={this.goToSingle}
                                    />
                                )}
                            </Container>
                            <div className="search__latest-announcements">
                                <Container>
                                    {searchMassage}
                                    <AnnouncementList>
                                        {Object.values(data).map(announcement => (
                                            <AnnouncementPreview
                                                key={announcement.uuid}
                                                announcement={announcement}
                                                addFavourite={this.props.addFavourite}
                                                removeFavourite={this.props.removeFavourite}
                                            />
                                        ))}
                                    </AnnouncementList>
                                    {pageNumberAnnouncements !== totalPagesAnnouncements - 1 && error === '' && (
                                        <Button
                                            onClick={this.getNextPage}
                                            buttonType={ButtonTypes.accent}
                                            caption="MORE"
                                            className="search__button"
                                            busy={this.props.announcements.pending}
                                            disabled={this.props.announcements.pending}
                                        />
                                    )}
                                </Container>
                            </div>
                        </div>
                    )}
            </div>
        );
    }
});
