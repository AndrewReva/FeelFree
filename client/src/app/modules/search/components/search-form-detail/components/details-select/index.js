import './details.styl';
import React from 'react';
import {Select} from 'kit/components/select';

export const DetailsSelect = ({input, name, options, multi}) => (
    <Select
        className="search-form__multi-select"
        {...input}
        name={name}
        options={options}
        multi={multi}
    />
);
