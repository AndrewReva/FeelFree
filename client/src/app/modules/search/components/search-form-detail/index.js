import './search-form-detail.styl';
import React, {Component} from 'react';

import { Field, reduxForm } from 'redux-form';

import {Button, ButtonTypes} from 'kit/components/button';
import {Icon, IconTypes} from 'kit/components/icon';
import {PeriodToggle} from './components/period-toggle';
import {InputNumberGroup} from './components/input-number-group';
import {PriceSlider} from './components/price-slider';
import {DetailsSelect} from './components/details-select';
// import {Spinner} from 'kit/components/spinner';

//import {PhotosCheckbox} from './components/photos-checkbox';
import {options} from './options';
import {invert} from 'lodash';
import autobind from 'autobind-decorator';
import classNames from 'classnames';

import {LocationAutocomplete} from 'app/components/location-autocomplete';

export const SearchFormDetail = (reduxForm({
    form: 'search'
})(class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openFilters: true
        };
    }

    @autobind
    onClickFilters() {
        this.setState({openFilters: !this.state.openFilters});
    }

    componentDidMount() {
        const openFilters = this.props.announcements.parameters.openFilters;
        if (openFilters !== undefined) {
            this.setState({openFilters});
        }
    }

    render() {
        const {getAnnouncements, handleSubmit, onClickSearch, initialValues, announcements} = this.props;

        const submit = values => {
            let details = invert(values.details.map(detail => detail.value));
            let location = {
                city: '',
                region: '',
                country: '',
                placeId: values.location ? values.location.placeId || '' : ''
            };

            let locationArray = values.location ? values.location.address.split(', ') : [];
            if (locationArray.length === 3) {
                location.city = locationArray[0];
                location.region = locationArray[1];
                location.country = locationArray[2];
            } else if (locationArray.length === 2) {
                location.city = locationArray[0];
                location.country = locationArray[1];
            }

            let newValues = {
                ...location,
                minPrice: values.price.min,
                maxPrice: values.price.max,
                period: values.period,
                rooms: values.rooms ? values.rooms : '',
                livingPlaces: values.livingPlaces ? values.livingPlaces : '',
                wiFi: !!details.wiFi,
                washingMachine: !!details.washingMachine,
                refrigerator: !!details.refrigerator,
                hairDryer: !!details.hairDryer,
                iron: !!details.iron,
                smoking: !!details.smoking,
                animals: !!details.animals,
                kitchenStuff: !!details.kitchenStuff,
                conditioner: !!details.conditioner,
                balcony: !!details.balcony,
                tv: !!details.tv,
                essentials: !!details.essentials,
                shampoo: !!details.shampoo
            };

            if (location.placeId) {
                this.props.setSearchParameters({values: {location: {placeId: location.placeId}}})
            } else {
                this.props.setSearchParameters({});
            }

            getAnnouncements(newValues);
            onClickSearch();
        };

        return (
            <form onSubmit={handleSubmit(submit)}>
                <div className="search-form">
                    <div className="search-form_main">
                        <div>
                            <span className="search-form__item-label">Location</span>
                            <Field name="location" component={LocationAutocomplete} label="Start typing location"/>
                        </div>
                        <div className="search-form__item">
                            <span className="search-form__item-label">Rooms</span>
                            <Field name="rooms" component={InputNumberGroup} min={1} max={10}/>
                        </div>
                        <div className="search-form__item">
                            <span className="search-form__item-label">Living places</span>
                            <Field name="livingPlaces" component={InputNumberGroup} min={1} max={20}/>
                        </div>
                        <div className="search-form__item">
                            <span className="search-form__item-label">Per</span>
                            <Field
                                name="period"
                                toggleType="text"
                                toggleLeft="Day"
                                toggleRight="Month"
                                component={PeriodToggle}
                            />
                        </div>
                        <Button
                            type="submit"
                            caption="SEARCH"
                            buttonType={ButtonTypes.accent}
                            className="search-form__button"
                            busy={announcements.pending}
                            disabled={announcements.pending}
                        />
                    </div>
                    <div className="search-form__more-label">
                        <Icon className="search-form__icon-plus" type={IconTypes.plus}/>
                        <a onClick={this.onClickFilters}>More filters</a>
                    </div>
                    <div className={classNames('search-form_additional', {'search-form_hide': this.state.openFilters})}>
                        <div className="search-form__item search-form__item_price">
                            <span className="search-form__item-label">Price</span>
                            <div className="search-form__item_price-wrapper">
                                <Field
                                    name="price"
                                    minValue={initialValues.price.min}
                                    maxValue={initialValues.price.max}
                                    step={5}
                                    component={PriceSlider}
                                />
                            </div>
                        </div>
                        <div className="search-form__item search-form__item_details">
                            <span className="search-form__item-label">Amenities</span>
                            <div className="search-form__item_details-wrapper">
                                <Field
                                    name="details"
                                    component={DetailsSelect}
                                    options={options}
                                    multi={true}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}));

/*
 <div className={classNames('search-form_secondary', {'search-form_hide': this.state.openFilters})}>
 <div className="search-form__item search-form__item_photos">
 <Field
 name="nonNullImage"
 component={PhotosCheckbox}
 />
 <span className="search-form__item-label search-form__item-label_checkbox">
 Only with photos
 </span>
 </div>
 </div>
 */
