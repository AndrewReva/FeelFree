import './search-map.styl';
import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import autobind from 'autobind-decorator';
import {geocodeByPlaceId} from 'react-places-autocomplete';

const Marker = ({announcement, goToSingle}) => (
    <div
        className="search-map__marker"
        onClick={() => {
            goToSingle(announcement.uuid)
        }}
        onMouseEnter={() => {
            console.log('enter')
        }}
    />
);

@autobind
export class SearchMap extends Component {
    constructor(props) {
        super(props);

        this.state = {
            center: {lat: 59.95, lng: 30.33},
            zoom: 12,
            showMap: false
        };
    }

    componentDidMount() {
        if (this.props.placeId) {
            geocodeByPlaceId(this.props.placeId, (error, center) => {
                this.setState({center, showMap: true, zoom: 11});
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.placeId !== this.props.placeId) {
            geocodeByPlaceId(nextProps.placeId, (error, center) => {
                this.setState({
                    showMap: true,
                    center,
                    zoom: 12
                });
            });
        }
    }

    onChange({center, zoom}) {
        this.setState({
            center,
            zoom
        });
    }

    render() {
        const {announcements, goToSingle} = this.props;

        return (
            <div className="search-map">
                {this.state.showMap && (
                    <div className="search-map__map">
                        <GoogleMapReact
                            onChange={this.onChange}
                            defaultCenter={this.props.defaultCenter}
                            defaultZoom={this.props.defaultZoom}
                            onClick={this.addMarker}
                            center={this.state.center}
                            zoom={this.state.zoom}
                        >
                            {Object.values(announcements).map((a, i) => (
                                <Marker
                                    key={i}
                                    lat={a.geolocation.latitude}
                                    lng={a.geolocation.longitude}
                                    announcement={a}
                                    goToSingle={goToSingle}
                                />
                            ))}
                        </GoogleMapReact>
                    </div>
                )}
            </div>
        );
    }
}

SearchMap.defaultProps = {
    defaultCenter: {lat: 59.95, lng: 30.33},
    center: {lat: 59.95, lng: 30.33},
    defaultZoom: 11
};
