import './add-images-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';

import {addImages} from 'app/services/account/requests';

import {validate} from './validate';

import {renderDropzoneInput} from './components/render-dropzone-input';

import {FormMessage, FormMessageTypes} from 'app/components/form-message/index';
import {Button, ButtonTypes} from 'kit/components/button/index';

export const AddImagesForm = reduxForm({
    form: 'addImages',
    validate
})(props => {
    const submit = values => {
        let formData = new FormData();

        formData.append('uuid', values.uuid);

        for (let i = 0; i < values.files.length; i++) {
            formData.append('files', values.files[i]);
        }

        return addImages(formData).then(() => {
            props.updateAnnouncement();
        });
    };

    return (
        <form className="add-images-form" onSubmit={props.handleSubmit(submit)}>
            <h4 className="add-images__title">Add images</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, you added images"
                    type={FormMessageTypes.success}
                    className="add-images-form__form-message"
                />
            )}

            {props.error && (
                <FormMessage
                    message={props.error}
                    type={FormMessageTypes.error}
                    className="add-images__form-message"
                />
            )}

            <Field name="files" component={renderDropzoneInput}/>

            <Button
                type="submit"
                className="add-images-form__submit-button"
                caption="Add image"
                buttonType={ButtonTypes.accent}
            />
        </form>
    );
});





