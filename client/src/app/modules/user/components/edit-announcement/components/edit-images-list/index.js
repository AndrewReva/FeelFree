import './edit-images-list.styl';
import React from 'react';

export const EditImageList = ({images, deleteImage}) => (
    <div>
        <h4>Edit images</h4>
        <ul className="edit-images-list">
            {images.map((image, i) => (
                <li key={i} className="edit-images-list__item">
                    <div className="edit-images-list__delete-image"
                         onClick={() => {
                             deleteImage(image.uuid);
                         }}
                    >
                        &#x2715;
                    </div>
                    <img className="edit-images-list__image" src={image.value}/>
                </li>
            ))}
        </ul>
    </div>
);
