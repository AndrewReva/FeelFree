import './edit-announcement-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';

import {editAnnouncement} from 'app/services/account/requests';

import {validate} from './validate';

import {options} from './options';

import {LocationInput} from './components/location-input';
import {InputGroup} from './components/input-group';
import {DetailsSelect} from './components/details-select';
import {InputNumberGroup} from './components/input-number-group';
import {TextAreaGroup} from './components/textarea-group';
import {PeriodToggle} from './components/period-toggle';

import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {Button, ButtonTypes} from 'kit/components/button';

export const EditAnnouncementForm = reduxForm({
    form: 'editAnnouncement',
    validate
})(props => {
    const submit = values => {
        let location = {
            city: '',
            region: '',
            country: '',
            latitude: values.location ? values.location.latitude : 0,
            longitude: values.location ? values.location.longitude : 0,
            placeId: values.location ? values.location.placeId : ''
        };

        let locationArray = values.location ? values.location.address.split(', ') : [];
        if (locationArray.length === 3) {
            location.city = locationArray[0];
            location.region = locationArray[1];
            location.country = locationArray[2];
        } else if (locationArray.length === 2) {
            location.city = locationArray[0];
            location.country = locationArray[1];
        }

        let newValues = {
            ...values,
            ...location,
            ...values.details.map(detail => ({[detail.value]: true})).reduce((m, detail) => ({...m, ...detail}), {})
        };
        delete newValues.location;
        delete newValues.details;

        return editAnnouncement(newValues).then(() => {
            props.updateAnnouncement();
        });
    };

    return (
        <form className="edit-announcement-form" onSubmit={props.handleSubmit(submit)}>
            <h3 className="edit-announcement__title">Edit announcement</h3>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, you edited announcement"
                    type={FormMessageTypes.success}
                    className="edit-announcement-form__form-message"
                />
            )}

            {props.error && (
                <FormMessage
                    message={props.error}
                    type={FormMessageTypes.error}
                    className="edit-announcement-form__form-message"
                />
            )}

            <div>
                <Field name="title" component={InputGroup} label="Title"/>

                <Field name="location" component={LocationInput} label="Start typing location"/>

                <Field name="street" component={InputGroup} label="Street"/>
                <Field name="appNumber" component={InputGroup} label="Appartments number"/>

                <Field name="roomCount" component={InputNumberGroup} label="Rooms" min={1} max={10}/>
                <Field
                    name="livingPlacesCount"
                    component={InputNumberGroup}
                    label="Living places"
                    min={1}
                    max={20}
                />

                <Field
                    name="period"
                    toggleType="text"
                    toggleLeft="Day"
                    toggleRight="Month"
                    component={PeriodToggle}
                />
                <Field name="price" component={InputGroup} label="Price" type="number"/>

                <Field name="shortDescription" component={TextAreaGroup} label="Short Description"/>

                <Field
                    name="details"
                    component={DetailsSelect}
                    options={options}
                    multi={true}
                />

                <Button
                    type="submit"
                    className="edit-announcement-form__submit-button"
                    caption="Save"
                    buttonType={ButtonTypes.accent}
                />
            </div>
        </form>
    );
});
