export const validate = values => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Title is required';
    } else if (values.title > 100) {
        errors.title = 'Can\'t be longer then 100 character';
    }

    if (!values.location.address) {
        errors.location = 'Valid location is required';
    }

    if (!values.street) {
        errors.street = 'Street is required';
    }

    if (!values.street) {
        errors.street = 'Street is required';
    }
    if (!values.roomCount) {
        errors.roomCount = 'Room count is required';
    }
    if (!values.livingPlacesCount) {
        errors.livingPlacesCount = 'Living places count is required';
    }
    if (!values.price) {
        errors.price = 'Price is required';
    } else if (values.price > 999999) {
        errors.price = 'Can\' be more than 999999';
    }
    return errors;
};
