import {isEmpty} from 'lodash';

export const validate = values => {
    const errors = {};
    if (isEmpty(values.files)) {
        errors.files = 'No images selected';
    } else if (values.files.length > 10) {
        errors.files = 'Maximum 10 images allowed';
    }

    if (!values.uuid) {
        errors._error = 'You need to click Create announcement first to add images';
    }
    return errors;
};
