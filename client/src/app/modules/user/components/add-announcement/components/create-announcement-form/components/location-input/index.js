import './location-input.styl';
import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import autobind from 'autobind-decorator';
import PlacesAutocomplete from 'react-places-autocomplete';
import {geocodeByPlaceId} from 'react-places-autocomplete';

import {ValidationError} from 'kit/components/validation-error';

const Marker = () => <div className="location-form-group__marker"/>;

@autobind
export class LocationInput extends Component {
    constructor(props) {
        super(props);

        this.state = {
            value: this.props.input.value.address || '',
            marker: null,
            geolocation: {
                lat: 0,
                lng: 0
            },
            showMap: false,
            zoom: this.props.zoom
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.input.value.address !== this.props.input.value.address) {
            this.setState({value: nextProps.input.value.address});
        }
    }


    handleChange(value) {
        this.setState({value});
    }

    handleSelect(value, placeId) {
        geocodeByPlaceId(placeId, (error, geolocation) => {
            this.setState({value, geolocation, showMap: true, zoom: 11});
            this.props.input.onChange({
                address: value,
                latitude: geolocation.lat,
                longitude: geolocation.lng,
                placeId
            });
        });
    }

    addMarker({lat, lng}) {
        this.setState({markers: (
            <Marker
                lat={lat}
                lng={lng}
            />)
        });
        this.props.input.onChange({
            ...this.props.input.value,
            latitude: lat,
            longitude: lng
        });
    }

    onChange({center, zoom}) {
        this.setState({
            geolocation: center,
            zoom
        });
    }

    render() {
        const {error, submitFailed} = this.props.meta;

        const inputProps = {
            value: this.state.value,
            type: 'search',
            onChange: this.handleChange,
            onFocus: this.props.input.onFocus,
            placeholder: this.props.label
        };

        const cssClasses = {
            root: 'location-form-group',
            input: 'input input_primary location-form-group__input',
            autocompleteContainer: 'location-form-group__autocomplete-container',
            autocompleteItem: 'location-form-group__autocomplete-item',
            autocompleteItemActive: 'location-form-group__autocomplete-item_active'
        };

        const options = {
            bounds: new window.google.maps.LatLngBounds(
                new window.google.maps.LatLng(44.3837, 22.1776),
                new window.google.maps.LatLng(52.3315, 40.2118),
            ),
            types: ['(cities)']
        };

        return (
            <div className="create-announcement-form__location">
                <div>
                    <p>Select location to see map</p>
                    <PlacesAutocomplete
                        inputProps={inputProps}
                        onSelect={this.handleSelect}
                        classNames={cssClasses}
                        options={options}
                        clearItemsOnError={true}
                    />
                    {submitFailed && error && (
                        <ValidationError>
                            {error}
                        </ValidationError>
                    )}
                </div>
                {this.state.showMap && (
                    <div className="create-announcement-form__map">
                        <GoogleMapReact
                            onChange={this.onChange}
                            defaultCenter={this.props.defaultCenter}
                            defaultZoom={this.props.defaultZoom}
                            onClick={this.addMarker}
                            center={this.state.geolocation}
                            zoom={this.state.zoom}
                        >
                            {this.state.markers}
                        </GoogleMapReact>
                    </div>
                )}
            </div>
        );
    }
}

LocationInput.defaultProps = {
    defaultCenter: {lat: 59.95, lng: 30.33},
    center: {lat: 59.95, lng: 30.33},
    defaultZoom: 11
};
