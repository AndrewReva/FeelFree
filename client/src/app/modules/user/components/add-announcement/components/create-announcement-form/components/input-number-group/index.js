import React from 'react';
import {InputNumber} from 'kit/components/input-number/index';
import {ValidationError} from 'kit/components/validation-error/index';

export const InputNumberGroup = ({input, name, label, min, max, meta: {touched, error}}) => (
    <div className="create-announcement-form__input-number-group">
        <p>{label}</p>
        <InputNumber
            {...input}
            name={name}
            min={min}
            max={max}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
