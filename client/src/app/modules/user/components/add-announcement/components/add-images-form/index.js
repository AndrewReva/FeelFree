import './add-images-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';




import {validate} from './validate';

import {renderDropzoneInput} from '../create-announcement-form/components/render-dropzone-input';

import {FormMessage, FormMessageTypes} from 'app/components/form-message/index';

export const AddImagesForm = reduxForm({
    form: 'addImagesCreate',
    validate
})(props => {
    return (
        <form className="add-images-form" onSubmit={props.handleSubmit}>
            <h4 className="add-images__title">Add images</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, you added images"
                    type={FormMessageTypes.success}
                    className="add-images-form__form-message"
                />
            )}

            {props.submitFailed && props.error && (
                <FormMessage
                    message={props.error}
                    type={FormMessageTypes.error}
                    className="add-images__form-message"
                />
            )}

            <Field name="files" component={renderDropzoneInput}/>
        </form>
    );
});





