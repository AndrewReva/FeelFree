import './user-announcements.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'autobind-decorator';

import {getUserAnnouncements,
deleteAnnouncement,
toggleHideAnnouncement,
removeFavourite,
addFavourite, getRentRequests, confirmRentRequest, rejectRentRequest} from 'app/services/account/actions';

import {AnnouncementList} from 'app/components/announcement-list';
import {AnnouncementDetailed} from 'app/components/announcement-detailed';

import {Button, ButtonTypes} from 'kit/components/button';
import {Spinner} from 'kit/components/spinner';

@autobind
export const UserAnnouncements = connect(state => ({account: state.account}), {
    getUserAnnouncements, deleteAnnouncement, toggleHideAnnouncement, removeFavourite, addFavourite, getRentRequests,
    confirmRentRequest, rejectRentRequest
})(class extends Component {
    componentDidMount() {
        this.props.getUserAnnouncements();
    }

    toEdit(id) {
        this.props.history.push(`/user/edit/${id}`);
    }

    toAdd() {
        this.props.history.push('/user/add');
    }

    render() {
        const {pending, announcements} = this.props.account;

        return (
            <div>
                <Button
                    className="user-announcements__add-btn"
                    buttonType={ButtonTypes.accent}
                    caption="Add New"
                    onClick={this.toAdd}
                />
                {pending ?
                    <div className="user-announcements__spinner-container"><Spinner/></div>
                    :
                    <AnnouncementList>
                        {Object.values(announcements).map(announcement => (
                            <div className="user-announcements__announcement" key={announcement.uuid}>
                                <AnnouncementDetailed
                                    announcement={announcement}
                                    toEdit={this.toEdit}
                                    deleteAnnouncement={this.props.deleteAnnouncement}
                                    toggleHideAnnouncement={this.props.toggleHideAnnouncement}
                                    removeFavourite={this.props.removeFavourite}
                                    addFavourite={this.props.addFavourite}
                                    getRentRequests={this.props.getRentRequests}
                                    confirmRentRequest={this.props.confirmRentRequest}
                                    rejectRentRequest={this.props.rejectRentRequest}
                                />
                            </div>
                        ))}
                    </AnnouncementList>
                }
            </div>
        );
    }
});

