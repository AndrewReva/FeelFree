import './user-profile.styl';
import React from 'react';

import {EditPasswordForm} from './components/edit-password-form';
import {EditEmailForm} from './components/edit-email-form';
import {EditInfoForm} from './components/edit-info-form';

import {Spinner} from 'kit/components/spinner';

export const UserProfile = ({user, updateUser}) => {
    const {firstName, lastName, emails, phones, profileEmail} = user.data;

    const infoInitialValues = {
        firstName,
        lastName,
        emails: emails.map(emailObj => emailObj.value),
        phones: phones.map(phoneObj => phoneObj.value)
    };
    const emailInitialValues = {email: profileEmail};


    return (
        <div className="user-profile">
            {user.loaded ?
                (
                    <div>
                        <EditPasswordForm/>
                        <EditEmailForm initialValues={emailInitialValues}/>
                        <EditInfoForm initialValues={infoInitialValues} updateUser={updateUser}/>
                    </div>
                )
                :
                (
                    <div className="user-profile__spinner-container"><Spinner/></div>
                )
            }
        </div>
    );
};
