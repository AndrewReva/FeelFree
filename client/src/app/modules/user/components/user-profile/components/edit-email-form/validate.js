export const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'Please enter new email';
        // eslint-disable-next-line max-len
    } else if (!/^[_A-Za-z0-9-\+]+(\..[_A-Za-z0-9-]+)*@[A-Za-zА-Яа-я0-9-_]+(\.[A-Za-zА-Яа-я0-9-_]+)*(\.[A-Za-zА-Яа-я0-9-_]{2,})*([^0-9!@#$%+^&*\(\);:\'\"\\?.,/\[\]{}<>№ ]+)$/.test(values.email)) {
        errors.email = 'Please enter valid email address';
    }
    return errors;
};
