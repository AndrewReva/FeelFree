import './render-fields.styl';
import React from 'react';
import {Field} from 'redux-form';

import {Input, InputTypes} from 'kit/components/input';
import {Button, ButtonTypes} from 'kit/components/button';
import {ValidationError} from 'kit/components/validation-error/index';

const InputGroup = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div className="render-fields__input-group">
            <Input
                type={type}
                {...input}
                className="render-fields__input"
                placeholder={label}
                inputType={InputTypes.primary}
            />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);

export const RenderFields = groupName => ({fields, label, meta: {touched, error}}) => (
    <div className="render-fields">
        <h5>{label}</h5>
        <Button
            type="button"
            caption={`Add ${groupName}`}
            buttonType={ButtonTypes.default}
            onClick={() => fields.push('')}
        />
        <ul className="render-fields__field-list">
            {fields.map((field, index) =>
                <li key={index} className="render-fields__field-group">
                    <Field
                        name={field}
                        type="text"
                        component={InputGroup}
                        label={`Enter ${groupName}`}
                    />
                    <Button
                        type="button"
                        caption="Remove"
                        buttonType={ButtonTypes.default}
                        onClick={() => fields.remove(index)}
                    />
                </li>
            )}
        </ul>
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
