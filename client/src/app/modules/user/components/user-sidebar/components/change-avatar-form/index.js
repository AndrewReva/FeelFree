import './change-avatar-form.styl';
import React from 'react';
import {reduxForm, Field} from 'redux-form';

import {changeAvatar} from 'app/services/account/requests';
import {avatarChanged} from 'app/services/user/actions';

import {validate} from './validate';

import {renderDropzoneInput} from './components/render-dropzone-input';

import {FormMessage, FormMessageTypes} from 'app/components/form-message/index';
import {Button, ButtonTypes} from 'kit/components/button/index';

export const ChangeAvatarForm = reduxForm({
    form: 'changeAvatar',
    validate
})(props => {
    const submit = values => {
        let formData = new FormData();

        for (let i = 0; i < values.files.length; i++) {
            formData.append('file', values.files[i]);
        }

        return changeAvatar(formData).then((image) => {
            avatarChanged(image)(props.dispatch);
        });
    };

    return (
        <form className="change-avatar-form" onSubmit={props.handleSubmit(submit)}>
            <h4 className="change-avatar-form__title">Change avatar</h4>

            {props.submitSucceeded && (
                <FormMessage
                    message="Success, your avatar changed"
                    type={FormMessageTypes.success}
                    className="change-avatar-form__form-message"
                />
            )}

            {props.error && (
                <FormMessage
                    message={props.error}
                    type={FormMessageTypes.error}
                    className="change-avatar-form__form-message"
                />
            )}

            <Field name="files" component={renderDropzoneInput}/>

            <Button
                type="submit"
                className="change-avatar-form__submit-button"
                caption="Add image"
                buttonType={ButtonTypes.accent}
            />
        </form>
    );
});





