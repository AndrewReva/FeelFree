import {isEmpty} from 'lodash';

export const validate = values => {
    const errors = {};
    if (isEmpty(values.files)) {
        errors.files = 'No image selected';
    }
    return errors;
};
