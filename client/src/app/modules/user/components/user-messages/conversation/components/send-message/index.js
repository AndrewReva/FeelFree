import './send-message.styl';
import React from 'react';
import {Button, ButtonTypes} from 'kit/components/button';
import {validate} from './validate';
import { Field, reduxForm } from 'redux-form';
import {FormMessage, FormMessageTypes} from 'app/components/form-message';
import {TextArea} from 'kit/components/textarea/index';
import {ValidationError} from 'kit/components/validation-error';
import classNames from 'classnames';


const TextAreaMessage = ({ input, label, type, meta: { touched, error, warning} }) => (
    <div>
        <TextArea
            type={type}
            {...input}
            placeholder={label}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);

export const SendMessage = reduxForm({
    validate
})(props => {
    const submit = values => {
        let newValues = {
            text: values.message,
            conversationId: props.id
        };

        props.sendConversationMessage(newValues);
        props.reset();
    };

    return (
        <form onSubmit={props.handleSubmit(submit)} className="messages-form">
            <div>
                <Field name="message" component={TextAreaMessage}/>
                <Button
                    buttonType={ButtonTypes.accent}
                    caption="Send" type="submit"
                    className="messages-form__button"
                />
            </div>
        </form>
    );
});
