import './kit-demo.styl';
import React from 'react';

import {Header} from 'app/components/header';
import {Footer} from 'app/components/footer';

import {Container} from 'kit/components/container';
import {Button, ButtonTypes} from 'kit/components/button';
import {Icon, IconTypes} from 'kit/components/icon';
import {IconButton} from 'kit/components/icon-button';
import {Input, InputTypes} from 'kit/components/input';
import {Select} from 'kit/components/select';
import {TextArea} from 'kit/components/textarea';
import {Checkbox} from 'kit/components/checkbox';
import {InputNumber} from 'kit/components/input-number';
import {Toggle} from 'kit/components/toggle';
import {Carousel, CarouselTypes} from 'kit/components/carousel';
import {DatePicker} from 'kit/components/date-picker';
import {RangeSlider} from 'kit/components/range-slider';
import {Spinner} from 'kit/components/spinner';

import kitDemoLogo from './kit-demo-logo.png';

const urls = [
    'http://lorempixel.com/360/244/cats/',
    'http://lorempixel.com/640/480/cats/',
    'http://lorempixel.com/800/600/cats/'
];

const urls2 = [
    'http://lorempixel.com/1140/600/cats/',
    'http://lorempixel.com/1280/720/cats/',
    'http://lorempixel.com/1920/1080/cats/'
];

const options = [
    {'value': 'value 1', 'label': 'label 1'},
    {'value': 'value 2', 'label': 'label 2'},
    {'value': 'value 3', 'label': 'label 3'}
];

export const KitDemo = () => (
    <div>
        <Header logo={kitDemoLogo}/>
        <div className="demo">
            <Container>
                <div className="demo__section">
                    <h2>Buttons</h2>
                    <div><Button caption="Login" buttonType={ButtonTypes.default}/></div>
                    <div><Button caption="Search" buttonType={ButtonTypes.accent}/></div>
                    <div><Button caption="Register" buttonType={ButtonTypes.primary}/></div>
                    <div><Button caption="Log in" buttonType={ButtonTypes.primary}/></div>
                    <div><Button caption="Login with Google" buttonType={ButtonTypes.primary} secondary={true}/></div>
                    <div>
                        <Button caption="Register with Facebook" buttonType={ButtonTypes.primary} ternary={true}/>
                    </div>
                </div>
                <hr/>
                <div className="demo__section">
                    <h2>Icon Buttons</h2>
                    <span><IconButton icon={IconTypes.facebook}/></span>
                    <span><IconButton icon={IconTypes.twitter}/></span>
                </div>
                <hr/>
                <div className="demo__section">
                    <h2>Icons</h2>
                    {Object.values(IconTypes).map((iconType, index) => (
                        <span key={index}>
                            <Icon type={iconType}/>
                        </span>
                    ))}
                </div>
                <hr/>
                <div className="demo__section">
                    <h2>Form</h2>
                    <div><Input placeholder="First Name" inputType={InputTypes.primary}/></div>
                    <div><Input placeholder="Country*" inputType={InputTypes.secondary}/></div>
                    <div><TextArea placeholder="Message"/></div>
                    <div><Checkbox/></div>
                    <div><Checkbox checked={true}/></div>
                    <div><InputNumber/></div>
                    <div><Select options={options} multi={true}/></div>
                </div>
                <hr/>
                <div className="demo__section">
                    <h2>Toggle</h2>
                    <div>
                        <Toggle
                            toggleLeft={<Icon type={IconTypes.list}/>}
                            toggleRight={<Icon type={IconTypes.grid}/>}
                            value={false}
                        />
                    </div>
                    <div>
                        <Toggle toggleLeft="Day" toggleRight="Month" value={true}/>
                    </div>
                </div>
                <hr/>
                <div className="demo__section">
                    <h2>Carousel</h2>
                    <Carousel imgUrls={urls} type={CarouselTypes.small}/>
                    <Carousel imgUrls={urls2} type={CarouselTypes.large}/>
                </div>
                <div className="demo__section">
                    <h2>Date peacker</h2>
                    <DatePicker/>
                </div>
                <div className="demo__section demo__section_color">
                    <h2>Range slider</h2>
                    <RangeSlider/>
                </div>
                <div className="demo__section">
                    <h2>Spinner</h2>
                    <Spinner/>
                </div>

            </Container>
        </div>
        <Footer/>
    </div>
);
