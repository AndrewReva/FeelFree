import './root.styl';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Switch, Route, Redirect} from 'react-router-dom';

import {checkAuthentication} from 'app/services/user/actions';

import {Home} from '../home';
import {Search} from '../search';
import {Login} from '../login';
import {Registration} from '../registration';
import {Single} from '../single';
import {User} from '../user';
import {ConfirmRegistration} from '../confirm-registration';
import {ResetPassword} from '../reset-password';

import {Header} from 'app/components/header';
import {Footer} from 'app/components/footer';
import {PrivacyPolicy} from 'app/components/footer/components/privacy-policy';

import {Spinner} from 'kit/components/spinner';

import feelFreeLogo from './feel-free-logo.png';

export const Root = connect(state => ({user: state.user}), {
    checkAuthentication
})(class extends Component {
    componentDidMount() {
        this.props.checkAuthentication();
    }

    render() {
        const {user, location} = this.props;

        return (
            <div className="root">
                {user.checked ?
                    (
                        <div>
                            <Header logo={feelFreeLogo} user={user} location={location}/>
                            <Switch>
                                <Route path="/home" component={Home}/>
                                <Route path="/search" component={Search}/>
                                <Route path="/privacy-policy" component={PrivacyPolicy}/>
                                <LoggedOutRoute path="/login" component={Login} user={user}/>
                                <Route path="/reset-password" component={ResetPassword}/>
                                <LoggedOutRoute path="/registration" component={Registration} user={user}/>
                                <Route path="/announcement/:id" component={Single}/>
                                <LoggedInRoute path="/user" component={User} user={user}/>
                                <Route path="/confirm-registration/:status" component={ConfirmRegistration}/>

                                <Route path="/fakeredirect" render={() => <div>REDIRECT TEST</div>}/>

                                <Redirect from="/" to="/home"/>
                            </Switch>
                            <Footer/>
                        </div>
                    )
                    :
                    (
                        <div className="root__spinner-container"><Spinner/></div>
                    )
                }
            </div>
        );
    }
});

const LoggedOutRoute = ({component, user, ...rest}) => (
    <Route
        {...rest}
        render={props => (
            !user.isLoggedIn ?
                (React.createElement(component, props))
                :
                (<Redirect to={{pathname: '/home'}}/>)
        )}
    />
);

const LoggedInRoute = ({component, user, ...rest}) => (
    <Route
        {...rest}
        render={props => (
            user.isLoggedIn ?
                (React.createElement(component, props))
                :
                (<Redirect to={{pathname: '/login', state: {from: props.location}}}/>)
        )}
    />
);



