export const validate = values => {
    const errors = {};
    if (values.firstName && values.firstName.length > 20) {
        errors.firstName = 'Can\'t be longer then 20 character';
    }
    if (values.lastName && values.lastName.length > 20) {
        errors.lastName = 'Can\'t be longer then 20 character';
    }
    if (!values.profileEmail) {
        errors.profileEmail = 'Please enter valid email address';
        // eslint-disable-next-line max-len
    } else if (!/^[_A-Za-z0-9-\+]+(\..[_A-Za-z0-9-]+)*@[A-Za-zА-Яа-я0-9-_]+(\.[A-Za-zА-Яа-я0-9-_]+)*(\.[A-Za-zА-Яа-я0-9-_]{2,})*([^0-9!@#$%+^&*\(\);:\'\"\\?.,/\[\]{}<>№ ]+)$/i
            .test(values.profileEmail)) {
        errors.profileEmail = 'Please enter valid email address';
    }
    if (!values.password) {
        errors.password = 'Please enter password';
    } else if (values.password.length < 7 || values.password.length > 15) {
        errors.password = 'Your password must be 7-15 characters';
    } else if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()]{1,}$/.test(values.password)) {
        errors.password = 'Your password is not strong enough';
    }
    if (!values.passwordConfirm) {
        errors.passwordConfirm = 'Please repeat password';
    } else if (values.passwordConfirm !== values.password) {
        errors.passwordConfirm = 'Password does not match';
    }
    if (!values.terms) {
        errors.terms = 'You must agree to our Terms and Conditions';
    }
    return errors;
};
