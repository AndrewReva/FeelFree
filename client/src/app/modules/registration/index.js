import './registration.styl';
import React, {Component} from 'react';
import autobind from 'autobind-decorator';

import {RegistrationForm} from './components/registration-form';

export class Registration extends Component {
    @autobind
    toLogin() {
        const {history} = this.props;
        history.push('/login');
    }

    render() {
        return (
            <div className="registration">
                <RegistrationForm toLogin={this.toLogin}/>
            </div>
        );
    }
}
