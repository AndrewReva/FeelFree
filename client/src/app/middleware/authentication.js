import {USER_CHECK_AUTHENTICATION, USER_LOGIN_SUCCESS, USER_LOGOUT, GET_USER_FAILURE} from '../services/user/constants';
import {GET_ANNOUNCEMENTS_FAILURE, GET_SINGLE_ANNOUNCEMENT_FAILURE} from '../services/announcements/constants';
import {getUser} from '../services/user/actions';

const ERROR_INVALID_TOKEN = 'invalid_token';
const ERROR_UNAUTHORIZED = 'unauthorized';

const getCookie = name => {
    let value = '; ' + document.cookie;
    let parts = value.split('; ' + name + '=');
    if (parts.length === 2) return parts.pop().split(';').shift();
}

export default store => next => action => {
    switch (action.type) {
        case USER_CHECK_AUTHENTICATION:
            try {
                const token = localStorage.getItem('token');
                const cookie = getCookie('access_token');

                if (cookie) {
                    localStorage.setItem('token', cookie);
                    getUser()(store.dispatch, store.getState);
                } else if (token !== null) {
                    getUser()(store.dispatch, store.getState);
                    return null;
                } else {
                    next(action);
                }
            } catch (err) {
                throw err;
            }
            break;

        case USER_LOGIN_SUCCESS:
            try {
                localStorage.setItem('token', action.authData.access_token);
                return getUser()(store.dispatch, store.getState);
            } catch (err) {
                throw err;
            }

        case USER_LOGOUT:
            try {
                localStorage.removeItem('token');
                next(action);
            } catch (err) {
                throw err;
            }
            break;

        case GET_ANNOUNCEMENTS_FAILURE:
        case GET_SINGLE_ANNOUNCEMENT_FAILURE:
        case GET_USER_FAILURE:
            if (action.error === ERROR_INVALID_TOKEN || action.error === ERROR_UNAUTHORIZED) {
                try {
                    localStorage.removeItem('token');
                    window.location.assign('/login');
                } catch (err) {
                    throw err;
                }
            }
            break;
    }

    return next(action);
};
