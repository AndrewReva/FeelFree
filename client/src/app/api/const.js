const CONTENT_TYPE_JSON = 'application/json;charset=UTF-8';
const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';
const CONTENT_TYPE_MULTI_FORM = 'multipart/form-data';

export {CONTENT_TYPE_JSON, CONTENT_TYPE_FORM, CONTENT_TYPE_MULTI_FORM};
