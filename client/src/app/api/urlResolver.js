const apiRoot = process.env.API_ROOT; // test

export default (url, extraParams) => {
    let params = {...extraParams};
    return `${apiRoot}${url}?${Object.entries(params).map(([key, value]) => `${key}=${String(value)}`).join('&')}`;
};
