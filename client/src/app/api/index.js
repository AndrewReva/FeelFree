import urlResolver from './urlResolver';
import {CONTENT_TYPE_JSON, CONTENT_TYPE_MULTI_FORM} from './const';
import 'isomorphic-fetch';

export const CALL_API = Symbol('Call API');

const callApi = endpoint => endpoint()
    .then(response => response.json().then(json => {
        if (response.status === 401) {
            return Promise.reject({error: 'unauthorized'});
        }
        if (!response.ok) {
            return Promise.reject(json);
        }
        return json;
    }));

export default store => next => action => {
    const callAPI = action[CALL_API];
    if (typeof callAPI === 'undefined') {
        return next(action);
    }

    let {endpoint} = callAPI;
    const {types} = callAPI;

    const actionWith = data => {
        const finalAction = {...action, ...data};
        delete finalAction[CALL_API];
        return finalAction;
    };

    const [requestType, successType, failureType] = types;
    next(actionWith({type: requestType}));

    return callApi(endpoint).then(
        response => next(actionWith({
            response,
            type: successType
        })),
        error => next(actionWith({
            type: failureType,
            error: error.error || 'Something bad happened'
        }))
    );
};


const checkToken = () => {
    try {
        const token = localStorage.getItem('token');
        if (token === null) {
            return {};
        }
        return {'Authorization': 'bearer ' + token};
    } catch (err) {
        return {};
    }
};

const GET_CONFIG = () => ({
    headers: {'Accept': CONTENT_TYPE_JSON, ...checkToken()}
});
const get = (url, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...GET_CONFIG()});

const POST_CONFIG = () => ({
    method: 'POST',
    headers: {'Content-Type': CONTENT_TYPE_JSON, 'Accept': CONTENT_TYPE_JSON, ...checkToken()}
});
const post = (url, body, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...POST_CONFIG(), body: JSON.stringify(body)});

const PUT_CONFIG = () => ({
    method: 'PUT',
    headers: {'Content-Type': CONTENT_TYPE_JSON, 'Accept': CONTENT_TYPE_JSON, ...checkToken()}
});
const put = (url, body, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...PUT_CONFIG(), body: JSON.stringify(body)});

const REMOVE_CONFIG = () => ({
    method: 'DELETE',
    headers: {'Content-Type': CONTENT_TYPE_JSON, 'Accept': CONTENT_TYPE_JSON, ...checkToken()}
});
const remove = (url, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...REMOVE_CONFIG()});


// wip
const PUT_WITH_CREDENTIALS_CONFIG = () => ({
    method: 'PUT',
    credentials: 'include',
    headers: {'Content-Type': CONTENT_TYPE_JSON, 'Accept': CONTENT_TYPE_JSON, ...checkToken()}
});
const putCred = (url, body, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...PUT_WITH_CREDENTIALS_CONFIG(), body: JSON.stringify(body)});

const POST_WITH_CREDENTIALS_CONFIG = () => ({
    method: 'POST',
    credentials: 'include',
    headers: {'Content-Type': CONTENT_TYPE_JSON, 'Accept': CONTENT_TYPE_JSON, ...checkToken()}
});
const postCred = (url, body, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...POST_WITH_CREDENTIALS_CONFIG(), body: JSON.stringify(body)});


const POST_IMAGE_CONFIG = {
    method: 'POST',
    headers: {
        'Accept': CONTENT_TYPE_JSON,
        ...checkToken()
    }
};
const postImage = (url, formData, extraParams = {}) =>
    fetch(urlResolver(url, extraParams), {...POST_IMAGE_CONFIG, body: formData});


export {get, post, put, remove, putCred, postCred, postImage};
