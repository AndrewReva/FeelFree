import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import api from '../api';
import rootReducer from '../reducers';
import authentication from '../middleware/authentication';

export default function configureStore(preloadedState) {
    return createStore(
        rootReducer,
        preloadedState,
        compose(applyMiddleware(thunk, api, authentication))
    );
}
