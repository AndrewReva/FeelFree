import {SubmissionError} from 'redux-form';
import {post} from 'app/api';

const submitForm = endpoint => endpoint()
    .then(res => res.json().then(json => {
        if (!res.ok) {
            throw new SubmissionError({...json, _error: 'Error registering'});
        }
        return json;
    }));

export const register = values => submitForm(() => post('v1/registration', values));
