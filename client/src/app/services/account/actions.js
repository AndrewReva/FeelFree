import {CALL_API, get, put, remove, post} from 'app/api';
import * as ActionTypes from './constants';

export const addFavourite = uuid => (dispatch, getState) => {
    const {pending} = getState().account;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.ADD_FAVOURITE_REQUEST,
                ActionTypes.ADD_FAVOURITE_SUCCESS,
                ActionTypes.ADD_FAVOURITE_FAILURE
            ],
            endpoint: () => get(`v1/account/favorite/add/${uuid}`)
        }
    });
};

export const getFavourites = () => (dispatch, getState) => {
    const {pending} = getState().account;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_FAVOURITES_REQUEST,
                ActionTypes.GET_FAVOURITES_SUCCESS,
                ActionTypes.GET_FAVOURITES_FAILURE
            ],
            endpoint: () => get('v1/account/favorite')
        }
    });
};

export const removeFavourite = uuid => (dispatch, getState) => {
    const {pending} = getState().account;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.REMOVE_FAVOURITE_REQUEST,
                ActionTypes.REMOVE_FAVOURITE_SUCCESS,
                ActionTypes.REMOVE_FAVOURITE_FAILURE
            ],
            endpoint: () => remove(`v1/account/favorite/remove/${uuid}`)
        }
    });
};

export const getUserAnnouncements = () => (dispatch, getState) => {
    const {pending} = getState().account;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_USER_ANNOUNCEMENTS_REQUEST,
                ActionTypes.GET_USER_ANNOUNCEMENTS_SUCCESS,
                ActionTypes.GET_USER_ANNOUNCEMENTS_FAILURE
            ],
            endpoint: () => get('v1/account/announcement', {size: 12})
        }
    });
};

export const getRentRequests = id => dispatch => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_RENT_REQUESTS_REQUEST,
                ActionTypes.GET_RENT_REQUESTS_SUCCESS,
                ActionTypes.GET_RENT_REQUESTS_FAILURE
            ],
            endpoint: () => get(`v1/account/announcement/${id}/rented-dates`, {status: 'WAITING'})
        }
    });
};

export const confirmRentRequest = body => dispatch => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.CONFIRM_RENT_REQUESTS_REQUEST,
                ActionTypes.CONFIRM_RENT_REQUESTS_SUCCESS,
                ActionTypes.CONFIRM_RENT_REQUESTS_FAILURE
            ],
            endpoint: () => put('v1/account/announcement/rented-dates', body)
        }
    });
};

export const rejectRentRequest = body => dispatch => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.REJECT_RENT_REQUESTS_REQUEST,
                ActionTypes.REJECT_RENT_REQUESTS_SUCCESS,
                ActionTypes.REJECT_RENT_REQUESTS_FAILURE
            ],
            endpoint: () => put('v1/account/announcement/rented-dates', body)
        }
    });
};

export const getSingleUserAnnouncement = id => dispatch => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_SINGLE_USER_ANNOUNCEMENT_REQUEST,
                ActionTypes.GET_SINGLE_USER_ANNOUNCEMENT_SUCCESS,
                ActionTypes.GET_SINGLE_USER_ANNOUNCEMENT_FAILURE
            ],
            endpoint: () => get(`v1/account/announcement/${id}`)
        }
    });
};

export const deleteAnnouncement = id => dispatch => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.DELETE_USER_ANNOUNCEMENT_REQUEST,
                ActionTypes.DELETE_USER_ANNOUNCEMENT_SUCCESS,
                ActionTypes.DELETE_USER_ANNOUNCEMENT_FAILURE
            ],
            endpoint: () => remove(`v1/account/announcement/${id}`)
        }
    });
};

export const toggleHideAnnouncement = body => dispatch => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.TOGGLE_HIDE_ANNOUNCEMENT_REQUEST,
                ActionTypes.TOGGLE_HIDE_ANNOUNCEMENT_SUCCESS,
                ActionTypes.TOGGLE_HIDE_ANNOUNCEMENT_FAILURE
            ],
            endpoint: () => put('v1/account/announcement/hide', body)
        }
    });
};

export const deleteImage = id => dispatch => imageId => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.DELETE_IMAGE_REQUEST,
                ActionTypes.DELETE_IMAGE_SUCCESS,
                ActionTypes.DELETE_IMAGE_FAILURE
            ],
            endpoint: () => remove(`v1/account/announcement/${id}/image/${imageId}`)
        }
    });
};

export const sendMessage = values => (dispatch) => {

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.SEND_MESSAGE_REQUEST,
                ActionTypes.SEND_MESSAGE_SUCCESS,
                ActionTypes.SEND_MESSAGE_FAILURE
            ],
            endpoint: () => post('v1/account/message/announcement/new', values)
        }
    });
};

export const sendHelpMessage = values => (dispatch) => {

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.SEND_HELP_MESSAGE_REQUEST,
                ActionTypes.SEND_HELP_MESSAGE_SUCCESS,
                ActionTypes.SEND_HELP_MESSAGE_FAILURE
            ],
            endpoint: () => post('v1/stuff/help', values)
        }
    });
};

export const getConversations = () => (dispatch, getState) => {
    const {pending} = getState().account;

    if (pending) {
        return null;
    }

    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.GET_CONVERSATIONS_REQUEST,
                ActionTypes.GET_CONVERSATIONS_SUCCESS,
                ActionTypes.GET_CONVERSATIONS_FAILURE
            ],
            endpoint: () => get('v1/account/message/')
        }
    });
};

export const sendConversationMessage = values => (dispatch) => {
    return dispatch({
        [CALL_API]: {
            types: [
                ActionTypes.SEND_CONVERSATION_MESSAGE_REQUEST,
                ActionTypes.SEND_CONVERSATION_MESSAGE_SUCCESS,
                ActionTypes.SEND_CONVERSATION_MESSAGE_FAILURE
            ],
            endpoint: () => post('v1/account/message/new', values)
        }
    });
};
