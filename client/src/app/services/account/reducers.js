import {
    GET_FAVOURITES_REQUEST,
    GET_FAVOURITES_SUCCESS,
    GET_FAVOURITES_FAILURE,
    REMOVE_FAVOURITE_SUCCESS,
    GET_USER_ANNOUNCEMENTS_REQUEST,
    GET_USER_ANNOUNCEMENTS_SUCCESS,
    GET_USER_ANNOUNCEMENTS_FAILURE,
    GET_SINGLE_USER_ANNOUNCEMENT_REQUEST,
    GET_SINGLE_USER_ANNOUNCEMENT_SUCCESS,
    GET_SINGLE_USER_ANNOUNCEMENT_FAILURE,
    DELETE_USER_ANNOUNCEMENT_REQUEST,
    DELETE_USER_ANNOUNCEMENT_SUCCESS,
    DELETE_USER_ANNOUNCEMENT_FAILURE,
    TOGGLE_HIDE_ANNOUNCEMENT_SUCCESS,
    DELETE_IMAGE_SUCCESS,
    GET_CONVERSATIONS_REQUEST,
    GET_CONVERSATIONS_SUCCESS,
    GET_CONVERSATIONS_FAILURE,
    SEND_CONVERSATION_MESSAGE_REQUEST,
    SEND_CONVERSATION_MESSAGE_SUCCESS,
    SEND_CONVERSATION_MESSAGE_FAILURE,
    ADD_FAVOURITE_SUCCESS
} from './constants';

const DEFAULT_FAVOURITES_STATE = {
    pending: false,
    loaded: false,
    favourites: {},
    announcements: {},
    conversations: [],
    pendingSendingMessage: false
};

export const accountReducer = (state = DEFAULT_FAVOURITES_STATE, action) => {
    if (action.type === GET_FAVOURITES_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }

    if (action.type === GET_FAVOURITES_SUCCESS) {
        return {
            ...state,
            favourites: action.response.reduce((newData, announcement) => {
                newData[announcement.uuid] = announcement;
                return newData;
            }, {}),
            pending: false
        };
    }

    if (action.type === GET_FAVOURITES_FAILURE) {
        return {
            ...state,
            favourites: {},
            pending: false,
            loaded: false
        };
    }

    if (action.type === GET_USER_ANNOUNCEMENTS_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }
    if (action.type === GET_USER_ANNOUNCEMENTS_SUCCESS) {
        return {
            ...state,
            announcements: action.response.content.reduce((newData, announcement) => {
                newData[announcement.uuid] = announcement;
                return newData;
            }, {}),
            pending: false
        };
    }
    if (action.type === GET_USER_ANNOUNCEMENTS_FAILURE) {
        return {
            ...state,
            announcements: {},
            pending: false,
            loaded: false,
            error: action.error
        };
    }

    if (action.type === GET_SINGLE_USER_ANNOUNCEMENT_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }
    if (action.type === GET_SINGLE_USER_ANNOUNCEMENT_SUCCESS) {
        return {
            ...state,
            announcements: {
                ...state.announcements,
                [action.response.uuid]: {...state.announcements[action.response.uuid], ...action.response}
            },
            pending: false
        };
    }
    if (action.type === GET_SINGLE_USER_ANNOUNCEMENT_FAILURE) {
        return {
            ...state,
            announcements: {},
            pending: false,
            loaded: false,
            error: action.error
        };
    }

    if (action.type === ADD_FAVOURITE_SUCCESS) {

        return {
            ...state,
            announcements: {
                ...state.announcements,
                [action.response.uuid]: {
                    ...state.announcements[action.response.uuid],
                    favorite: action.response.favorite
                }
            }
        };
    }

    if (action.type === REMOVE_FAVOURITE_SUCCESS) {
        let newFavourites = {...state.favourites};
        delete newFavourites[action.response.uuid];

        return {
            ...state,
            favourites: newFavourites,
            announcements: {
                ...state.announcements,
                [action.response.uuid]: {
                    ...state.announcements[action.response.uuid],
                    favorite: action.response.favorite
                }
            }
        };
    }

    if (action.type === DELETE_USER_ANNOUNCEMENT_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }
    if (action.type === DELETE_USER_ANNOUNCEMENT_SUCCESS) {
        let newAnnouncements = {...state.announcements};
        delete newAnnouncements[action.response.uuid];

        return {
            ...state,
            announcements: newAnnouncements,
            pending: false
        };
    }
    if (action.type === DELETE_USER_ANNOUNCEMENT_FAILURE) {
        return {
            ...state,
            announcements: {},
            pending: false,
            loaded: false,
            error: action.error
        };
    }

    if (action.type === TOGGLE_HIDE_ANNOUNCEMENT_SUCCESS) {
        return {
            ...state,
            announcements: {
                ...state.announcements,
                [action.response.uuid]: {
                    ...state.announcements[action.response.uuid],
                    hidden: action.response.hidden
                }
            }
        };
    }

    if (action.type === DELETE_IMAGE_SUCCESS) {
        const changedAnnouncement = state.announcements[action.response.announcementUuid];
        const deletedIndex = changedAnnouncement.images.findIndex(image => image.uuid === action.response.imageUuid);

        return {
            ...state,
            announcements: {
                ...state.announcements,
                [action.response.announcementUuid]: {
                    ...changedAnnouncement,
                    images: [
                        ...changedAnnouncement.images.slice(0, deletedIndex),
                        ...changedAnnouncement.images.slice(deletedIndex + 1)
                    ]
                }
            }
        };
    }

    if (action.type === GET_CONVERSATIONS_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }

    if (action.type === GET_CONVERSATIONS_SUCCESS) {
        return {
            ...state,
            conversations: action.response,
            pending: false
        };
    }

    if (action.type === GET_CONVERSATIONS_FAILURE) {
        return {
            ...state,
            conversations: [],
            pending: false,
            loaded: false
        };
    }

    if (action.type === SEND_CONVERSATION_MESSAGE_REQUEST) {
        return {
            ...state
        };
    }

    if (action.type === SEND_CONVERSATION_MESSAGE_SUCCESS) {
        let index = 0;
        let conversationObj = state.conversations.find((el, i) => {
            index = i;
            return el.id === action.response.conversationId;
        });

        let newObj = {...conversationObj, messages: [...conversationObj.messages, action.response]};

        return {
            ...state,
            conversations: [
                ... state.conversations.slice(0, index),
                newObj,
                ... state.conversations.slice(index + 1)
            ]
        };
    }

    if (action.type === SEND_CONVERSATION_MESSAGE_FAILURE) {
        return {
            ...state,
            conversations: []
        };
    }

    return state;
};
