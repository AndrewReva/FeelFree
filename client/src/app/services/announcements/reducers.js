import {
    GET_ANNOUNCEMENTS_REQUEST,
    GET_ANNOUNCEMENTS_SUCCESS,
    GET_ANNOUNCEMENTS_FAILURE,
    GET_SINGLE_ANNOUNCEMENT_REQUEST,
    GET_SINGLE_ANNOUNCEMENT_SUCCESS,
    GET_SINGLE_ANNOUNCEMENT_FAILURE,
    GET_NEXT_ANNOUNCEMENTS_REQUEST,
    GET_NEXT_ANNOUNCEMENTS_SUCCESS,
    GET_NEXT_ANNOUNCEMENTS_FAILURE,
    SET_SEARCH_PARAMETERS,
    GET_PRICE_RANGE_REQUEST,
    GET_PRICE_RANGE_SUCCESS,
    GET_PRICE_RANGE_FAILURE,
    GET_ANNOUNCEMENT_COMMENTS_REQUEST,
    GET_ANNOUNCEMENT_COMMENTS_SUCCESS,
    GET_ANNOUNCEMENT_COMMENTS_FAILURE,
    ADD_ANNOUNCEMENT_COMMENT_REQUEST,
    ADD_ANNOUNCEMENT_COMMENT_SUCCESS,
    ADD_ANNOUNCEMENT_COMMENT_FAILURE,
    GET_NEXT_ANNOUNCEMENT_COMMENTS_REQUEST,
    GET_NEXT_ANNOUNCEMENT_COMMENTS_SUCCESS,
    GET_NEXT_ANNOUNCEMENT_COMMENTS_FAILURE,
    BOOK_ANNOUNCEMENT_REQUEST,
    BOOK_ANNOUNCEMENT_SUCCESS,
    BOOK_ANNOUNCEMENT_FAILURE
} from './constants';

import {ADD_FAVOURITE_SUCCESS, REMOVE_FAVOURITE_SUCCESS} from '../account/constants';

const DEFAULT_ANNOUNCEMENTS_STATE = {
    pending: false,
    loaded: false,
    data: {},
    totalPagesAnnouncements: null,
    pageNumberAnnouncements: null,
    totalPagesComments: null,
    pageNumberComments: null,
    error: '',
    parameters: {},
    priceRange: {},
    pendingPriceRange: false,
    pendingComments: false,
    pendingBooking: false
};

export const announcementsReducer = (state = DEFAULT_ANNOUNCEMENTS_STATE, action) => {
    if (action.type === GET_ANNOUNCEMENTS_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }

    if (action.type === GET_ANNOUNCEMENTS_SUCCESS) {
        return {
            ...state,
            data: action.response.content.reduce((newData, announcement) => {
                newData[announcement.uuid] = announcement;
                return newData;
            }, {}),
            totalPagesAnnouncements: action.response.totalPages,
            pageNumberAnnouncements: action.response.number,
            pending: false,
            error: ''
        };
    }

    if (action.type === GET_ANNOUNCEMENTS_FAILURE) {
        return {
            ...state,
            data: {},
            pending: false,
            loaded: false,
            error: action.error
        };
    }

    if (action.type === GET_SINGLE_ANNOUNCEMENT_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }

    if (action.type === GET_SINGLE_ANNOUNCEMENT_SUCCESS) {
        return {
            ...state,
            data: {...state.data, [action.response.uuid]: {...state.data[action.response.uuid], ...action.response}},
            pending: false
        };
    }

    if (action.type === GET_SINGLE_ANNOUNCEMENT_FAILURE) {
        return {
            ...state,
            data: {},
            pending: false,
            loaded: false
        };
    }

    if (action.type === GET_NEXT_ANNOUNCEMENTS_REQUEST) {
        return {
            ...state,
            pending: true
        };
    }

    if (action.type === GET_NEXT_ANNOUNCEMENTS_SUCCESS) {
        return {
            ...state,
            data: action.response.content.reduce((newData, announcement) => {
                newData[announcement.uuid] = announcement;
                return newData;
            }, {...state.data}),
            pageNumberAnnouncements: action.response.number,
            pending: false
        };
    }

    if (action.type === GET_NEXT_ANNOUNCEMENTS_FAILURE) {
        return {
            ...state,
            pending: false,
            loaded: false
        };
    }

    if (action.type === SET_SEARCH_PARAMETERS) {
        return {
            ...state,
            parameters: action.values
        };
    }

    if (action.type === GET_PRICE_RANGE_REQUEST) {
        return {
            ...state,
            pendingPriceRange: true
        };
    }

    if (action.type === GET_PRICE_RANGE_SUCCESS) {
        return {
            ...state,
            priceRange: action.response,
            pendingPriceRange: true
        };
    }

    if (action.type === GET_PRICE_RANGE_FAILURE) {
        return {
            ...state,
            priceRange: {},
            pendingPriceRange: false
        };
    }

    if (action.type === ADD_FAVOURITE_SUCCESS || action.type === REMOVE_FAVOURITE_SUCCESS) {
        return {
            ...state,
            data: {
                ...state.data,
                [action.response.uuid]: {
                    ...state.data[action.response.uuid],
                    favorite: action.response.favorite
                }
            }
        };
    }

    if (action.type === ADD_FAVOURITE_SUCCESS) {
        return {
            ...state,
            data: {
                ...state.data,
                [action.response.uuid]: {
                    ...state.data[action.response.uuid],
                    favorite: action.response.favorite
                }
            }
        };
    }

    if (action.type === GET_ANNOUNCEMENT_COMMENTS_REQUEST) {
        return {
            ...state,
            data: {...state.data, [action.id]: {
                ...state.data[action.id], comments: {content:[]}}},
            pendingComments: true
        };
    }

    if (action.type === GET_ANNOUNCEMENT_COMMENTS_SUCCESS) {
        return {
            ...state,
            data: {...state.data, [action.id]: {
                ...state.data[action.id], comments: {...action.response}}},
            totalPagesComments: action.response.totalPages,
            pageNumberComments: action.response.number,
            pendingComments: false
        };
    }

    if (action.type === GET_ANNOUNCEMENT_COMMENTS_FAILURE) {
        return {
            ...state,
            pendingComments: false
        };
    }

    if (action.type === ADD_ANNOUNCEMENT_COMMENT_REQUEST) {
        return {
            ...state,
            pendingComments: true
        };
    }
    if (action.type === ADD_ANNOUNCEMENT_COMMENT_SUCCESS) {
        return {
            ...state,
            data: {
                ...state.data,
                [action.id]: {...state.data[action.id],
                    comments: {...state.data[action.id].comments,
                        content:[action.response, ...state.data[action.id].comments.content]}
                }},
            pendingComments: false
        };
    }
    if (action.type === ADD_ANNOUNCEMENT_COMMENT_FAILURE) {
        return {
            ...state,
            pendingComments: false
        };
    }

    if (action.type === GET_NEXT_ANNOUNCEMENT_COMMENTS_REQUEST) {
        return {
            ...state,
            pendingComments: true
        };
    }

    if (action.type === GET_NEXT_ANNOUNCEMENT_COMMENTS_SUCCESS) {
        return {
            ...state,
            data: {
                ...state.data,
                [action.id]: {...state.data[action.id],
                    comments: {...state.data[action.id].comments,
                        content:[...state.data[action.id].comments.content, ...action.response.content]}
                }},
            pageNumberComments: action.response.number,
            pendingComments: false
        };
    }

    if (action.type === GET_NEXT_ANNOUNCEMENT_COMMENTS_FAILURE) {
        return {
            ...state,
            pendingComments: false
        };
    }

    if (action.type === BOOK_ANNOUNCEMENT_REQUEST) {
        return {
            ...state,
            pendingBooking: true
        };
    }

    if (BOOK_ANNOUNCEMENT_SUCCESS || BOOK_ANNOUNCEMENT_FAILURE) {
        return {
            ...state,
            pendingBooking: false
        };
    }

    return state;
};
