import {
    USER_LOGOUT,
    GET_USER_REQUEST,
    GET_USER_SUCCESS,
    GET_USER_FAILURE,
    USER_CHECK_AUTHENTICATION,
    USER_AVATAR_CHANGE
} from './constants';

const DEFAULT_AUTHENTICATION_STATE = {
    loaded: false,
    pending: false,
    checked: false,
    isLoggedIn: false,
    data: {}
};

export const userReducer = (state = DEFAULT_AUTHENTICATION_STATE, action) => {
    if (action.type === USER_CHECK_AUTHENTICATION) {
        return {
            ...state,
            checked: true
        };
    }
    if (action.type === USER_LOGOUT) {
        return {
            ...state,
            loaded: false,
            pending: false,
            checked: true,
            data: {},
            isLoggedIn: false
        };
    }
    if (action.type === GET_USER_REQUEST) {
        return {
            ...state,
            loaded: false,
            pending: true
        };
    }
    if (action.type === GET_USER_SUCCESS) {
        return {
            ...state,
            loaded: true,
            pending: false,
            checked: true,
            isLoggedIn: true,
            data: action.response
        };
    }
    if (action.type === GET_USER_FAILURE) {
        return {
            ...state,
            pending: false,
            loaded: false,
            checked: false,
            data: {}
        };
    }

    if (action.type === USER_AVATAR_CHANGE) {
        return {
            ...state,
            data: {
                ...state.data,
                image: action.image
            }
        };
    }

    return state;
};
