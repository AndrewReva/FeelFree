import './user-control.styl';
import React from 'react';

import {Avatar, AvatarTypes} from '../avatar';
import {Nav, NavItem} from 'kit/components/nav';
import {Link} from 'kit/components/link';

export const UserControl = ({user, location}) => {
    if (user && user.isLoggedIn) {
        const image = user.data.image;

        return (
            <div className="user-control">
                <Nav className="header__nav user-control__nav">
                    <NavItem to="/user/logout">Logout</NavItem>
                </Nav>
                <Link to="/user">
                    <Avatar type={AvatarTypes.small} image={image}/>
                </Link>
            </div>
        );
    } else {
        return (
            <div className="user-control">
                <Nav className="header__nav">
                    <NavItem to={{pathname: '/login', state: { from: location }}}>Login</NavItem>
                    <NavItem to="/registration">Register</NavItem>
                </Nav>
            </div>
        );
    }
};
