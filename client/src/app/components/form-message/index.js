import './form-message.styl';
import React from 'react';
import classNames from 'classnames';

export const FormMessageTypes = {
    success: 'success',
    error: 'error'
};

const Classes = {
    [FormMessageTypes.success]: 'form-message_success',
    [FormMessageTypes.error]: 'form-message_error'
};

export const FormMessage = ({message, type, className}) => {
    const classes = classNames('form-message', Classes[type], className);

    return (
        <div className={classes}>
            {message}
        </div>
    );
};
