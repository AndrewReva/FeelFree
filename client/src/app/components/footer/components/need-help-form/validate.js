export const validate = values => {
    const errors = {};
    if (!values.title) {
        errors.title = 'Please enter title';
    }
    if (!values.messageBody) {
        errors.messageBody = 'Please enter message';
    }
    if (!values.email) {
        errors.email = 'Please enter email';
    } else if (!/^[_A-Za-z0-9-\+]+(\..[_A-Za-z0-9-]+)*@[A-Za-zА-Яа-я0-9-_]+(\.[A-Za-zА-Яа-я0-9-_]+)*(\.[A-Za-zА-Яа-я0-9-_]{2,})*([^0-9!@#$%+^&*\(\);:\'\"\\?.,/\[\]{}<>№ ]+)$/i
            .test(values.email)) {
        errors.email = 'Please enter valid email address';
    }
    return errors;
};
