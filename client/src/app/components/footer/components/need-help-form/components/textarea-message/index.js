import React from 'react';
import {TextArea} from 'kit/components/textarea/index';
import {ValidationError} from 'kit/components/validation-error';

export const TextAreaMessage = ({ input, label, type, meta: { touched, error, warning} }) => (
    <div>
        <TextArea
            type={type}
            {...input}
            placeholder={label}
        />
        {touched && error && <ValidationError>{error}</ValidationError>}
    </div>
);
