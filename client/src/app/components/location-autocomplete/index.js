import './location-autocomplete.styl';
import React, {Component} from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
import autobind from 'autobind-decorator';

import {ValidationError} from 'kit/components/validation-error';

@autobind
export class LocationAutocomplete extends Component {
    constructor(props) {
        super(props);

        this.state = {value: this.props.input.value.address || ''};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.input.value.address !== this.props.input.value.address) {
            this.setState({value: nextProps.input.value.address});
        }
    }


    handleChange(value) {
        this.setState({value});
        if (value === '') {
            this.props.input.onChange({address: value});
        }
    }

    handleSelect(value, placeId) {
        this.setState({value});
        this.props.input.onChange({address: value, placeId});
    }

    render() {
        const {error, submitFailed} = this.props.meta;

        const inputProps = {
            value: this.state.value,
            type: 'search',
            onChange: this.handleChange,
            onFocus: this.props.input.onFocus,
            placeholder: this.props.label
        };

        const cssClasses = {
            root: 'location-form-group',
            input: 'input input_primary location-form-group__input',
            autocompleteContainer: 'location-form-group__autocomplete-container',
            autocompleteItem: 'location-form-group__autocomplete-item',
            autocompleteItemActive: 'location-form-group__autocomplete-item_active'
        };

        const options = {
            bounds: new window.google.maps.LatLngBounds(
                new window.google.maps.LatLng(44.3837, 22.1776),
                new window.google.maps.LatLng(52.3315, 40.2118),
            ),
            types: ['(cities)']
        };

        return (
            <div>
                <PlacesAutocomplete
                    inputProps={inputProps}
                    onSelect={this.handleSelect}
                    classNames={cssClasses}
                    options={options}
                    clearItemsOnError={true}
                />
                {submitFailed && error && <ValidationError>{error}</ValidationError>}
            </div>
        );
    }
}
