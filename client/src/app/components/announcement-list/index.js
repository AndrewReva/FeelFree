import './announcement-list.styl';
import React from 'react';

export const AnnouncementList = ({children}) => (
    <div className="announcement-list" children={children}/>
);

/*
 <div>
 {isEmpty(announcements) ?{Object.values(announcements).map(announcement => (
 <AnnouncementPreview
 key={announcement.uuid}
 announcement={announcement}
 addFavourite={addFavourite}
 removeFavourite={removeFavourite}
 />
 ))}
 (<div><Spinner/></div>)
 :
 (<div className="announcement-list">
 {Object.values(announcements).map(announcement => (
 <AnnouncementPreview
 key={announcement.uuid}
 announcement={announcement}
 addFavourite={addFavourite}
 />
 ))}
 </div>)
 }
 </div>
 */
