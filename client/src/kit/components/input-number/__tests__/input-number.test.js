import React from 'react';
import {shallow, mount} from 'enzyme';
import {InputNumber} from '../';


describe('InputNumber', () => {

    const newValue = 10;
    const smallValue = 1;

    it('Should change value state after input change', () => {
        const wrapper = shallow(<InputNumber/>);
        expect(wrapper.state('value')).toEqual(1);

        wrapper.find('input').simulate('change', {target: {value: newValue}});
        expect(wrapper.state('value')).toEqual(newValue);
    });
    it('Should change value state after buttonDown clicked', () => {
        const wrapper = shallow(<InputNumber/>);
        wrapper.find('input').simulate('change', {target: {value: newValue}});
        expect(wrapper.state('value')).toEqual(newValue);

        wrapper.find('.input-number__down').simulate('click');
        expect(wrapper.state('value')).toEqual(newValue - 1);

    });
    it('Should not change value state after buttonDown clicked', () => {
        const wrapper = shallow(<InputNumber/>);
        wrapper.find('input').simulate('change', {target: {value: smallValue}});
        expect(wrapper.state('value')).toEqual(smallValue);

        wrapper.find('.input-number__down').simulate('click');
        expect(wrapper.state('value')).toEqual(smallValue);

    });
    it('Should change value state after buttonUp clicked', () => {
        const wrapper = shallow(<InputNumber/>);
        wrapper.find('input').simulate('change', {target: {value: newValue}});
        expect(wrapper.state('value')).toEqual(newValue);

        wrapper.find('.input-number__up').simulate('click');
        expect(wrapper.state('value')).toEqual(newValue + 1);
    });
    it('Should update the state of the component when the value prop is changed', () => {
        const component = mount(<InputNumber/>);
        expect(component.find('input').node.value).toEqual('1');
        component.setProps({ value: newValue });
        expect(component.state('value')).toEqual(newValue);
    });
    it('Should not update the state of the component', () => {
        const component = mount(<InputNumber/>);
        expect(component.find('input').node.value).toEqual('1');
        component.setProps({ value: 1 });
        expect(component.state('value')).toEqual(1);
    });

});
