import React from 'react';
import {shallow} from 'enzyme';
import {Select} from '../';


describe('Select', () => {
    const newValue = 'newValue';
    const newClassName = 'newClassName';
    const newOptions = [{'value': 'value 1', 'label': 'label 1'}];

    it('Should exist right className(primary type)', () => {
        const wrapper = shallow(<Select className={newClassName}/>);
        expect(wrapper.find('.select-default').hasClass(newClassName)).toBeTruthy();
    });

    it('Should exist right value state', () => {
        const wrapper = shallow(<Select value={newValue}/>);
        expect(wrapper.state('value')).toEqual(newValue);
    });

    it('Should change value state after select change', () => {
        const wrapper = shallow(<Select/>);
        expect(wrapper.state('value')).toEqual('');
        wrapper.find('.select-default').simulate('change', newValue);
        expect(wrapper.state('value')).toEqual(newValue);
    });

    it('Should exist right options', () => {
        const wrapper = shallow(<Select options={newOptions}/>);
        expect(wrapper.find('.select-default').props().options).toEqual(newOptions);
    });

    it('Should update the state of the component when the value prop is changed', () => {
        const wrapper = shallow(<Select/>);
        expect(wrapper.state('value')).toEqual('');
        wrapper.setProps({ value: newValue });
        expect(wrapper.state('value')).toEqual(newValue);
    });

    it('Should not update the state of the component', () => {
        const wrapper = shallow(<Select/>);
        expect(wrapper.state('value')).toEqual('');
        wrapper.setProps({ value: '' });
        expect(wrapper.state('value')).toEqual('');
    });

});
