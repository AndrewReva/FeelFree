import './icon-button.styl';
import React, {PropTypes} from 'react';
import {Icon} from '../icon';
import {noop} from 'lodash';

export const IconButton = props => {
    return (
        <button className="btn-nav btn-nav_icon" onClick={props.onClick}>
            {props.icon &&
            <Icon type={props.icon}/>
            }
        </button>
    );
};

IconButton.propTypes = {
    icon: PropTypes.string,
    onClick: PropTypes.func
};

IconButton.defaultProps = {
    icon: '',
    onClick: noop
};
