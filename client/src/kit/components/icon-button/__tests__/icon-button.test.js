import React from 'react';
import {mount} from 'enzyme';
import {IconButton} from '../';
import {Icon, IconTypes} from '../../icon';


describe('IconButton', () => {
    it('Should display icon if icon prop exist', () => {
        const wrapper = mount(<IconButton icon={IconTypes.twitter}/>);
        expect(wrapper.find(Icon).length).toBe(1);
    });
});
