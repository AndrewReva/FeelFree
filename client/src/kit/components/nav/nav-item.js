import React, {PropTypes} from 'react';
import {Link} from '../link';

export const NavItem = props => (
    <li className="nav__item">
        <Link {...props}/>
    </li>
);

NavItem.propTypes = {
    to: PropTypes.oneOfType([
        React.PropTypes.string,
        React.PropTypes.object
    ])
};
