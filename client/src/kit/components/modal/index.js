import React, {Component, PropTypes} from 'react';
import autobind from 'autobind-decorator';
import ReactModal from 'react-modal';
import {Button, ButtonTypes} from '../button';
import {noop} from 'lodash';

@autobind
export class Modal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: this.props.isOpen
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.isOpen !== this.props.isOpen) {
            this.setState({isOpen: nextProps.isOpen});
        }
    }

    handleCloseModal() {
        this.setState({isOpen: false});
        this.props.closeModal();
    }

    render() {
        const customStyles = {
            content : this.props.style
        };

        return (
            <ReactModal
                className={this.props.className}
                isOpen={this.state.isOpen}
                style={customStyles}
                contentLabel={this.props.contentLabel}
                onRequestClose={this.props.onRequestClose}
            >
                {this.props.children}
                <Button buttonType={ButtonTypes.default} caption="Close" onClick={this.handleCloseModal}/>
            </ReactModal>
        );
    }
}

Modal.PropTypes = {
    isOpen: PropTypes.bool,
    children: PropTypes.any,
    className: PropTypes.string,
    style: PropTypes.object,
    contentLabel: PropTypes.string,
    closeModal: PropTypes.func,
    onRequestClose: PropTypes.func
};

Modal.defaultProps = {
    isOpen: false,
    children: '',
    className: '',
    style: {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    },
    contentLabel: '',
    closeModal: noop,
    onRequestClose: noop
};
