import React from 'react';
import {shallow, mount} from 'enzyme';
import {Checkbox} from '../index';

describe('Checkbox', () => {
    const value = true;
    const newValue = false;

    it('Should exist right checked value', () => {
        const component = mount(<Checkbox checked={value}/>);
        expect(component.find('input').node.checked).toEqual(value);
    });

    it('Should change checked value state after checkbox change', () => {
        const component = shallow(<Checkbox checked={value}/>);
        expect(component.state('checked')).toEqual(value);

        component.find('input').simulate('change', {target: {checked: newValue}});
        expect(component.state('checked')).toEqual(newValue);
    });

    it('Should update the state of the component when the checked prop is changed', () => {
        const component = mount(<Checkbox checked={value}/>);
        expect(component.find('input').node.checked).toEqual(value);
        component.setProps({ checked: newValue });
        expect(component.state('checked')).toEqual(newValue);
    });

    it('Should not update the state of the component', () => {
        const component = mount(<Checkbox checked={value}/>);
        expect(component.find('input').node.checked).toEqual(value);
        component.setProps({ checked: value });
        expect(component.state('checked')).toEqual(value);
    });
});
