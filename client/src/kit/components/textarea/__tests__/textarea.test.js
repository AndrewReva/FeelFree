import React from 'react';
import {shallow, mount} from 'enzyme';
import {TextArea} from '../';

describe('TextArea', () => {
    const placeholder = 'newPlaceholder';

    const newValue = 'newValue';

    it('Should exist right placeholder', () => {
        const wrapper = mount(<TextArea placeholder={placeholder}/>);
        expect(wrapper.find('.text-area').node.placeholder).toEqual(placeholder);
    });

    it('Should change value state after textArea input change', () => {
        const wrapper = shallow(<TextArea/>);
        expect(wrapper.state('value')).toEqual('');
        wrapper.find('.text-area').simulate('change', {target: {value: newValue}});
        expect(wrapper.state('value')).toEqual(newValue);
    });

    it('Should update the state of the component when the value prop is changed', () => {
        const wrapper = shallow(<TextArea/>);
        expect(wrapper.state('value')).toEqual('');
        wrapper.setProps({ value: newValue });
        expect(wrapper.state('value')).toEqual(newValue);
    });

    it('Should not update the state of the component', () => {
        const component = mount(<TextArea/>);
        expect(component.state('value')).toEqual('');
        component.setProps({ value: '' });
        expect(component.state('value')).toEqual('');
    });

});
