import React from 'react';
import { shallow, mount } from 'enzyme';
import {Button, ButtonTypes} from '../';


describe('Button', () => {

    const caption = 'newCaption';

    it('Should exist right className(default type)', () => {
        const wrapper = shallow(<Button buttonType={ButtonTypes.default}/>);
        expect(wrapper.find('.btn').hasClass('btn_default')).toBeTruthy();
    });
    it('Should exist right className(primary type)', () => {
        const wrapper = shallow(<Button buttonType={ButtonTypes.primary}/>);
        expect(wrapper.find('.btn').hasClass('btn_primary')).toBeTruthy();
    });
    it('Should exist right className(ternary button)', () => {
        const wrapper = shallow(<Button ternary={true}/>);
        expect(wrapper.find('.btn').hasClass('btn_ternary')).toBeTruthy();
    });
    it('Should exist right className(secondary button)', () => {
        const wrapper = shallow(<Button secondary={true}/>);
        expect(wrapper.find('.btn').hasClass('btn_secondary')).toBeTruthy();
    });
    it('Should exist right caption', () => {
        const wrapper = shallow(<Button caption={caption}/>);
        expect(wrapper.find('.btn').text()).toEqual(caption);
    });
    it('Should display icon if it is secondary button', () => {
        const wrapper = mount(<Button secondary={true}/>);
        expect(wrapper.find('.icon').length).toBe(1);
    });
});
