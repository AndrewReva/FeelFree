import './toggle.styl';
import React, {Component, PropTypes} from 'react';
import {Icon, IconTypes} from '../icon';
import autobind from 'autobind-decorator';
import classNames from 'classnames';
import {noop, isString} from 'lodash/fp';

@autobind
export class Toggle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value});
        }
    }

    onChange(event) {
        let newValue = event.target.checked;
        this.setState({value: newValue});
        if (this.props.toggleType === 'text' && newValue) {
            this.props.onChange(this.props.toggleLeft.toUpperCase());
        } else if (this.props.toggleType === 'text' && !newValue) {
            this.props.onChange(this.props.toggleRight.toUpperCase());
        } else {
            this.props.onChange(event.target.checked, this.props.name);
        }
    }

    isString() {
        return (isString(this.props.toggleLeft) && isString(this.props.toggleRight));
    }

    render() {
        let toggleClass = classNames('toggle', {'toggle_text': this.isString()}, this.props.className);
        let inputClass = classNames('toggle__checkbox', {'toggle__checkbox_clicked': !this.state.value});
        let sliderClass = classNames('toggle__slider', {'toggle__slider_text': this.isString()});
        let leftSpanClass = classNames('toggle__left', {'toggle__left_text': this.isString()});
        let rightSpanClass = classNames('toggle__right', {'toggle__right_text': this.isString()});
        let containerClass = classNames({'toggle__text-block': this.isString()});

        return (
            <label className={toggleClass}>
                <input
                    className={inputClass}
                    type="checkbox"
                    checked={this.state.value}
                    onChange={this.onChange}
                    name={this.props.name}
                />
                <div className={sliderClass}/>
                <div className={containerClass}>
                    <span className={leftSpanClass}>{this.props.toggleLeft}</span>
                    <span className={rightSpanClass}>{this.props.toggleRight}</span>
                </div>
            </label>
        );
    }
}

Toggle.propTypes = {
    value: PropTypes.bool,
    toggleType: PropTypes.string.isRequired,
    toggleLeft: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.string
    ]),
    toggleRight: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.string
    ]),
    onChange: PropTypes.func,
    name: PropTypes.string,
    className: PropTypes.string
};

Toggle.defaultProps = {
    value: false,
    toggleType: 'icon',
    toggleLeft: <Icon type={IconTypes.list}/>,
    toggleRight: <Icon type={IconTypes.grid}/>,
    onChange: noop,
    name: '',
    className: ''
};


// toggleType props should be one of: 'icon', 'text'
