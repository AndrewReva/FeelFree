import React from 'react';
import {shallow, mount} from 'enzyme';
import {Toggle} from '../';


describe('Toggle', () => {
    const newValue = false;

    it('Should exist right className(text type)', () => {
        const wrapper = shallow(<Toggle toggleLeft="Day" toggleRight="Month"/>);
        expect(wrapper.find('.toggle').hasClass('toggle_text')).toBeTruthy();
        expect(wrapper.find('.toggle__slider').hasClass('toggle__slider_text')).toBeTruthy();
        expect(wrapper.find('.toggle__left').hasClass('toggle__left_text')).toBeTruthy();
        expect(wrapper.find('.toggle__right').hasClass('toggle__right_text')).toBeTruthy();
    });
    it('Should set right value to state', () => {
        const wrapper = shallow(<Toggle value={true}/>);
        expect(wrapper.state('value')).toEqual(true);
    });
    it('Should change value state after toggle click', () => {
        const wrapper = shallow(<Toggle value={true}/>);
        expect(wrapper.state('value')).toEqual(true);
        wrapper.find('.toggle__checkbox').simulate('change', {target: {checked: false}});
        expect(wrapper.state('value')).toEqual(false);
    });
    it('Should update the state of the component when the value prop is changed', () => {
        const component = mount(<Toggle/>);
        expect(component.find('input').node.checked).toEqual(true);
        component.setProps({ value: newValue });
        expect(component.state('value')).toEqual(newValue);
    });
    it('Should not update the state of the component', () => {
        const component = mount(<Toggle/>);
        expect(component.find('input').node.checked).toEqual(true);
        component.setProps({ value: true });
        expect(component.state('value')).toEqual(true);
    });
});
