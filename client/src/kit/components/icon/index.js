import './icon.styl';
import React, {PropTypes} from 'react';
import classNames from 'classnames';

export const IconTypes = {
    google: 'google',
    facebook: 'facebook',
    twitter: 'twitter',
    user: 'user',
    massage: 'massage',
    heart: 'heart',
    announcements: 'announcements',
    chevronRight: 'chevron-right',
    chevronLeft: 'chevron-left',
    chevronDown: 'chevron-down',
    list: 'list',
    grid: 'grid',
    phone: 'phone',
    tv: 'tv',
    wiFi: 'wiFi',
    plus: 'plus',
    shampoo: 'shampoo',
    essentials: 'essentials',
    conditioner: 'conditioner',
    washingMachine: 'washingMachine',
    balcony: 'balcony',
    iron: 'iron',
    refrigerator: 'refrigerator',
    animals: 'animals',
    smoking: 'smoking',
    hairDryer: 'hairDryer',
    kitchenStuff: 'kitchenStuff'
};

const Classes = {
    [IconTypes.google]: 'icon_google',
    [IconTypes.facebook]: 'icon_fb',
    [IconTypes.twitter]: 'icon_twit',
    [IconTypes.user]: 'icon_user',
    [IconTypes.massage]: 'icon_massage',
    [IconTypes.heart]: 'icon_heart',
    [IconTypes.announcements]: 'icon_announcements',
    [IconTypes.chevronRight]: 'icon_chevron-right',
    [IconTypes.chevronLeft]: 'icon_chevron-left',
    [IconTypes.chevronDown]: 'icon_chevron-down',
    [IconTypes.list]: 'icon_list',
    [IconTypes.grid]: 'icon_grid',
    [IconTypes.phone]: 'icon_phone',
    [IconTypes.tv]: 'icon_tv',
    [IconTypes.wiFi]: 'icon_wifi',
    [IconTypes.plus]: 'icon_plus',
    [IconTypes.shampoo]: 'icon_shampoo',
    [IconTypes.essentials]: 'icon_towel',
    [IconTypes.conditioner]: 'icon_conditioner',
    [IconTypes.washingMachine]: 'icon_washing-machine',
    [IconTypes.balcony]: 'icon_balcony',
    [IconTypes.iron]: 'icon_iron',
    [IconTypes.refrigerator]: 'icon_refrigerator',
    [IconTypes.animals]: 'icon_animals',
    [IconTypes.smoking]: 'icon_smoking',
    [IconTypes.hairDryer]: 'icon_hairdryer',
    [IconTypes.kitchenStuff]: 'icon_kitchen-stuff'
};

export const Icon = props => {
    let className = classNames('icon', Classes[props.type], props.className);

    return (
        <span className={className}/>
    );
};

Icon.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string
};

Icon.defaultProps = {
    type: '',
    className: ''
};
