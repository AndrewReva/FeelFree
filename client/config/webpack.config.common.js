let webpack = require('webpack');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let path = require('path');

module.exports = {
    entry: './src/app/index.js',
    output: {
        publicPath: '/',
        filename: 'bundle.[hash].js',
        path: path.resolve(__dirname, '../build/assets')
    },
    plugins: [
        new HtmlWebpackPlugin({
            publicPath: '/',
            favicon: 'favicon.ico',
            filename: './index.html',
            template: './src/app/index.ejs'
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
                'API_ROOT': JSON.stringify(process.env.API_ROOT)
            }
        })
    ],
    resolve: {
        extensions: [
            '.js'
        ],
        modules: [path.resolve(__dirname, '../src'), 'node_modules']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                include: path.resolve(),
                loader: 'babel-loader'
            },
            {
                test   : /\.(ttf|eot|woff|svg|jpg)$/,
                loader: 'url-loader',
                options: {
                    limit: '10000'
                }
            },
            {
                test: /\.png$/,
                exclude: /node_modules/,
                include: path.resolve(),
                loader: 'file-loader'
            }
        ]
    }
};
